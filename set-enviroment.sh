#!/usr/bin/env bash

if [ "$1" == "dev" ] || [ "$1" == "prod" ];
then
  ENV=$1;

  PATH_ENV=""
  if [ "$1" == "dev" ];
  then
    API_PATH="http://52.14.211.239:85";
  fi
  if [ "$1" == "prod" ];
  then
    API_PATH="http://52.14.211.239:91";
  fi

  echo "Switching to Firebase to $ENV"
   cp -rf "firebaseEnviroments/android/$ENV/google-services.json" android/app/
   cp -rf "firebaseEnviroments/android/$ENV/pd-driver.keystore" android/app/
   cp -rf "firebaseEnviroments/ios/$ENV/GoogleService-Info.plist" ios/pidoylistodriver/
   echo "export default { env: '$ENV', path: '$API_PATH' };" > env.js
  echo "Finish"
  else
    echo "Missing paramenter, please execute sh set-enviroment.sh dev"
fi
