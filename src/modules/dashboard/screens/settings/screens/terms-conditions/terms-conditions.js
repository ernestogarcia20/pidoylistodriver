import React, {Component} from 'react';
import {connect} from 'react-redux';
import TermsConditionsLayout from './components/terms-conditions-layout';
import Container from '../../../../../shared/widgets/container';
import Header from '../../../../../shared/widgets/header';
import I18n from '../../../../../../translation';

class TermsConditions extends Component {
  constructor(props) {
    super(props);
    this.state = {isTerms: true};
  }

  componentDidMount() {
    const {
      navigation: {getParam},
    } = this.props;
    const isTerms = getParam('isTerms', true);
    this.setState({isTerms});
  }

  render() {
    const {
      navigation: {goBack},
    } = this.props;
    const {isTerms} = this.state;
    return (
      <Container>
        <Header
          name={I18n.t(
            isTerms
              ? 'title_header.terms_conditions'
              : 'title_header.privacy_policies',
          )}
          actionDrawer={() => goBack()}
        />
        <TermsConditionsLayout {...this.state} />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(TermsConditions);
