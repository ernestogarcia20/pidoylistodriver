import React from 'react';
import {View, StyleSheet} from 'react-native';
import WebView from 'react-native-webview';

const Layout = ({isTerms}) => {
  return (
    <View style={styles.container}>
      <WebView
        source={{
          uri: isTerms
            ? 'https://www.pidoylisto.com/Terminos.html'
            : 'https://www.pidoylisto.com/Politicas.html',
        }}
      />
    </View>
  );
};

export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEE',
    alignContent: 'center',
  },
});
