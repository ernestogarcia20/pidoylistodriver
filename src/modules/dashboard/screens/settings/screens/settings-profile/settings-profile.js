import React, {Component} from 'react';
import {connect} from 'react-redux';
import SettingsProfileLayout from './components/settings-profile-layout';
import Container from '../../../../../shared/widgets/container';
import Header from '../../../../../shared/widgets/header';
import I18n from '../../../../../../translation';
import {updateDriver} from '../../../../services/services-dashboard';
import {showMessage} from 'react-native-flash-message';

class SettingsProfile extends Component {
  state = {
    first_name: '',
    last_name: '',
    email: '',
    phone: '',
    loading: false,
    editable: false,
  };

  componentDidMount() {
    const {user} = this.props;
    this.setState({...this.state, ...user});
  }

  changeProfile = () => {
    const {editable, first_name, last_name, email, phone} = this.state;
    const {user, checkInternet} = this.props;
    const newUser = {
      first_name,
      last_name,
      email,
      phone,
    };
    this.handleChange('editable', !editable);
    if (!editable) {
      return;
    }

    if (!checkInternet) {
      showMessage({
        message: I18n.t('warning'),
        description: I18n.t('internet'),
        type: 'danger',
      });
      return;
    }

    if (user === newUser) {
      return;
    }
    updateDriver(user.uid_driver, {...user, ...newUser}).then(() => {
      showMessage({
        message: I18n.t('success'),
        description: I18n.t('succes_description'),
        type: 'success',
      });
    });
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    const {
      checkInternet,
      user,
      navigation: {goBack},
    } = this.props;
    return (
      <Container>
        <Header
          name={I18n.t('title_header.profile')}
          actionDrawer={() => goBack()}
        />
        <SettingsProfileLayout
          user={user}
          {...this.state}
          checkInternet={checkInternet}
          setValue={this.handleChange}
          changeProfile={this.changeProfile}
        />
      </Container>
    );
  }
}
const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  checkInternet: state.checkInternet.checkInternet,
});

export default connect(mapStateToProps)(SettingsProfile);
