import React from 'react';
import {
  View,
  StyleSheet,
  ScrollView,
  TouchableOpacity,
  TextInput,
  Dimensions,
} from 'react-native';
import Text from '../../../../../../shared/widgets/text';
import palette from '../../../../../../shared/assets/palette';
import I18n from '../../../../../../../translation';
import {getInitials} from '../../../../../../utils/functions';
import FastImage from 'react-native-fast-image';

const Layout = ({
  user,
  setValue,
  first_name,
  last_name,
  email,
  phone,
  editable,
  changeProfile,
}) => {
  const renderItem = ({name, value, stateName, _editable}) => {
    return (
      <View style={styles.renderItem}>
        <Text
          style={{textTransform: 'capitalize'}}
          weight="Medium"
          fontSize={14}>
          {name}
        </Text>
        <Text>{value}</Text>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <View style={styles.contentImage}>
          <View style={styles.circleImage}>
            <FastImage
              style={styles.circleImage}
              source={{uri: user.selfieUrl}}
            />
          </View>
          <View style={styles.contentCode}>
            <Text weight="Medium" fontSize={16} color="rgba(0,0,0,0.7)">
              Tu codigo:{' '}
              <Text fontSize={20} color={palette.darkColor} weight="SemiBold">
                {String(user.id).padStart(5, '0')}
              </Text>
            </Text>
          </View>
        </View>
        {renderItem({
          name: I18n.t('settings.first_name'),
          value: first_name,
          stateName: 'first_name',
          _editable: true,
        })}
        {renderItem({
          name: I18n.t('settings.last_name'),
          value: last_name,
          stateName: 'last_name',
          _editable: true,
        })}
        {renderItem({
          name: I18n.t('settings.email'),
          value: email,
          stateName: 'email',
          _editable: false,
        })}
        {renderItem({
          name: I18n.t('settings.phone'),
          value: phone,
          stateName: 'phone',
          _editable: false,
        })}
      </ScrollView>
    </View>
  );
};
export default Layout;
const {width} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
  },
  contentCode: {alignItems: 'center', marginHorizontal: 10, marginTop: 15},
  circleImage: {
    width: 150,
    height: 150,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'white',
  },
  contentImage: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '5%',
    marginTop: '10%',
  },
  scrollView: {paddingBottom: '20%'},
  flatList: {
    paddingTop: '15%',
  },
  renderItem: {
    height: 60,
    marginVertical: 10,
    backgroundColor: 'rgba(0,0,0,0.04)',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: '5%',
  },
  renderItemEnable: {
    height: 60,
    marginVertical: 5,
    backgroundColor: 'rgba(255,255,255,0.4)',
    justifyContent: 'space-between',
    flexDirection: 'row',
    alignItems: 'center',
    paddingHorizontal: '5%',
  },
  button: {
    height: 40,
    backgroundColor: palette.secondaryColor,
    position: 'absolute',
    bottom: 15,
    left: '25%',
    right: '25%',
    borderRadius: 10,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  input: {color: 'black', width: '50%', textAlign: 'right'},
});
