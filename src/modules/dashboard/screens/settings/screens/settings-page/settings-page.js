import React, {Component} from 'react';
import {connect} from 'react-redux';
import SettingsPageLayout from './components/settings-page-layout';
import Container from '../../../../../shared/widgets/container';
import Header from '../../../../../shared/widgets/header';
import I18n from '../../../../../../translation';
import {SETTING_PROFILE, TERMS_CONDITIONS} from '../../../../../utils/constants/constants-navigate';
import {updateDriver} from '../../../../services/services-dashboard';
import {showMessage} from 'react-native-flash-message';

class SettingsPage extends Component {
  state = {
    data: [],
  };

  componentDidMount() {
    const {user} = this.props;
    const data = [
      {
        isSwitch: false,
        text: I18n.t('settings.profile'),
        navigate: SETTING_PROFILE,
      },
    ];
    if (!user.current_order) {
      data.push({
        isSwitch: true,
        text: I18n.t('settings.active'),
      });
    }
    this.setState({
      data: [
        {
          title: I18n.t('settings.account'),
          icon: 'user',
          data,
        },
        {
          title: I18n.t('settings.about_app'),
          data: [
            {
              isSwitch: false,
              text: I18n.t('settings.terms_conditions'),
              navigate: TERMS_CONDITIONS,
              params: {isTerms: true},
            },
            {
              isSwitch: false,
              text: I18n.t('settings.privacy_policies'),
              navigate: TERMS_CONDITIONS,
              params: {isTerms: false},
            },
            {
              isVersion: true,
            },
          ],
        },
      ],
    });
  }

  onPressButton = (navigateTo, params) => {
    const {
      navigation: {navigate},
    } = this.props;
    navigate(navigateTo, params);
  };
  onPressSwitch = (value) => {
    const {user, checkInternet} = this.props;
    if (!checkInternet) {
      showMessage({
        message: I18n.t('warning'),
        description: I18n.t('internet'),
        type: 'danger',
      });
      return;
    }
    updateDriver(user.uid_driver, {online: value});
  };

  render() {
    const {
      checkInternet,
      user,
      navigation: {openDrawer},
    } = this.props;
    return (
      <Container>
        <Header
          name={I18n.t('title_header.settings')}
          actionDrawer={openDrawer}
          menu
        />
        <SettingsPageLayout
          user={user}
          {...this.state}
          checkInternet={checkInternet}
          onPressButton={this.onPressButton}
          onPressSwitch={this.onPressSwitch}
        />
      </Container>
    );
  }
}
const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  checkInternet: state.checkInternet.checkInternet,
});

export default connect(mapStateToProps)(SettingsPage);
