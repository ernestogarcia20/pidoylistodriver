import React from 'react';
import {
  View,
  StyleSheet,
  SectionList,
  TouchableOpacity,
  Switch,
} from 'react-native';
import Text from '../../../../../../shared/widgets/text';
import Icon from 'react-native-vector-icons/FontAwesome';
import palette from '../../../../../../shared/assets/palette';
import {APP_VERSION} from '../../../../../../utils/constants/global-constants';
const Layout = ({data, user, onPressButton, onPressSwitch}) => {
  const renderItem = ({item}) => {
    return (
      <View style={styles.item}>
        <Text color="rgba(0,0,0,0.6)" fontSize={14} weight="800">
          {!item.isVersion ? item.text : APP_VERSION}
        </Text>
        {!item.isSwitch && !item.isVersion && (
          <TouchableOpacity
            onPress={() => onPressButton(item.navigate, item.params)}
            style={styles.button}>
            <Icon name="chevron-right" color="rgba(0,0,0,0.7)" size={22} />
          </TouchableOpacity>
        )}
        {item.isSwitch && !!item.isVersion && (
          <Switch
            value={user.online}
            onValueChange={onPressSwitch}
            trackColor={{
              false: 'black',
              true: palette.active,
            }}
            thumbColor={user.online ? 'white' : 'white'}
          />
        )}
      </View>
    );
  };
  const renderHeader = ({section: {title, icon}}) => {
    return (
      <View style={styles.headerSection}>
        <View style={styles.contentInfo}>
          <Icon
            name={icon}
            style={{paddingHorizontal: 10}}
            color="rgba(0,0,0,0.8)"
            size={30}
          />
          <Text color="rgba(0,0,0,0.7)" fontSize={18} weight="800">
            {title}
          </Text>
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <SectionList
        style={styles.sectionList}
        sections={data}
        keyExtractor={(item, index) => item + index}
        renderItem={renderItem}
        renderSectionHeader={renderHeader}
        stickySectionHeadersEnabled={false}
      />
    </View>
  );
};
export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEE',
    alignContent: 'center',
  },
  headerSection: {
    height: 50,
    justifyContent: 'center',
    flexDirection: 'column',
    marginBottom: 6,
    marginTop: 15,
    backgroundColor: 'white',
  },
  contentInfo: {
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  item: {
    justifyContent: 'space-between',
    alignItems: 'center',
    flexDirection: 'row',
    height: 50,
    paddingHorizontal: '5%',
    backgroundColor: 'white',
  },
  button: {
    width: 30,
    justifyContent: 'center',
    alignItems: 'center',
  },
  line: {
    height: 1,
    backgroundColor: 'rgba(0,0,0,0.2)',
    position: 'absolute',
    bottom: 0,
    left: 15,
    right: 15,
  },
  sectionList: {
    paddingTop: '5%',
  },
});
