import React, {Component} from 'react';
import {connect} from 'react-redux';
import OrderDetailLayout from './component/order-detail-layout';
import {Linking} from 'react-native';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import LaunchNavigator from 'react-native-launch-navigator';
import {getKilometros} from '../../../utils/functions';
import {showMessage} from 'react-native-flash-message';
import ModalOptionMaps from '../../../shared/widgets/modal-option-maps';
import {
  findByOrderIdListener,
  updateOrderNewItem,
  finishOrder,
  updateDriver,
} from '../../services/services-dashboard';
import Sound from 'react-native-sound';
import AlertSound from '../../../shared/assets/sounds/alert.mp3';
import ModalAlert from 'react-native-awesome-alerts';

import I18n from '../../../../translation';

class OrderDetail extends Component {
  constructor(props) {
    super(props);
    this.state = {
      item: {},
      itemClient: {},
      data: {},
      error: '',
      latlng: {latitude: 0, longitude: 0},
      distance: 0,
      unit: 'km',
      point: 'A',
      region: {
        latitude: 0,
        longitude: 0,
      },
      loaderMap: false,
      loading: true,
      disableButtonOrder: false,
    };
    this.arrayholder = [];
    this.sound = null;
  }

  componentDidMount() {
    const {
      navigation: {getParam},
      dispatch,
    } = this.props;
    const item = getParam('item', {});
    Sound.setActive(true);
    Sound.setMode('Default');
    Sound.setCategory('Playback');
    this.suscribe = findByOrderIdListener(
      dispatch,
      item.uid_company,
      item.id_order,
    );
  }

  componentDidUpdate(prevProps) {
    const {geolocation, currentItem} = this.props;
    if (prevProps.geolocation !== geolocation) {
      this.checkZone();
    }
    if (prevProps.currentItem !== currentItem) {
      this.init();
    }
  }

  init = () => {
    const {currentItem, geolocation} = this.props;
    if (!currentItem) {
      return;
    }
    if (currentItem.traking === 4) {
      this.listenerCancel();
      return;
    }
    this.setState({data: currentItem}, () => {
      const region = {
        latitude: parseFloat(geolocation.lat),
        longitude: parseFloat(geolocation.long),
        longitudeDelta: 0.00102,
        latitudeDelta: 0.01,
      };
      this.setState({
        region,
        point: currentItem.traking === 1 ? 'A' : 'B',
      });
      this.filterPoints(currentItem.traking === 1 ? 'A' : 'B');
    });
  };

  listenerCancel = () => {
    const {user} = this.props;
    this.sound = new Sound(AlertSound, (error) => {
      if (error) {
        return;
      }
      this.sound.play(() => {
        // Success counts as getting to the end
        // Release when it's done so we're not using up resources
        this.sound.release();
      });
    });
    updateDriver(user.uid_driver, {
      reload_geolocation: false,
      current_order: '',
      current_uid_company: '',
    });
    this.setState({showAlert: true});
  };

  componentWillUnmount() {
    this.suscribe.off('value');
    if (!this.sound) {
      return;
    }
    this.sound.pause();
  }

  checkZone = () => {
    const {geolocation} = this.props;
    const {item, itemClient, point} = this.state;
    if (!item) {
      return;
    }
    const {latitud, longitud} = point === 'B' ? itemClient : item;
    const km = getKilometros(
      geolocation.lat,
      geolocation.long,
      latitud,
      longitud,
    );
    const mts = km * 1000;
    this.setState({disableButtonOrder: mts > 100});
  };

  filterPoints = (point) => {
    const {data} = this.state;

    if (!data) {
      return;
    }

    const item = data.points.find((e) => e.point === 'A');
    const itemClient = data.points.find((e) => e.point === 'B');
    const {latitud, longitud} = item;
    const {latitud: latitude, longitud: longitude} = itemClient;
    const latlng = {
      latitude: point === 'B' ? latitude : latitud,
      longitude: point === 'B' ? longitude : longitud,
    };
    this.setState(
      {
        item: {...item, description: data.description},
        latlng,
        loading: false,
        itemClient,
      },
      () => {
        this.checkZone();
      },
    );
  };

  changeOrFinishOrder = () => {
    const {point, disableButtonOrder} = this.state;
    const {checkInternet} = this.props;
    if (disableButtonOrder || !checkInternet) {
      showMessage({
        message: I18n.t('warning'),
        description: I18n.t(checkInternet ? 'block_button_map' : 'internet'),
        type: checkInternet ? 'warning' : 'danger',
        position: checkInternet ? 'bottom' : 'top',
      });
      return;
    }
    this.setState({loading: true});
    setTimeout(() => {
      if (point === 'A') {
        this.changeTraking(2).then(() => {
          this.setState({point: 'B'});
          this.filterPoints('B');
        });
        return;
      }
      this.finishOrder();
    }, 1000);
  };

  finishOrder = () => {
    const {
      navigation: {goBack},
      user,
    } = this.props;
    let {data: item} = this.state;
    if (!item) {
      return false;
    }
    this.changeTraking(3).then(() => {
      updateDriver(user.uid_driver, {
        reload_geolocation: false,
        current_order: '',
        current_uid_company: '',
      });
      finishOrder({...item, traking: 3}).then(() => {
        goBack();
        this.setState({loading: false});
      });
    });
  };

  changeTraking = (newTraking) => {
    const {data: item} = this.state;
    if (!item) {
      return false;
    }

    return updateOrderNewItem(
      item.uid_company,
      {...item, traking: newTraking},
      item.id_order,
    );
  };

  navigateMap = (appName) => {
    const {latlng, item} = this.state;
    this.setState({loaderMap: true});
    if (!latlng.longitude || !latlng.latitude || !item) {
      return;
    }
    if (!item.latitud || item.latitud === undefined) {
      return;
    }
    LaunchNavigator.isAppAvailable(appName).then((isWazeAvailable) => {
      if (!isWazeAvailable) {
        this.setState({loaderMap: false, showModalOption: false});
        return;
      }
      const app = appName;

      LaunchNavigator.navigate([latlng.latitude, latlng.longitude], {app})
        .then(() => this.setState({loaderMap: false, showModalOption: false}))
        .catch(() => this.setState({loaderMap: false, showModalOption: false}));
    });
  };

  handleChange = (name, value) => this.setState({[name]: value});

  phoneCall = (phone) => {
    Linking.openURL(`tel:${phone}`);
  };

  closePage = () => {
    const {
      navigation: {goBack},
    } = this.props;
    goBack();
  };

  render() {
    const {
      navigation: {goBack},
      checkInternet,
      geolocation,
      user,
      currentItem,
    } = this.props;
    const {showModalOption, showAlert} = this.state;
    return (
      <Container>
        <Header
          name={I18n.t('title_header.detail_order')}
          actionDrawer={() => goBack()}
        />
        <OrderDetailLayout
          checkInternet={checkInternet}
          {...this.state}
          user={user}
          changeOrFinishOrder={this.changeOrFinishOrder}
          geolocation={geolocation}
          setValue={this.handleChange}
          onPressItem={this.onPressItem}
          navigateMap={this.navigateMap}
          currentItem={currentItem}
          phoneCall={this.phoneCall}
        />
        <ModalOptionMaps
          show={showModalOption}
          onSelect={this.navigateMap}
          closeModal={() => this.setState({showModalOption: false})}
        />
        <ModalAlert
          show={showAlert}
          showProgress={false}
          title={I18n.t('modal_alert.order_cancel')}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={true}
          confirmText={I18n.t('buttons.ok')}
          confirmButtonColor="#DD6B55"
          onConfirmPressed={() => this.setState({showAlert: false})}
          onDismiss={this.closePage}
        />
      </Container>
    );
  }
}
const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  checkInternet: state.checkInternet.checkInternet,
  geolocation: state.myGeolocation.geolocation,
  currentItem: state.orderList.currentOrder,
});

export default connect(mapStateToProps)(OrderDetail);
