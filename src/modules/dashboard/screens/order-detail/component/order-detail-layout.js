import React, {Fragment} from 'react';
import {
  View,
  StyleSheet,
  Dimensions,
  ScrollView,
  TouchableOpacity,
} from 'react-native';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import MapViewDirections from '../../../../shared/widgets/maps-directions';
import Text from '../../../../shared/widgets/text';
import palette from '../../../../shared/assets/palette';
import Loader from '../../../../shared/widgets/loader';
import FabButton from '../../../../shared/widgets/fab-button';
import LaunchNavigator from 'react-native-launch-navigator';
import {FloatingAction} from 'react-native-floating-action';
import Icon from 'react-native-vector-icons/MaterialIcons';
import I18n from '../../../../../translation';
import customStyle from '../../../../shared/assets/style-map';
import {API_KEY_MAP} from '../../../../utils/constants/global-constants';
let mapView = null;
const Layout = ({
  distance,
  unit,
  data,
  item,
  setValue,
  loading,
  latlng,
  changeOrFinishOrder,
  region,
  navigateMap,
  disableButtonOrder,
  point,
  phoneCall,
  currentItem,
  itemClient,
}) => {
  const onReadyMap = (result) => {
    //console.log(`Distance: ${result.distance} km`);
    //console.log(`Duration: ${result.duration} min.`);
    const unitMap = result.distance < 1 ? 'm' : 'km';
    const distanceMap =
      result.distance < 1 ? result.distance * 1000 : result.distance;
    setValue('unit', unitMap);
    setValue(
      'distance',
      unitMap === 'm' ? distanceMap.toFixed(0) : distanceMap.toFixed(2),
    );
    if (!mapView) {
      return null;
    }
    mapView.fitToCoordinates(result.coordinates, {
      edgePadding: {
        right: width / 20,
        bottom: height / 20,
        left: width / 20,
        top: height / 20,
      },
    });
  };
  return (
    <View style={styles.container}>
      {currentItem && !loading && currentItem.traking < 3 ? (
        <ScrollView contentContainerStyle={styles.scroll}>
          <View style={styles.map}>
            {latlng.latitude !== 0 && region.latitude !== 0 && (
              <MapView
                provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                style={styles.map}
                ref={(c) => (mapView = c)}
                showsUserLocation
                customMapStyle={customStyle}
                loadingBackgroundColor="black"
                initialRegion={region}>
                <Marker
                  coordinate={region}
                  icon={require('../../../../shared/assets/icon/pick_driver.png')}
                />
                <Marker
                  coordinate={latlng}
                  icon={
                    point === 'B'
                      ? require('../../../../shared/assets/icon/pick_home.png')
                      : require('../../../../shared/assets/icon/pick_merchant.png')
                  }
                />

                <MapViewDirections
                  onReady={onReadyMap}
                  origin={region}
                  destination={latlng}
                  showsMyLocationButton
                  optimizeWaypoints
                  splitWaypoints
                  apikey={API_KEY_MAP}
                  strokeWidth={5}
                  strokeColor="hotpink"
                />
              </MapView>
            )}

            <View style={styles.absoluteButton}>
              <FabButton
                nameIcon="location-arrow"
                onPress={() => setValue('showModalOption', true)}
              />
            </View>
          </View>
          <View style={styles.contentDetail}>
            <View style={styles.contentHeaderDetail}>
              <View style={styles.contentColumnDetail}>
                <View>
                  <Text fontSize={16} weight="Light" align="left">
                    {point === 'A'
                      ? I18n.t('order_detail.merchant.merchant_name')
                      : I18n.t('order_detail.client.client_name')}
                  </Text>
                  <Text
                    fontSize={24}
                    weight="SemiBold"
                    color="rgba(0,0,0,0.7)"
                    align="left">
                    {point === 'A' ? item.name : itemClient.name}
                  </Text>
                </View>
                <View style={styles.contentRowidOrder}>
                  <View style={styles.textPadding}>
                    <Text fontSize={16} weight="Light" align="center">
                      {I18n.t('order_detail.price_delivery')}
                    </Text>
                    <Text
                      align="center"
                      fontSize={22}
                      color="#149B72"
                      weight="Bold">
                      {parseFloat(data.price).toFixed(2)}$
                    </Text>
                  </View>

                  <View style={styles.textPadding}>
                    <Text fontSize={16} weight="Light" align="center">
                      {I18n.t('order_detail.number_order')}
                    </Text>
                    <Text
                      align="center"
                      fontSize={22}
                      color={'rgba(0,0,0,0.7)'}
                      weight="Bold">
                      #{String(data.id_order).padStart(5, '0')}
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.absoluteDistance}>
                <Text fontSize={18} weight="Medium">
                  {distance} {unit}
                </Text>
              </View>
            </View>
            {item.phone ? (
              <View>
                <View style={styles.descriptionOrder}>
                  <View style={styles.alingStart}>
                    <Text weight="Light" color="rgba(0,0,0,0.8)" fontSize={16}>
                      {I18n.t('order_detail.merchant.info_merchant')}
                    </Text>
                    <View style={styles.separator} />

                    <Text
                      color="rgba(0,0,0,0.8)"
                      fontSize={16}
                      weight="SemiBold"
                      align="justify">
                      {item.phone}
                    </Text>
                  </View>
                  <FabButton
                    nameIcon="phone"
                    onPress={() => phoneCall(item.phone)}
                  />
                </View>
                {point === 'B' ? (
                  <View style={styles.descriptionOrder}>
                    <View style={styles.alingStart}>
                      <Text
                        weight="Light"
                        color="rgba(0,0,0,0.8)"
                        fontSize={16}>
                        {I18n.t('order_detail.client.info_client')}
                      </Text>
                      <View style={styles.separator} />

                      <Text
                        color="rgba(0,0,0,0.8)"
                        fontSize={16}
                        weight="SemiBold"
                        align="justify">
                        {itemClient.phone}
                      </Text>
                    </View>
                    <FabButton
                      nameIcon="phone"
                      onPress={() => phoneCall(itemClient.phone)}
                    />
                  </View>
                ) : null}
              </View>
            ) : null}
            {point === 'B' && (
              <View style={styles.descriptionOrder}>
                <View style={styles.alingStart}>
                  <Text weight="Light" color="rgba(0,0,0,0.7)" fontSize={16}>
                    {I18n.t('order_detail.client.indications')}
                  </Text>
                  <View style={styles.separator} />
                  <Text
                    color="rgba(0,0,0,0.8)"
                    fontSize={16}
                    weight="SemiBold"
                    align="justify">
                    {item.description}
                  </Text>
                </View>
              </View>
            )}
          </View>
        </ScrollView>
      ) : (
        <View style={styles.contentLoader}>
          <Loader />
        </View>
      )}
      {currentItem && !loading && currentItem.traking < 3 && (
        <View style={styles.contentButton}>
          <TouchableOpacity
            style={
              disableButtonOrder
                ? styles.statusButtonDisable
                : styles.statusButton
            }
            onPress={changeOrFinishOrder}>
            <Text color="white">
              {point === 'A'
                ? I18n.t('order_received')
                : I18n.t('order_delivered')}
            </Text>
          </TouchableOpacity>
        </View>
      )}
    </View>
  );
};
export default Layout;
const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEE',
  },
  contentRowidOrder: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
    paddingHorizontal: '5%',
  },
  contentColumnDetail: {flex: 1},
  absoluteDistance: {position: 'absolute', right: 10, top: 30},
  contentLoader: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
  map: {
    height: 250,
    backgroundColor: 'black',
    zIndex: 1,
  },
  scroll: {
    paddingBottom: '15%',
  },
  absoluteButton: {
    position: 'absolute',
    bottom: -65,
    right: -30,
    zIndex: 1,
    backgroundColor: 'transparent',
    width: 100,
    height: 100,
  },
  contentDetail: {
    zIndex: -1,
  },
  alingStart: {
    alignItems: 'flex-start',
  },
  separator: {
    marginVertical: 5,
  },
  textPadding: {paddingTop: 5},
  contentHeaderDetail: {
    paddingVertical: 20,
    paddingHorizontal: 15,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'flex-start',
    backgroundColor: 'white',
    marginBottom: 7,
  },
  descriptionOrder: {
    alignItems: 'center',
    justifyContent: 'space-between',
    paddingHorizontal: 15,
    paddingTop: '3%',
    paddingBottom: '6%',
    backgroundColor: 'white',
    marginVertical: 5,
    flexDirection: 'row',
  },
  contentButton: {
    position: 'absolute',
    bottom: 10,
    left: 0,
    right: 0,
    zIndex: 1,
    paddingHorizontal: '20%',
  },
  statusButton: {
    backgroundColor: palette.primaryColor,
    height: 40,
    paddingHorizontal: '15%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  statusButtonDisable: {
    backgroundColor: palette.progressStyle.foregroundColor,
    height: 40,
    paddingHorizontal: '15%',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 10,
  },
  row: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
});
