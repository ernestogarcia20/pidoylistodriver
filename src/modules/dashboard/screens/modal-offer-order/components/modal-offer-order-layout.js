import React from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  ActivityIndicator,
  ScrollView,
  StatusBar,
  SafeAreaView,
} from 'react-native';
import palette from '../../../../shared/assets/palette';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Text from '../../../../shared/widgets/text';
import MapView, {PROVIDER_GOOGLE, Marker} from 'react-native-maps';
import MapViewDirections from '../../../../shared/widgets/maps-directions';
import {API_KEY_MAP} from '../../../../utils/constants/global-constants';
import WaitingByMerchant from '../../../../shared/assets/lottie/waiting_driver_by_merchant.json';
import LottieView from 'lottie-react-native';
import I18n from '../../../../../translation';
let mapView = null;
const Layout = ({
  loaderZoom,
  waitingByAccepted,
  locationDriver,
  locationMerchant,
  locationClient,
  orderSelected,
  price,
  priceOffer,
  buttonOffer1,
  buttonOffer2,
  buttonOffer3,
  setValue,
  insertToOffer,
  cancelOfferOrder,
  closeModal,
  adressNameA,
  adressNameB,
  distanceA,
  unitA,
  descriptionOrder,
  distanceB,
  unitB,
  duration,
  params,
}) => {
  const onReadyMap = (result) => {
    const unit = result.distance < 1 ? 'm' : 'km';
    const distance =
      result.distance < 1 ? result.distance * 1000 : result.distance;
    setValue('unit', unit);
    setValue('distance', distance.toFixed(2));
    if (!mapView) {
      return null;
    }
    mapView.fitToCoordinates(result.coordinates, {
      edgePadding: {
        right: width / 20,
        bottom: height / 20,
        left: width / 20,
        top: height / 20,
      },
    });
  };
  return (
    <SafeAreaView style={[styles.modalContainer]}>
      <StatusBar hidden animated showHideTransition="fade" />
      <ScrollView contentContainerStyle={styles.contentScroll} bounces={false}>
        <View style={styles.loaderZoom}>
          {!waitingByAccepted && (
            <View style={styles.content}>
              <TouchableOpacity style={styles.cancel} onPress={closeModal}>
                <Icon name="times" size={28} />
              </TouchableOpacity>
              <Text fontSize={18} weight="SemiBold" color="rgba(0,0,0,0.8)">
                {I18n.t('modal_offer.trip_complete')}
              </Text>
              <View style={styles.contentMap}>
                {locationDriver.latitude !== 0 &&
                  locationClient.latitude !== 0 &&
                  locationMerchant.latitude !== 0 && (
                    <MapView
                      provider={PROVIDER_GOOGLE} // remove if not using Google Maps
                      style={styles.map}
                      ref={(mapRef) => (mapView = mapRef)}
                      showsUserLocation
                      loadingBackgroundColor="white"
                      initialRegion={locationDriver}>
                      <Marker
                        coordinate={locationDriver}
                        icon={require('../../../../shared/assets/icon/pick_driver.png')}
                      />
                      <Marker
                        coordinate={locationMerchant}
                        icon={require('../../../../shared/assets/icon/pick_merchant.png')}
                      />
                      <Marker
                        coordinate={locationClient}
                        icon={require('../../../../shared/assets/icon/pick_home.png')}
                      />
                      <MapViewDirections
                        onReady={onReadyMap}
                        origin={locationDriver}
                        destination={locationMerchant}
                        showsMyLocationButton
                        precision="low"
                        optimizeWaypoints
                        splitWaypoints
                        apikey={API_KEY_MAP}
                        strokeWidth={5}
                        strokeColor={palette.status.in_progress_merchant}
                      />
                      <MapViewDirections
                        origin={locationMerchant}
                        destination={locationClient}
                        showsMyLocationButton
                        optimizeWaypoints
                        splitWaypoints
                        precision="low"
                        apikey={API_KEY_MAP}
                        strokeWidth={5}
                        strokeColor={palette.status.in_progress_client}
                      />
                    </MapView>
                  )}
              </View>
              <View style={styles.contentDetailPoint}>
                <View style={styles.contentPoints}>
                  <View style={styles.contentRowPoint}>
                    <View style={styles.contentPointA}>
                      <Text color="white" fontSize={16} weight="SemiBold">
                        A
                      </Text>
                    </View>
                    <View style={styles.contentInputPoint}>
                      <View style={styles.contentWrapPoint}>
                        <Text align="left" weight="SemiBold">
                          {adressNameA}
                          <Text fontSize={16} weight="Light">
                            {` ~ ${duration}`} min{' '}
                            <Text fontSize={14} weight="Light">
                              ({distanceA} {unitA})
                            </Text>
                          </Text>
                        </Text>
                      </View>
                    </View>
                  </View>
                  <View style={styles.contentDot}>
                    <View style={styles.dot} />
                    <View style={styles.dot} />
                  </View>
                  <View style={styles.contentRowPoint}>
                    <View style={styles.contentPointB}>
                      <Text color="white" fontSize={16} weight="SemiBold">
                        B
                      </Text>
                    </View>
                    <View style={styles.contentInputPoint}>
                      <View style={styles.contentWrapPoint}>
                        <Text align="left" weight="SemiBold">
                          {adressNameB}
                          {' ~ '}
                          <Text
                            align="left"
                            weight="Light"
                            fontSize={16}
                            color="rgba(0,0,0,0.9)">{`${distanceB} ${unitB}`}</Text>
                        </Text>
                      </View>
                    </View>
                  </View>
                </View>
              </View>
              <View style={styles.contentDescription}>
                <Text align="left" weight="Bold">
                  {I18n.t('modal_offer.description')}
                </Text>
                <View style={styles.wrapDescription}>
                  <Text align="left" weight="Light">
                    {descriptionOrder}
                  </Text>
                </View>
              </View>
              <View style={styles.contentOfferButtons}>
                <View style={styles.contentButton}>
                  <TouchableOpacity
                    style={styles.buttonOffer}
                    onPress={() => {
                      insertToOffer(orderSelected.price);
                    }}>
                    <Text color="white" fontSize={18} weight="Light">
                      {I18n.t('modal_offer.accept_trip_to')}{' '}
                      <Text color="white" fontSize={18} weight="Bold">
                        $ {parseFloat(orderSelected.price).toFixed(2)}
                      </Text>
                    </Text>
                  </TouchableOpacity>
                </View>
                <View style={styles.conetentText}>
                  <Text fontSize={18} weight="Light">
                    {I18n.t('modal_offer.offer_by')}
                  </Text>
                  <View style={styles.contentOfferPrices}>
                    <TouchableOpacity
                      style={styles.buttonOfferRow}
                      onPress={() => {
                        insertToOffer(buttonOffer1);
                      }}>
                      <Text color="white" weight="Bold" fontSize={18}>
                        $ {buttonOffer1}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.buttonOfferRow}
                      onPress={() => {
                        insertToOffer(buttonOffer2);
                      }}>
                      <Text color="white" weight="Bold" fontSize={18}>
                        $ {buttonOffer2}
                      </Text>
                    </TouchableOpacity>
                    <TouchableOpacity
                      style={styles.buttonOfferRow}
                      onPress={() => {
                        insertToOffer(buttonOffer3);
                      }}>
                      <Text color="white" weight="Bold" fontSize={18}>
                        $ {buttonOffer3}
                      </Text>
                    </TouchableOpacity>
                  </View>
                </View>
              </View>
            </View>
          )}
          {waitingByAccepted && (
            <View style={styles.content}>
              <View style={styles.contentWaiting}>
                <Text fontSize={24} weight="SemiBold">
                  {I18n.t('modal_offer.waiting')}
                </Text>
              </View>
              <View style={styles.contentAnimation}>
                <LottieView source={WaitingByMerchant} autoPlay loop={false} />
              </View>
              <View style={styles.contentOfferPriceText}>
                <Text fontSize={24} weight="SemiBold">
                  {I18n.t('modal_offer.offered')}{' '}
                  <Text
                    color={palette.status.finish}
                    fontSize={24}
                    weight="Bold">
                    $ {parseFloat(priceOffer).toFixed(2)}
                  </Text>
                </Text>
                <Text fontSize={24} weight="Light">
                  {I18n.t('modal_offer.reviewing')}
                </Text>
              </View>
              <View style={styles.contentExit}>
                <TouchableOpacity onPress={cancelOfferOrder}>
                  <Text color={palette.inactive} weight="Medium" fontSize={18}>
                    {I18n.t('buttons.cancel_request')}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
          )}
        </View>
      </ScrollView>
    </SafeAreaView>
  );
};

export default Layout;
const {width, height} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    width: '100%',
    minHeight: 45,
    borderRadius: 2,
    paddingHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#cacaca',
    paddingVertical: 4,
  },
  wrapDescription: {flexDirection: 'row', flexWrap: 'nowrap'},
  contentOfferButtons: {flex: 0.5, justifyContent: 'center'},
  contentScroll: {flex: 1},
  loaderZoom: {
    flex: 1,
    backgroundColor: 'white',
    borderTopEndRadius: 15,
    borderTopStartRadius: 15,
  },
  cancel: {
    flex: 0.1,
    alignItems: 'flex-end',
    marginHorizontal: 20,
    justifyContent: 'center',
  },
  contentDescription: {
    height: 90,
    justifyContent: 'flex-start',
    paddingLeft: '5%',
    backgroundColor: 'rgba(0,0,0,0.04)',
    marginTop: 15,
    paddingVertical: 5,
  },
  contentWrapPoint: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  contentWaiting: {flex: 0.1, justifyContent: 'center'},
  contentDetailPoint: {
    marginTop: 20,
    flex: 0.25,
    zIndex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  contentDot: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: 12,
    marginLeft: 8,
  },
  dot: {
    height: 7,
    width: 7,
    backgroundColor: 'black',
    borderRadius: 50,
    marginVertical: 5,
  },
  contentInputPoint: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 10,
    flexWrap: 'nowrap',
  },
  contentPointA: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    backgroundColor: palette.status.in_progress_merchant,
    borderWidth: 0,
    borderRadius: 50,
  },
  contentPointB: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    backgroundColor: palette.status.in_progress_client,
    borderWidth: 0,
    borderRadius: 50,
  },
  contentRowPoint: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: -1,
  },
  contentPoints: {flex: 1},
  contentZoomLoader: {
    position: 'absolute',
    top: 0,
    bottom: 0,
    left: 0,
    right: 0,
    zIndex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: 'rgba(0,0,0,0.8)',
  },
  contentExit: {flex: 0.1},
  contentOfferPriceText: {flex: 0.3},
  buttonOfferRow: {
    width: width / 3.5,
    height: 45,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 20,
    backgroundColor: '#149B72',
  },
  contentAnimation: {flex: 0.6},
  contentButton: {justifyContent: 'center'},
  image: {width: 40, height: 40, marginBottom: 5},
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  conetentText: {marginTop: '5%', justifyContent: 'center'},
  contentOfferPrices: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: '5%',
    marginHorizontal: '4%',
  },
  closeModal: {
    position: 'absolute',
    bottom: 15,
    left: 0,
    right: 0,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {flex: 1},
  modalContainer: {
    backgroundColor: 'rgba(0,0,0,0.4)',
    flex: 1,
  },
  map: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    zIndex: 1,
  },
  contentMap: {
    flex: 0.4,
    backgroundColor: 'white',
    justifyContent: 'center',
    zIndex: 1,
    marginTop: 10,
  },
  buttonOffer: {
    height: 50,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: '10%',
    borderRadius: 20,
    backgroundColor: palette.status.in_progress_client,
  },
});
