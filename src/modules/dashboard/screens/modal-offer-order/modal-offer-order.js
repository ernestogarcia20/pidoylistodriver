import React, {Component} from 'react';

import ModalOfferOrderLayout from './components/modal-offer-order-layout';
import {Alert, View, StyleSheet} from 'react-native';
import {connect} from 'react-redux';
import {
  getOrderByKey,
  removeDriverToOfferOrder,
  setDriverToOfferOrder,
  setDispatch,
  removeOrderTemp,
} from '../../services/services-dashboard';
import {ORDER_DETAIL} from '../../../utils/constants/constants-navigate';
import {REPLACE_CURRENT_SCREEN} from '../../../utils/constants/global-constants';
import I18n from '../../../../translation';
import {getKilometros} from '../../../utils/functions';
import moment from 'moment';
import {SET_HANDLE_BACK} from '../../../../store/reducers/handle-back';
import Spinner from 'react-native-loading-spinner-overlay';
import Loader from '../../../shared/widgets/loader';
import ModalAlert from 'react-native-awesome-alerts';

class ModalOfferOrder extends Component {
  state = {
    show: false,
    preSelectedItem: [],
    selectedItem: [],
    data: [],
    keyword: '',
    unit: '',
    distance: '',
    waitingByAccepted: false,
    priceOffer: 0,
    buttonOffer1: 0,
    buttonOffer2: 0,
    buttonOffer3: 0,
    loaderZoom: false,
    itemOrderOffer: {},
    selected: false,
    orderSelected: {},
    adressNameA: '',
    adressNameB: '',
    distanceA: 0,
    unitA: 'km',
    distanceB: 0,
    unitB: 'km',
    duration: 0,
    descriptionOrder: '',
  };
  componentDidMount() {
    // locationMerchant, locationClient, orderSelected: item
    const {
      navigation: {getParam},
      geolocation,
      params,
    } = this.props;

    const locationClient = getParam('locationClient', {});
    const locationMerchant = getParam('locationMerchant', {});
    const orderSelected = getParam('orderSelected', {});

    const {
      latitud: lat_a,
      longitud: long_a,
      address: address_name_a,
    } = orderSelected.points.find((e) => e.point === 'A');

    const {
      latitud: lat_b,
      longitud: long_b,
      address: address_name_b,
    } = orderSelected.points.find((e) => e.point === 'B');

    const {long: myLong, lat: myLat} = geolocation;
    if (!lat_a || !myLong || !long_a || !myLat) {
      return;
    }

    const speed = 30;
    const totalKm_a = getKilometros(myLat, myLong, lat_a, long_a);
    const totalKm_b = getKilometros(lat_a, long_a, lat_b, long_b);
    const totalDistanceA = totalKm_a < 1 ? totalKm_a * 1000 : totalKm_a;
    const time = totalKm_a < 1 ? 1 : parseInt((totalDistanceA / speed) * 60);
    const totalTime = time < 1 ? 1 : time;

    const offerBy = parseFloat(params.offer_by || 0.25);
    const offer1 = parseFloat(
      parseFloat(orderSelected.price) + offerBy,
    ).toFixed(2);
    const offer2 = parseFloat(
      parseFloat(orderSelected.price) + offerBy * 2,
    ).toFixed(2);
    const offer3 = parseFloat(
      parseFloat(orderSelected.price) + offerBy * 3,
    ).toFixed(2);

    this.setState({
      locationClient,
      locationMerchant,
      orderSelected,
      buttonOffer1: offer1,
      buttonOffer2: offer2,
      buttonOffer3: offer3,
      distanceA: totalKm_a < 1 ? totalKm_a * 1000 : totalKm_a,
      unitA: totalKm_a < 1 ? 'm' : 'km',
      distanceB: totalKm_b < 1 ? totalKm_b * 1000 : totalKm_b,
      unitB: totalKm_b < 1 ? 'm' : 'km',
      adressNameA: address_name_a,
      adressNameB: address_name_b,
      duration: totalTime,
      descriptionOrder: orderSelected.description,
    });
  }
  closeModal = () => {
    const {
      navigation: {goBack},
    } = this.props;
    goBack();
  };
  closeModalWithAlert = (noAvailable) => {
    this.setState({showAlert: true});
  };
  handleAccepted = (item) => {
    const {
      dispatch,
      navigation: {navigate},
    } = this.props;
    navigate(ORDER_DETAIL, {item});
    dispatch({
      key: ORDER_DETAIL,
      type: REPLACE_CURRENT_SCREEN,
      routeName: ORDER_DETAIL,
      params: {item},
    });
  };

  componentWillUnmount() {
    const {dispatch} = this.props;
    setDispatch(dispatch, true, SET_HANDLE_BACK);
    clearInterval(this.clockCall);
    this.clockCall = null;
    removeOrderTemp(dispatch);
    if (!this.unsubscribe) {
      return;
    }
    this.unsubscribe.off('child_removed');
  }

  componentDidUpdate(prevProps) {
    const {orderTemp} = this.props;
    if (prevProps.orderTemp !== orderTemp) {
      this.orderTempChange();
    }
  }

  startTimer = () => {
    let band = false;
    this.clockCall = setInterval(() => {
      const {orderSelected} = this.state;
      if (!orderSelected) {
        return clearInterval(this.clockCall);
      }
      const timeCreated = moment(
        orderSelected.date_created,
        'YYYY-MM-DD HH:mm:ss',
      );
      const now = moment();
      const diff = now.diff(timeCreated, 'minutes');

      if (diff >= 5 && diff && this.clockCall) {
        clearInterval(this.clockCall);
        this.clockCall = null;
        if (band) {
          return;
        }
        band = true;
        this.timeOutOrder();
        return;
      }
    }, 1000);
  };

  timeOutOrder = () => {
    this.closeModalWithAlert();
  };

  orderTempChange = () => {
    const {orderTemp, user} = this.props;
    const {orderSelected} = this.state;

    if (user.current_order) {
      this.setState({selected: true});
      return this.handleAccepted({
        id_order: orderSelected.id_order,
        uid_company: orderSelected.uid_company,
      });
    }

    if (!orderTemp && !user.current_order) {
      return this.closeModalWithAlert();
    }

    if (orderTemp.remove) {
      return this.closeModalWithAlert();
    }
  };

  showModal = () => this.setState({show: true});

  insertToOffer = (price) => {
    const {user, dispatch, geolocation} = this.props;
    const {
      orderSelected: {key_order, date_created},
    } = this.state;
    const timeCreated = moment(date_created, 'YYYY-MM-DD HH:mm:ss');
    const now = moment();
    const timerCurrent = now.diff(timeCreated, 'minutes');
    if (timerCurrent >= 5) {
      this.timeOutOrder();
      return;
    }
    const initialObj = {
      driver_name: `${user.first_name} ${user.last_name}`,
      latitude: geolocation.lat,
      longitude: geolocation.long,
      price: price,
      rating: user.rating ? user.rating : 5,
      trips: user.trips_made ? user.trips_made : 0,
      url_image: user.selfieUrl,
      uid_driver: user.uid_driver,
    };
    this.setState({loaderZoom: true});
    this.unsubscribe = getOrderByKey(dispatch, key_order);
    setDriverToOfferOrder(key_order, initialObj).then(() => {
      this.startTimer();
      setDispatch(dispatch, false, SET_HANDLE_BACK);
      setTimeout(() => {
        this.setState({
          waitingByAccepted: true,
          loaderZoom: false,
          priceOffer: price,
          itemOrderOffer: initialObj,
        });
      }, 500);
    });
  };

  cancelOfferOrder = () => {
    const {user, dispatch} = this.props;
    const {
      orderSelected: {key_order},
    } = this.state;
    this.setState({loaderZoom: true});
    getOrderByKey(dispatch, key_order);
    removeDriverToOfferOrder(key_order, user.uid_driver)
      .then(() => {
        this.closeModal();
        this.setState({loaderZoom: false});
      })
      .catch(() => {
        this.setState({loaderZoom: false});
      });
  };
  handleChange = (name, value) => this.setState({[name]: value});
  render() {
    const {geolocation, params} = this.props;
    const {
      waitingByAccepted,
      priceOffer,
      loaderZoom,
      locationMerchant,
      locationClient,
      orderSelected,
      showAlert,
    } = this.state;
    const locationDriver = {
      longitude: parseFloat(geolocation.long),
      latitude: parseFloat(geolocation.lat),
      longitudeDelta: 0.00102,
      latitudeDelta: 0.01,
    };
    if (
      !orderSelected ||
      !locationMerchant ||
      !locationClient ||
      !locationDriver
    ) {
      return <View />;
    }
    return (
      <View style={styles.container}>
        <ModalOfferOrderLayout
          loaderZoom={loaderZoom}
          waitingByAccepted={waitingByAccepted}
          locationDriver={locationDriver}
          locationMerchant={locationMerchant}
          locationClient={locationClient}
          orderSelected={orderSelected}
          priceOffer={priceOffer}
          setValue={this.handleChange}
          insertToOffer={this.insertToOffer}
          cancelOfferOrder={this.cancelOfferOrder}
          closeModal={this.closeModal}
          params={params}
          {...this.state}
        />
        <ModalAlert
          show={showAlert}
          showProgress={false}
          title={I18n.t('order_not_available')}
          closeOnTouchOutside={true}
          closeOnHardwareBackPress={false}
          showCancelButton={false}
          showConfirmButton={true}
          confirmText={I18n.t('buttons.ok')}
          confirmButtonColor="#DD6B55"
          onConfirmPressed={() => this.setState({showAlert: false})}
          onDismiss={this.closeModal}
        />
        <Spinner
          visible={loaderZoom}
          overlayColor="rgba(0,0,0,0.85)"
          size="large"
          animation="fade"
          customIndicator={
            <View style={styles.contentIndicator}>
              <Loader
                text={I18n.t('loader.wait_moment')}
                textColor="white"
                colorIndicator="white"
                fontSize={20}
              />
            </View>
          }
          textStyle={styles.spinnerTextStyle}
        />
      </View>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  geolocation: state.myGeolocation.geolocation,
  orderTemp: state.orderList.orderTemp,
  params: state.params,
});
export default connect(mapStateToProps)(ModalOfferOrder);
const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  spinnerTextStyle: {
    color: '#FFF',
  },
  contentIndicator: {flex: 1, justifyContent: 'center', alignItems: 'center'},
});
