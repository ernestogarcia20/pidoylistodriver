import React, {PureComponent} from 'react';
import {View, StyleSheet, TouchableOpacity, Dimensions} from 'react-native';
import Text from '../../../../shared/widgets/text';
import * as Progress from 'react-native-progress';
import palette from '../../../../shared/assets/palette';
import moment from 'moment';
import {getKilometros} from '../../../../utils/functions';
import I18n from '../../../../../translation';
import Sound from 'react-native-sound';
import SoundOrder from '../../../../shared/assets/sounds/definite.mp3';

class Item extends PureComponent {
  state = {
    timer: 1,
    restTotalTimer: 0,
    adressNameA: '',
    adressNameB: '',
    distanceA: 0,
    unitA: 'km',
    distanceB: 0,
    unitB: 'km',
    duration: 0,
    item: {name: ''},
  };

  clockCall = null;

  componentDidMount() {
    const {item} = this.props;
    this.init();
    if (item.existOrder) {
      return;
    }
    let sound = null;
    Sound.setMode('Default');
    Sound.setCategory('Playback');
    sound = new Sound(SoundOrder, (error) => {
      if (error) {
        return;
      }
      sound.play(() => {
        // Success counts as getting to the end
        // Release when it's done so we're not using up resources
        sound.release();
      });
    });
  }

  init = () => {
    const {item} = this.props;
    const {date_created} = item;
    const timeCreated = moment(date_created, 'YYYY-MM-DD HH:mm:ss');
    const now = moment();
    const second = 5 * 60;
    const timerCurrent = now.diff(timeCreated, 'second');
    const t = ((100 / second) * timerCurrent) / 100;
    const total = 1 - t;
    this.setState(
      {
        restTotalTimer: 0.0033,
        timer: total,
        item: item.points.find((e) => e.point === 'A'),
      },
      () => {
        this.setLocation();
        if (item.existOrder) {
          return;
        }
        this.startTimer();
      },
    );
  };

  setLocation() {
    const {item, geolocation} = this.props;
    if (!geolocation || !item) {
      return;
    }
    const {
      latitud: lat_a,
      longitud: long_a,
      address: address_name_a,
    } = item.points.find((e) => e.point === 'A');
    const {
      latitud: lat_b,
      longitud: long_b,
      address: address_name_b,
    } = item.points.find((e) => e.point === 'B');
    const {long: myLong, lat: myLat} = geolocation;
    if (!lat_a || !myLong || !long_a || !myLat) {
      return;
    }
    const speed = 30;
    const totalKm_a = getKilometros(myLat, myLong, lat_a, long_a);
    const totalKm_b = getKilometros(lat_a, long_a, lat_b, long_b);
    const totalDistanceA = totalKm_a < 1 ? totalKm_a * 1000 : totalKm_a;
    const time = totalKm_a < 1 ? 1 : parseInt((totalDistanceA / speed) * 60);
    const totalTime = time < 1 ? 1 : time;
    this.setState({
      distanceA: totalKm_a < 1 ? totalKm_a * 1000 : totalKm_a,
      unitA: totalKm_a < 1 ? 'm' : 'km',
      distanceB: totalKm_b < 1 ? totalKm_b * 1000 : totalKm_b,
      unitB: totalKm_b < 1 ? 'm' : 'km',
      adressNameA: address_name_a,
      adressNameB: address_name_b,
      duration: totalTime,
    });
  }

  startTimer = () => {
    this.clockCall = setInterval(() => {
      this.decrementClock();
    }, 1000);
  };

  componentDidUpdate(prevP) {
    const {item, geolocation} = this.props;
    const {long: myLong, lat: myLat} = geolocation;
    if (myLat !== prevP.geolocation.lat || myLong !== prevP.geolocation.long) {
      this.setLocation();
    }
    if (prevP.item.date_created !== item.date_created) {
      clearInterval(this.clockCall);
      this.clockCall = null;
      this.init();
    }
  }

  decrementClock = () => {
    const {item, removeItem} = this.props;
    if (this.state.timer <= 0 && this.clockCall) {
      this.clockCall = null;
      removeItem(item);
      clearInterval(this.clockCall);
    }
    this.setState((prevstate) => ({
      timer: prevstate.timer - this.state.restTotalTimer,
    }));
  };

  componentWillUnmount() {
    clearInterval(this.clockCall);
  }

  render() {
    const {
      onPressItem,
      onPressItemIgnore,
      item: itemList,
      textButton,
    } = this.props;
    const {
      timer,
      distanceA,
      unitA,
      distanceB,
      unitB,
      adressNameA,
      adressNameB,
      item,
      duration,
    } = this.state;
    const {price, existOrder} = itemList;
    const {name} = item;
    return (
      <View
        style={[itemList.existOrder ? styles.itemExist : styles.item]}
        key={itemList.key_order}>
        <View style={styles.positionProgress}>
          {timer !== 1 && !existOrder && (
            <Progress.Bar
              progress={timer}
              borderColor="black"
              borderWidth={0}
              width={width - 20}
              borderRadius={5}
              height={10}
              color={palette.primaryColor}
            />
          )}
        </View>
        <View style={styles.contentDetail}>
          <View style={styles.contentNameCompany}>
            <View style={styles.nameCompany}>
              <Text align="left" fontSize={20} weight="SemiBold">
                {name}
              </Text>
            </View>
            <View style={styles.contentPrice}>
              {itemList.existOrder && (
                <View>
                  <Text
                    align="center"
                    fontSize={22}
                    color={'rgba(0,0,0,0.7)'}
                    weight="Bold">
                    #{String(itemList.id_order).padStart(5, '0')}
                  </Text>
                </View>
              )}
              <Text
                fontSize={itemList.existOrder ? 18 : 22}
                weight="Bold"
                color="#149B72">
                ${parseFloat(price).toFixed(2)}
              </Text>
            </View>
          </View>

          <View style={styles.contentDetailPoint}>
            <View style={styles.contentPoints}>
              <View style={styles.contentRowPoint}>
                <View style={styles.contentPoint}>
                  <Text>A</Text>
                </View>
                <View style={styles.contentInputPoint}>
                  <View style={styles.contentWrapPoint}>
                    <Text align="left" weight="SemiBold">
                      {adressNameA}
                      {' ~ '}
                      <Text fontSize={16} weight="Light">
                        {duration} Min
                      </Text>
                    </Text>
                  </View>
                </View>
              </View>
              <View style={styles.contentDot}>
                <View style={styles.dot} />
                <View style={styles.dot} />
              </View>
              <View style={styles.contentRowPoint}>
                <View style={styles.contentPoint}>
                  <Text>B</Text>
                </View>
                <View style={styles.contentInputPoint}>
                  <View style={styles.contentWrapPoint}>
                    <Text align="left" weight="SemiBold">
                      {adressNameB}
                      {' ~ '}
                      <Text
                        align="left"
                        weight="Light"
                        fontSize={16}
                        color="rgba(0,0,0,0.9)">{`${distanceB} ${unitB}`}</Text>
                    </Text>
                  </View>
                </View>
              </View>
            </View>
          </View>
        </View>
        <View style={styles.contentButtonAction}>
          {!existOrder && (
            <TouchableOpacity
              style={styles.buttonIgnore}
              onPress={() => onPressItemIgnore(itemList)}>
              <Text color="white" weight="Light">
                {existOrder ? textButton : I18n.t('buttons.ignore_order')}
              </Text>
            </TouchableOpacity>
          )}
          <TouchableOpacity
            style={styles.buttonAccept}
            onPress={() => onPressItem(itemList)}>
            <Text color="white" weight="SemiBold">
              {existOrder ? textButton : I18n.t('buttons.accepted_order')}
            </Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

export default Item;
const {width} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
  },
  contentDetailPoint: {
    flex: 1,
    zIndex: 1,
    flexDirection: 'row',
    paddingHorizontal: 10,
  },
  contentPoints: {flex: 0.9},
  contentDetail: {
    flex: 1,
    marginTop: 15,
    paddingLeft: 10,
    marginHorizontal: 10,
  },
  contentWrapPoint: {
    flex: 1,
    flexWrap: 'wrap',
    flexDirection: 'row',
  },
  contentNameCompany: {
    justifyContent: 'flex-start',
    marginVertical: 10,
    flexDirection: 'row',
  },
  contentButtonAction: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 10,
  },
  item: {
    backgroundColor: 'white',
    margin: 10,
    justifyContent: 'center',
    borderRadius: 10,
    height: 270,
    // IOS
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.3,
    shadowRadius: 2.22,

    // Android
    elevation: 3,
  },
  itemExist: {
    backgroundColor: 'white',
    margin: 10,
    justifyContent: 'center',
    borderRadius: 10,
    height: 280,
    // IOS
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.3,
    shadowRadius: 2.22,

    // Android
    elevation: 3,
  },
  list: {
    marginTop: 10,
  },
  contentDot: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    width: 12,
    marginLeft: 6,
  },
  nameCompany: {flex: 0.6},
  dot: {
    height: 7,
    width: 7,
    backgroundColor: 'red',
    borderRadius: 50,
    marginVertical: 5,
  },
  contentInputPoint: {
    flex: 1,
    justifyContent: 'center',
    paddingLeft: 10,
    flexWrap: 'nowrap',
  },
  contentPoint: {
    justifyContent: 'center',
    alignItems: 'center',
    width: 25,
    height: 25,
    borderColor: palette.primaryColor,
    borderWidth: 2,
    borderRadius: 50,
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  separator: {
    width: 2,
    backgroundColor: 'rgba(0,0,0,0.1)',
    marginVertical: 20,
    zIndex: 1,
    marginLeft: 10,
  },
  contentPrice: {
    flex: 0.4,
    justifyContent: 'center',
    alignItems: 'flex-end',
    paddingRight: 5,
  },
  buttonAccept: {
    height: 30,
    backgroundColor: '#149B72',
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 15,
    marginHorizontal: 10,
    marginBottom: 10,
    marginTop: '10%',
    borderRadius: 5,
  },
  buttonIgnore: {
    height: 30,
    backgroundColor: palette.status.cancel,
    justifyContent: 'center',
    flex: 1,
    alignItems: 'center',
    paddingHorizontal: 15,
    marginHorizontal: 10,
    marginBottom: 10,
    marginTop: '10%',
    borderRadius: 5,
  },
  contentRow: {
    flex: 0.5,
    alignItems: 'flex-start',
    alignContent: 'space-between',
  },
  contentRowPoint: {
    flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    zIndex: -1,
  },
  positionProgress: {
    position: 'absolute',
    left: 0,
    top: 0,
    zIndex: 1,
    right: 0,
  },
  posititonTimer: {
    transform: [{rotate: '10deg'}],
    flex: 1,
  },
  infoPriceDistance: {
    flex: 0.3,
    justifyContent: 'space-between',
    paddingVertical: 10,
  },
  infoOrder: {flex: 0.7, justifyContent: 'space-between', paddingVertical: 15},
  detailInfoOrder: {
    paddingHorizontal: '5%',
    flex: 1,
    justifyContent: 'space-between',
  },
});
