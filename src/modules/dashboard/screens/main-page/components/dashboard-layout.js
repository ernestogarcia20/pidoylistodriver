import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  Image,
  Dimensions,
  TouchableOpacity,
  Switch,
} from 'react-native';
import Item from './item-order-list';
import LottieView from 'lottie-react-native';
import LoaderAnimated from '../../../../shared/widgets/loader-animated';
import Text from '../../../../shared/widgets/text';
import Inactive from '../../../../shared/assets/lottie/inactive.json';
import I18n from '../../../../../translation';
import palette from '../../../../shared/assets/palette';
import WithoutInternet from '../../../../shared/widgets/empty-component';

const Layout = ({
  changeStateOnline,
  onPressOffer,
  checkInternet,
  onPressItemIgnore,
  orderTemp,
  listIgnore,
  orderList,
  geolocation,
  removeItem,
  user,
  spinner,
  currentOrder,
  onPressGoToDetailOrder,
  onPressSwitch,
}) => {
  const renderHeader = () => {
    return (
      <View>
        <View />
      </View>
    );
  };

  const renderItem = ({item, index}) => {
    return (
      <Item
        key={`${index}`}
        item={item}
        listIgnore={listIgnore}
        removeItem={removeItem}
        onPressItemIgnore={onPressItemIgnore}
        //onPressItem={onPressItem}
        onPressItem={onPressOffer}
        geolocation={geolocation}
      />
    );
  };

  const emptyComponent = () => {
    return (
      <View style={styles.loader}>
        {checkInternet && orderList.length === 0 && !spinner && (
          <View style={styles.contentLoader}>
            <LoaderAnimated text={I18n.t('loader.waiting')} fontSize={24} />
          </View>
        )}
      </View>
    );
  };
  return (
    <View style={styles.container}>
      {checkInternet && !user.current_order && (
        <View style={styles.contentStatus}>
          <Text weight="Light">
            Estado:{' '}
            <Text
              weight="Bold"
              fontSize={16}
              color={user.online ? palette.active : palette.inactive}>
              {user.online ? 'Activo' : 'Inactivo'}
            </Text>
          </Text>
          <Switch
            value={user.online}
            onValueChange={onPressSwitch}
            trackColor={{
              false: 'black',
              true: palette.active,
            }}
            thumbColor={user.online ? 'white' : 'white'}
          />
        </View>
      )}
      <View style={styles.container}>
        {user.online && !user.current_order && checkInternet && (
          <FlatList
            style={styles.list}
            data={orderList}
            extraData={orderList}
            renderItem={renderItem}
            keyExtractor={(item, index) => item + index}
            ListEmptyComponent={emptyComponent}
          />
        )}
        {user.current_order && currentOrder.driver_name ? (
          <View style={styles.contentCurrentOrder}>
            <View>
              <Text fontSize={24} weight="Medium">
                Pedido en curso
              </Text>
            </View>
            <View style={styles.contentItemOrder}>
              <Item
                item={{...currentOrder, existOrder: true}}
                onPressItem={onPressGoToDetailOrder}
                geolocation={geolocation}
                textButton={I18n.t('buttons.go_to_order')}
              />
            </View>
          </View>
        ) : null}
        {!user.online && !user.current_order && checkInternet && (
          <View style={{flex: 1}}>
            <View style={styles.contentWifi}>
              <Text align="justify" fontSize={18} weight="Light">
                {I18n.t('offline')}
              </Text>
            </View>
          </View>
        )}
      </View>
      {!checkInternet && <WithoutInternet />}
    </View>
  );
};

const {height} = Dimensions.get('window');

export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
  },
  contentItemOrder: {flex: 0.8},
  item: {
    paddingHorizontal: 30,
    backgroundColor: 'black',
    margin: 10,
    flexDirection: 'row',
    justifyContent: 'center',
    borderRadius: 10,
    paddingVertical: '5%',
    height: 140,
  },
  contentStatus: {
    height: 60,
    justifyContent: 'space-between',
    alignItems: 'center',
    backgroundColor: 'white',
    flexDirection: 'row',
    paddingHorizontal: '10%',
    borderBottomWidth: 1,
    borderColor: 'rgba(0,0,0,0.15)',
  },
  contentCurrentOrder: {flex: 1, justifyContent: 'center'},
  list: {
    paddingTop: 10,
  },
  contentImage: {
    flex: 0.3,
    alignItems: 'center',
  },
  contentRow: {
    flex: 0.5,
    alignItems: 'flex-start',
    alignContent: 'space-between',
  },
  loader: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  separator: {
    height: 1,
    width: '86%',
    backgroundColor: '#CED0CE',
    marginLeft: '14%',
  },
  contentPrice: {
    height: '70%',
    width: '100%',
    alignItems: 'flex-end',
    justifyContent: 'flex-end',
    flexDirection: 'column',
  },
  positionProgress: {position: 'absolute', top: -1, left: -0, right: 0},
  contentWifi: {
    marginTop: 15,
    paddingVertical: '5%',
    paddingHorizontal: '10%',
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: 'white',
  },
});
