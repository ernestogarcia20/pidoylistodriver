import React, {PureComponent} from 'react';
import {connect} from 'react-redux';
import DashboardLayout from './components/dashboard-layout';
import {StyleSheet, View} from 'react-native';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import Spinner from 'react-native-loading-spinner-overlay';

import {
  getOrder,
  removeInList,
  updateDriver,
  findByOrderId,
  removeOrderTemp,
  setListIgnore,
} from '../../services/services-dashboard';
import {ORDER_DETAIL} from '../../../utils/constants/constants-navigate';
import I18n from '../../../../translation';
import {showMessage} from 'react-native-flash-message';
import Loader from '../../../shared/widgets/loader';
import {getParams} from '../../../auth/services/user-service';

class Dashboard extends PureComponent {
  constructor(props) {
    super(props);
    this.state = {
      active: true,
      reload: false,
      spinner: false,
      currentOrder: false,
      itemOfferSelected: {},
      locationMerchant: {},
      locationClient: {},
      showModalOffer: false,
    };
    this.unsubscribe = null;
  }

  componentDidMount() {
    const {
      dispatch,
      user: {uid_driver, current_order},
    } = this.props;
    const {currentOrder} = this.state;
    removeOrderTemp(dispatch);
    this.unsubscribe = getOrder(dispatch, uid_driver, this.props);
    if (current_order && !currentOrder) {
      this.getCurrentOrder();
    }
    getParams(dispatch);
  }

  componentDidUpdate(props) {
    const {orderTemp} = this.props;
    if (props.orderTemp !== orderTemp && orderTemp) {
      this.getCurrentOrder();
    }
  }

  componentWillUnmount() {
    if (!this.unsubscribe) {
      return;
    }
    this.unsubscribe.off('value');
  }

  getCurrentOrder = () => {
    const {user} = this.props;
    if (!user.current_order) {
      return;
    }
    findByOrderId(user.current_uid_company, user.current_order).then((item) => {
      this.setState({currentOrder: item, showModalOffer: false});
    });
  };

  removeItem = (item) => {
    const {dispatch, orderList} = this.props;
    const list = orderList.filter((itemf) => item !== itemf);
    removeInList(dispatch, list);
  };

  onPressGoToDetailOrder = () => {
    const {
      navigation: {navigate},
    } = this.props;
    const {currentOrder} = this.state;
    navigate(ORDER_DETAIL, {item: currentOrder});
  };

  changeStateOnline = () => {
    const {user, checkInternet} = this.props;
    if (!user) {
      return;
    }
    if (!checkInternet) {
      showMessage({
        message: I18n.t('warning'),
        description: I18n.t('internet'),
        type: 'danger',
      });
      return;
    }
    updateDriver(user.uid_driver, {online: true});
  };

  onPressOffer = (item) => {
    const {latitud: latitude, longitud: longitude} = item.points.find(
      (x) => x.point === 'A',
    );
    const client = item.points.find((x) => x.point === 'B');
    const {
      navigation: {navigate},
    } = this.props;
    const locationMerchant = {
      longitude: parseFloat(longitude),
      latitude: parseFloat(latitude),
      longitudeDelta: 0.00102,
      latitudeDelta: 0.01,
    };
    const locationClient = {
      longitude: parseFloat(client.longitud),
      latitude: parseFloat(client.latitud),
      longitudeDelta: 0.00102,
      latitudeDelta: 0.01,
    };
    navigate('Modal', {locationMerchant, locationClient, orderSelected: item});
  };

  closeModal = () => {
    const {dispatch} = this.props;
    this.setState({showModalOffer: false});
    removeOrderTemp(dispatch);
  };

  onPressItemIgnore = (item) => {
    const {listIgnore, dispatch} = this.props;
    const key = `${item.uid_company}-${item.id_order}`;
    const newListIgnore = listIgnore;
    const findInListIgnore = newListIgnore.find((x) => x === key);
    this.removeItem(item);
    if (findInListIgnore) {
      return;
    }
    newListIgnore.push(key);
    setListIgnore(dispatch, newListIgnore);
  };

  onPressSwitch = (value) => {
    const {user, checkInternet} = this.props;
    if (!checkInternet) {
      showMessage({
        message: I18n.t('warning'),
        description: I18n.t('internet'),
        type: 'danger',
      });
      return;
    }
    updateDriver(user.uid_driver, {online: value});
  };

  render() {
    const {spinner} = this.state;
    const {
      navigation: {openDrawer},
      orderList,
      geolocation,
      checkInternet,
      user,
      orderTemp,
      listIgnore,
    } = this.props;
    return (
      <Container>
        <Header
          name={I18n.t('title_header.order_list')}
          actionDrawer={openDrawer}
          menu
        />
        <DashboardLayout
          logOut={this.handleLogOut}
          onPressItem={this.handleButton}
          user={user}
          geolocation={geolocation}
          {...this.state}
          orderList={orderList}
          listIgnore={listIgnore}
          onPressItemIgnore={this.onPressItemIgnore}
          checkInternet={checkInternet}
          onPressOffer={this.onPressOffer}
          orderTemp={orderTemp}
          removeItem={this.removeItem}
          onPressGoToDetailOrder={this.onPressGoToDetailOrder}
          changeStateOnline={this.changeStateOnline}
          onPressSwitch={this.onPressSwitch}
        />
        <Spinner
          visible={spinner}
          overlayColor="rgba(0,0,0,0.85)"
          size="large"
          animation="fade"
          customIndicator={
            <View style={styles.contentIndicator}>
              <Loader
                text={I18n.t('loader.wait_moment')}
                textColor="white"
                colorIndicator="white"
                fontSize={20}
              />
            </View>
          }
          textStyle={styles.spinnerTextStyle}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  orderList: state.orderList.orderList,
  checkInternet: state.checkInternet.checkInternet,
  geolocation: state.myGeolocation.geolocation,
  listIgnore: state.listIgnore.listIgnore,
  orderTemp: state.orderList.orderTemp,
  params: state.params,
});
const styles = StyleSheet.create({
  spinnerTextStyle: {
    color: '#FFF',
  },
  contentIndicator: {flex: 1, justifyContent: 'center', alignItems: 'center'},
});

export default connect(mapStateToProps)(Dashboard);
