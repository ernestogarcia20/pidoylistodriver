import React, {Component} from 'react';
import {connect} from 'react-redux';
import OrdersHistoryLayout from './components/orders-history-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import I18n from '../../../../translation';
import * as RNLocalize from 'react-native-localize';
import {getOrdersHistory} from '../../services/services-dashboard';
import moment from 'moment';
import 'moment/min/locales';

class OrdersHistory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      buttonSelected: 0,
      listCancel: [],
      listFinalized: [],
      startDate: '',
      endDate: '',
      placeHolder: '',
    };
    this.suscribe = {};
  }

  componentDidMount() {
    moment.locale(RNLocalize.getLocales()[0].languageCode);
    const formatPlaceHolder = 'LL';
    const formatConsult = 'YYYY-MM-DD';
    const startDate = moment();
    const endDate = moment().subtract(7, 'days');
    this.setState(
      {
        startDate: startDate.format(formatConsult),
        endDate: endDate.format(formatConsult),
        placeHolder: `${endDate.format(
          formatPlaceHolder,
        )} -> ${startDate.format(formatPlaceHolder)}`,
      },
      () => {
        this.getHistoryList();
      },
    );
    const {user, dispatch} = this.props;
    getOrdersHistory(dispatch, user.uid_driver);
  }

  getHistoryList = () => {
    const {user, dispatch} = this.props;
    const {startDate, endDate} = this.state;
    const obj = {
      uid_company: '',
      uid_driver: user.uid_driver,
      datefrom: endDate,
      dateto: startDate,
    };

    getOrdersHistory(dispatch, obj);
  };

  confirmRangeDate = ({startDate, endDate}) => {
    this.setState({startDate: endDate, endDate: startDate}, () => {
      this.getHistoryList();
    });
  };

  componentDidUpdate(prevProps) {
    const {checkInternet, ordersHistory} = this.props;
    if (checkInternet !== prevProps.checkInternet && checkInternet) {
      if (
        prevProps.ordersHistory !== ordersHistory &&
        ordersHistory.length === 0
      ) {
        this.componentDidMount();
      }
    }
  }

  render() {
    const {user} = this.props;
    const {
      navigation: {openDrawer},
      ordersHistory,
      checkInternet,
    } = this.props;
    return (
      <Container>
        <Header
          name={I18n.t('title_header.orders_history')}
          actionDrawer={openDrawer}
          menu
        />
        <OrdersHistoryLayout
          user={user}
          {...this.state}
          ordersHistory={ordersHistory}
          checkInternet={checkInternet}
          confirmRangeDate={this.confirmRangeDate}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  ordersHistory: state.ordersHistory.ordersHistory,
  checkInternet: state.checkInternet.checkInternet,
});

export default connect(mapStateToProps)(OrdersHistory);
