import React from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import EmptyComponent from '../../../../shared/widgets/empty-component';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Text from '../../../../shared/widgets/text';
import moment from 'moment';
import I18n from '../../../../../translation';
import palette from '../../../../shared/assets/palette';
import DatePicker from '../../../../shared/widgets/date-picker/ComposePicker';
import {STATUS_ORDER} from '../../../../utils/constants/global-constants';

const Layout = ({
  checkInternet,
  ordersHistory,
  placeHolder,
  confirmRangeDate,
}) => {
  const emptyComponent = () => {
    return <EmptyComponent ordersHistory />;
  };

  const renderItem = ({item}) => {
    if (!item) {
      return;
    }
    const {company_descricption, price, date_created, traking} = item;
    if (traking !== 3 && traking !== 4) {
      return;
    }
    return (
      <View style={styles.item}>
        <View style={styles.contentRoundIcon}>
          <View
            style={[
              styles.roundIcon,
              {backgroundColor: STATUS_ORDER[traking].color},
            ]}>
            <Icon name={STATUS_ORDER[traking].icon} color="white" size={20} />
          </View>
        </View>
        <View style={styles.detailOrder}>
          <Text fontSize={14} weight="700" align="auto">
            {company_descricption}
          </Text>
          <Text color={STATUS_ORDER[traking].color} fontSize={13}>
            {STATUS_ORDER[traking].label}
          </Text>
        </View>
        <View style={[styles.detailOrderDate]} />
        <View style={[styles.detailOrderAbsolute]}>
          <Text color="#149B72" weight="Bold" fontSize={16}>
            {parseFloat(price).toFixed(2)}$
          </Text>
          <Text
            align="center"
            fontSize={14}
            color={'rgba(0,0,0,0.8)'}
            weight="SemiBold">
            #{String(item.id_order).padStart(5, '0')}
          </Text>
          <Text fontSize={12} align="auto" color="rgba(0,0,0,0.5)">
            {moment(date_created).format('DD MMMM YYYY | hh:mm a')}
          </Text>
        </View>
      </View>
    );
  };
  return (
    <View style={styles.container}>
      <DatePicker
        style={styles.datePicker}
        placeholder={placeHolder}
        customStyles={{
          placeholderText: {fontSize: 14}, // placeHolder style
          headerStyle: {backgroundColor: palette.status.finish}, // title container style
          headerMarkTitle: {color: 'white', fontSize: 24}, // title mark style
          headerDateTitle: {fontSize: 20}, // title Date style
          contentInput: {}, //content text container style
          contentText: {fontSize: 14}, //after selected text Style
        }} // optional
        centerAlign // optional text will align center or not
        allowFontScaling={false} // optional
        mode={'range'}
        blockAfter
        markText={I18n.t('date_picker.date_range')}
        ButtonText={I18n.t('buttons.choose')}
        onConfirm={confirmRangeDate}
        returnFormat="YYYY-MM-DD"
      />
      {checkInternet && (
        <FlatList
          style={styles.list}
          disableVirtualization
          data={ordersHistory || []}
          renderItem={renderItem}
          keyExtractor={(item, index) => item + index}
          ListEmptyComponent={emptyComponent}
        />
      )}
      {!checkInternet && <EmptyComponent />}
    </View>
  );
};

export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEE',
    alignContent: 'center',
  },
  item: {
    paddingVertical: 20,
    flexDirection: 'row',
    backgroundColor: 'white',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: '4%',
    marginVertical: 5,
  },
  roundIcon: {
    height: 40,
    width: 40,
    borderRadius: 50,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#149B72',
  },
  contentRoundIcon: {
    flex: 0.15,
    justifyContent: 'center',
    alignItems: 'flex-start',
  },
  detailOrder: {flex: 0.45, alignItems: 'flex-start'},
  detailOrderDate: {
    flex: 0.4,
    alignItems: 'flex-end',
  },
  detailOrderAbsolute: {
    position: 'absolute',
    right: 15,
    flex: 0.4,
    alignItems: 'flex-end',
  },
  datePicker: {
    height: 50,
    borderRadius: 0,
    width: '100%',
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: 10,
    backgroundColor: 'white',
    borderWidth: 0,
  },
});
