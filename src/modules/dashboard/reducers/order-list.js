/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_ORDER_LIST = 'SET_ORDER_LIST';
export const SET_ORDER_BY_KEY = 'SET_ORDER_BY_KEY';
export const SET_CURRENT_ORDER = 'SET_CURRENT_ORDER';
export const REMOVE_ALL = 'REMOVE_ALL';

const INITIAL_STATE = {
  orderList: [],
  orderTemp: {},
  currentOrder: {},
};
function OrderList(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_ORDER_LIST:
      return {...state, orderList: action.payload};
    case SET_ORDER_BY_KEY:
      return {...state, orderTemp: action.payload};
    case SET_CURRENT_ORDER:
      return {...state, currentOrder: action.payload};
    case REMOVE_ALL:
      return {...state, ...INITIAL_STATE};
    default:
      return state;
  }
}

export default OrderList;
