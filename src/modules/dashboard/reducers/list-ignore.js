/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_LIST_IGNORE = 'SET_LIST_IGNORE';
export const REMOVE_ALL = 'REMOVE_ALL';

const INITIAL_STATE = {
  listIgnore: [],
};
function ListIgnore(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_LIST_IGNORE:
      return {...state, listIgnore: action.payload};
    case REMOVE_ALL:
      return {listIgnore: []};
    default:
      return state;
  }
}

export default ListIgnore;
