/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_GEOLOCATION = 'SET_CSET_GEOLOCATIONOMPANIES';
export const REMOVE_ALL = 'REMOVE_ALL';

const INITIAL_STATE = {
  geolocation: {lat: 0, long: 0},
};
function geolocation(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_GEOLOCATION:
      return {...state, geolocation: action.payload};
    default:
      return state;
  }
}

export default geolocation;
