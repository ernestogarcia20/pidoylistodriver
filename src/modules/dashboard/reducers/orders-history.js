/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_ORDERS_HISTORY = 'SET_ORDERS_HISTORY';
export const REMOVE_ALL = 'REMOVE_ALL';

const INITIAL_STATE = {
  ordersHistory: [],
};
function OrderList(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_ORDERS_HISTORY:
      return {...state, ordersHistory: action.payload};
    case REMOVE_ALL:
      return {...state, ...INITIAL_STATE};
    default:
      return state;
  }
}

export default OrderList;
