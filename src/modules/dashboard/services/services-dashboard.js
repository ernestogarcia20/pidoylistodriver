import {
  SET_ORDER_LIST,
  SET_ORDER_BY_KEY,
  SET_CURRENT_ORDER,
} from '../reducers/order-list';
import {database} from '../../shared/services/firebase-service';
import API from '../../shared/services/api-service';
import {SET_GEOLOCATION} from '../reducers/my-geolocation';
import {SET_USER} from '../../auth/reducers/user';
import {SET_ORDERS_HISTORY} from '../reducers/orders-history';
import {URL_DELIVERY, URL_DINERS} from '../../utils/constants/url-constants';
import {getKilometros} from '../../utils/functions';
import moment from 'moment';
import {SET_LIST_IGNORE} from '../reducers/list-ignore';

export const getOrder = (dispatch, uid_driver, props) => {
  const ref = database.ref().child('/order_list_temp/');
  ref
    .orderByChild('order_accepted')
    .endAt(false)
    .on(
      'value',
      (snapshot) => {
        const {geolocation, listIgnore, params} = props;
        if (!snapshot && !snapshot.exists()) {
          return;
        }
        const obj = snapshot.val();
        const array = [];
        if (!obj) {
          setDispatch(dispatch, array, SET_ORDER_LIST);
          return;
        }
        Object.keys(obj).map((key) => {
          const item = obj[key];
          const timeCreated = moment(item.date_created, 'YYYY-MM-DD HH:mm:ss');
          const now = moment();
          const timerCurrent = now.diff(timeCreated, 'minutes');

          if (!item.points || timerCurrent >= 5) {
            return;
          }

          const keyIgnore = `${item.uid_company}-${item.id_order}`;
          const findListIgnore = listIgnore.find((x) => x === keyIgnore);
          if (findListIgnore) {
            return;
          }

          const {latitud, longitud} = item.points[0];
          const distance = getKilometros(
            geolocation.lat,
            geolocation.long,
            latitud,
            longitud,
          );
          if (distance >= params.range_km) {
            return;
          }

          if (item.drivers) {
            if (item.drivers.length > 0) {
              const findme = item.drivers.find((x) => x === uid_driver);
              if (findme && findme !== undefined) {
                array.push({...item, key_order: key});
              }
              return;
            }
          }

          array.push({...item, key_order: key});
        });
        setDispatch(dispatch, array, SET_ORDER_LIST);
      },
      () => getOrder(dispatch, uid_driver, props),
    );

  return ref;
};

export const removeInList = async (dispatch, list) => {
  setDispatch(dispatch, list, SET_ORDER_LIST);
};

export const setGeolocation = async (dispatch, geolocation) => {
  setDispatch(dispatch, geolocation, SET_GEOLOCATION);
};

export const setListIgnore = (dispatch, listIgnore) => {
  setDispatch(dispatch, listIgnore, SET_LIST_IGNORE);
};

export const updateDocumentsDriver = async (item) => {
  return API.post(`${URL_DELIVERY}/UpdateDriverUrl`, item);
};

export const updateDriver = async (uid, item) => {
  return database.ref(`/drivers/${uid}`).update(item);
};

export const removeOrderTemp = async (dispatch) => {
  setDispatch(dispatch, false, SET_ORDER_BY_KEY);
};

export const updateOrdersHistoryDriver = async (uid, item) => {
  return new Promise((resolve, eject) => {
    database
      .ref()
      .child(`/orders_history/${uid}/${item.id_order}`)
      .set(item)
      .then(() => {
        finishOrder(item).then(resolve).catch(eject);
      })
      .catch(eject);
  });
};

export const finishOrder = async (order) => {
  return API.post(`${URL_DINERS}/changeOrder`, order);
};

export const setDriverToOfferOrder = async (key_order, driver) => {
  return database
    .ref()
    .child(`/offer_list/${key_order}/${driver.uid_driver}`)
    .set(driver);
};

export const removeDriverToOfferOrder = async (key_order, uid_driver) => {
  return database
    .ref()
    .child(`/offer_list/${key_order}/${uid_driver}`)
    .remove();
};

export const getOrderByKey = (dispatch, key_order) => {
  const ref = database.ref(`/order_list_temp/${key_order}`);
  ref.on(
    'child_removed',
    (snapshot) => {
      if (!snapshot && !snapshot.exists()) {
        setTimeout(() => {
          setDispatch(dispatch, false, SET_ORDER_BY_KEY);
        }, 250);
        return;
      }
      const obj = snapshot.val();
      if (!obj || obj === undefined) {
        setTimeout(() => {
          setDispatch(dispatch, false, SET_ORDER_BY_KEY);
        }, 250);
        return;
      }

      setDispatch(dispatch, obj, SET_ORDER_BY_KEY);
    },
    () => {
      setTimeout(() => {
        setDispatch(dispatch, {remove: true}, SET_ORDER_BY_KEY);
      }, 250);
    },
  );
  return ref;
};

export const getUser = async (dispatch, uid) => {
  return new Promise((resolve, eject) => {
    const ref = database.ref(`/drivers/${uid}`);
    ref.keepSynced(true);
    ref.on('value', (snapshot) => {
      if (!snapshot && !snapshot.exists()) {
        eject(false);
        ref.off('value');
      }
      const obj = snapshot.val();
      if (!obj) {
        eject(false);
        ref.off('value');
        setDispatch(dispatch, false, SET_USER);
        return;
      }
      setDispatch(dispatch, obj, SET_USER);
      resolve(obj);
    });
  });
};

export const updateOrderItem = async (item, index) => {
  return new Promise((resolve, reject) => {
    //return database.ref(`/order_list_temp/${index}`).update(item);
    const ref = database.ref(`/order_list_temp/${index}`);
    const trans = ref.transaction(
      (transaction) => {
        if (!transaction) {
          return;
        }
        if (transaction.order_accepted) {
          return;
        }
        transaction = {...transaction, ...item};
        return transaction;
      },
      (error, committed, snapshot) => {
        if (!committed || error) {
          reject();
        }
      },
    );
    trans.then(resolve).catch(reject);
  });
};

export const updateOrderNewItem = async (uid_companies, item, orderId) => {
  return database.ref(`/order_list/${uid_companies}/${orderId}`).update(item);
};

export const findByOrderId = async (uid_companies, orderId) => {
  return new Promise((resolve, eject) => {
    database
      .ref(`/order_list/${uid_companies}/${orderId}`)
      .once('value', (snapshot) => {
        if (!snapshot && !snapshot.exists()) {
          eject(false);
        }
        const obj = snapshot.val();
        if (!obj) {
          return;
        }
        resolve(obj);
      });
  });
};
export const findByOrderIdListener = (dispatch, uid_companies, orderId) => {
  const ref = database.ref(`/order_list/${uid_companies}/${orderId}`);
  ref.keepSynced(true);
  ref.on(
    'value',
    (snapshot) => {
      if (!snapshot && !snapshot.exists()) {
        setDispatch(dispatch, false, SET_CURRENT_ORDER);
      }
      const obj = snapshot.val();
      if (!obj) {
        setDispatch(dispatch, false, SET_CURRENT_ORDER);
        return;
      }
      setDispatch(dispatch, obj, SET_CURRENT_ORDER);
    },
    () => findByOrderIdListener(dispatch, uid_companies, orderId),
  );
  return ref;
};

export const getOrdersHistory = async (dispatch, obj) => {
  return new Promise((resolve, eject) => {
    return API.post(`${URL_DINERS}/GetOrder`, obj)
      .then((result) => {
        //console.log(result);
        if (!result) {
          return eject(result);
        }
        if (!result.ListData) {
          return eject(result);
        }
        setDispatch(dispatch, result.ListData, SET_ORDERS_HISTORY);
        resolve(result);
      })
      .catch(eject);
  });
};

export const writeInNewOrderAccepted = (item) => {
  return new Promise((resolve, reject) => {
    try {
      API.post(`${URL_DINERS}/changeOrder`, item).then(resolve).catch(reject);

      /* database
        .ref()
        .child('order_list/' + item.id_order)
        .set(item)
        .then(resolve)
        .catch(reject);*/
    } catch (e) {
      reject(e);
    }
  });
};

export const updateInNewOrderAccepted = (item, key) => {
  return database.ref('/order_list/' + key).update({
    ...item,
    id_order: '33',
  });
};

export const setDispatch = (dispatch, data, type) => {
  dispatch({
    type,
    payload: data,
  });
};
