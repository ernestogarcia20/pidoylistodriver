import React from 'react';
import {View, StyleSheet, FlatList, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome';
import Text from '../../shared/widgets/text';
import Button from '../../shared/widgets/button';
import I18n from '../../../translation';
import palette from '../../shared/assets/palette';
import {getInitials} from '../../utils/functions';
import FastImage from 'react-native-fast-image';
import { APP_VERSION } from '../../utils/constants/global-constants';

const Layout = ({user, routes, onPressItem, logOut, loading, menuSelected}) => {
  const renderItem = ({item}) => {
    return (
      <TouchableOpacity
        style={styles.listItem}
        onPress={() => onPressItem(item)}>
        <View style={styles.contentIcon}>
          <Icon
            name={item.icon}
            size={28}
            color={
              menuSelected === item.name
                ? 'rgba(179, 24,22,0.7)'
                : 'rgba(0,0,0,0.7)'
            }
          />
        </View>
        <View style={styles.contentText}>
          <Text
            align="left"
            fontSize={13}
            weight="800"
            color={
              menuSelected === item.name
                ? 'rgba(179, 24,22,0.7)'
                : 'rgba(0,0,0,0.8)'
            }>
            {item.name}
          </Text>
        </View>
      </TouchableOpacity>
    );
  };
  return (
    <View style={styles.container}>
      <View style={styles.contentImage}>
        <View style={styles.circleImage}>
          <FastImage
            style={styles.circleImage}
            source={{uri: user.selfieUrl}}
          />
        </View>
      </View>
      <Text color="gray" weight="bold" fontSize={16} style={{marginTop: 10}}>
        {user.first_name} {user.last_name}
      </Text>
      <Text color="gray" style={{marginBottom: 10}}>
        {user.email}
      </Text>
      <View style={styles.contentCode}>
        <Text weight="Medium" fontSize={16} color="rgba(0,0,0,0.7)">
          Tu codigo:{' '}
          <Text fontSize={20} color={palette.darkColor} weight="SemiBold">
            {String(user.id).padStart(5, '0')}
          </Text>
        </Text>
      </View>
      <View style={styles.sidebarDivider} />
      <FlatList
        style={styles.flatList}
        data={routes}
        renderItem={renderItem}
        keyExtractor={(item) => item.name}
      />
      <Button
        text={I18n.t('menu_list.logout')}
        isLoading={loading}
        onPress={logOut}
      />
    </View>
  );
};
export default Layout;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
  },
  profileImg: {
    width: 80,
    height: 80,
    borderRadius: 40,
    marginTop: 20,
  },
  listItem: {
    height: 60,
    alignItems: 'center',
    flexDirection: 'row',
  },
  circleImage: {
    width: 100,
    height: 100,
    borderRadius: 100,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#EEE',
  },
  contentImage: {
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 10,
  },
  flatList: {
    paddingHorizontal: '5%',
  },
  sidebarDivider: {
    height: 1,
    backgroundColor: 'lightgray',
    marginVertical: 10,
  },
  contentCode: {alignItems: 'center', marginHorizontal: 10},
  contentIcon: {flex: 0.2},
  contentText: {flex: 0.8},
});
