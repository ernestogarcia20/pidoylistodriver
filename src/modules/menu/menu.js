import React, {Component} from 'react';
import {connect} from 'react-redux';
import Container from '../shared/widgets/container';
import MenuLayout from './components/menu-layout';
import {
  LOGIN,
  ORDERS_HISTORY,
  SETTINGS,
  DASHBOARD,
} from '../utils/constants/constants-navigate';
import {logOut} from '../auth/services/user-service';
import {showMessage} from 'react-native-flash-message';
import I18n from '../../translation';
import {setDispatch} from '../dashboard/services/services-dashboard';
import {SET_MENU} from '../../store/reducers/menu';
import messaging from '@react-native-firebase/messaging';

class Menu extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      routes: [
        {
          name: I18n.t('menu_list.order_list'),
          navigate: DASHBOARD,
          icon: 'motorcycle',
        },
        {
          name: I18n.t('menu_list.orders_history'),
          navigate: ORDERS_HISTORY,
          icon: 'history',
        },
        {
          name: I18n.t('menu_list.settings'),
          navigate: SETTINGS,
          icon: 'cog',
        },
      ],
      selected: I18n.t('menu_list.order_list'),
    };
  }

  handleLogOut = () => {
    const {
      navigation: {navigate},
      dispatch,
      checkInternet,
    } = this.props;
    if (!checkInternet) {
      showMessage({
        message: I18n.t('warning'),
        description: I18n.t('internet'),
        type: 'danger',
      });
      return;
    }
    this.setState({loading: true});
    navigate(LOGIN);
    logOut(dispatch)
      .then(() => {
        messaging().unsubscribeFromTopic('drivers');
        this.setState({loading: false});
      })
      .catch(() => this.setState({loading: false}));
  };

  onPressItem = (item) => {
    const {
      navigation: {navigate, closeDrawer},
      dispatch,
    } = this.props;
    setDispatch(dispatch, item.name, SET_MENU);
    navigate(item.navigate);
    closeDrawer();
  };

  render() {
    const {routes} = this.state;
    const {user, menuSelected} = this.props;
    return (
      <Container>
        <MenuLayout
          user={user}
          onPressItem={this.onPressItem}
          logOut={this.handleLogOut}
          routes={routes}
          menuSelected={menuSelected}
          {...this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  checkInternet: state.checkInternet.checkInternet,
  menuSelected: state.menu.menu,
});

export default connect(mapStateToProps)(Menu);
