/**
 * AUTH
 */

export const AUTHENTICATOR = 'Authenticator';
export const LOGIN = 'Login';
export const SIGN_UP = 'SignUp';
export const PHONE_VERIFY = 'PhoneVerify';
export const DOCUMENTS = 'Documents';
export const RECOVER_PASSWORD = 'RecoverPassword';
export const PROCESS_VERIFY = 'ProcessVerify';
export const SUCCESSFULL_VERIFICATION = 'SuccessfullVerification';
export const TERMS_CONDITIONS = 'TermsConditions';
/**
 * DASHBOARD
 */
export const DASHBOARD = 'Dashboard';
export const DRAWER = 'Drawer';
export const ORDER_LIST = 'OrderList';
export const ORDERS_HISTORY = 'OrdersHistory';
export const SETTINGS = 'Settings';
export const SETTING_PROFILE = 'SettingProfile';
export const ORDER_DETAIL = 'OrderDetail';
