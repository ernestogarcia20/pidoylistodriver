import palette from '../../shared/assets/palette';
import I18n from '../../../translation';

export const API_KEY_ANDROID = 'AIzaSyDrD1fcIUf3YK5zYV0-zM2yAQKEIIYJHsU';
export const API_KEY_IOS = 'AIzaSyA2v02n7vMb2V1nS8xUjgoKcwoswJpE56U';
export const API_KEY_MAP = 'AIzaSyB7Tg3D2Tcq7k-njcfDbzs1SaHS3spbsBU';

export const APP_VERSION = 'Version 1.4.5';
/**
 * ACOOUNT
 */

export const INITIAL_ACCOUNT = {
  current_order: '',
  address: '',
  countryId: 1,
  documentUrl: '',
  email: '',
  first_name: '',
  id: 1,
  last_name: '',
  latitude: '',
  longitude: '',
  online: true,
  phone: '',
  receiptUrl: '',
  reload_geolocation: false,
  selfieUrl: '',
  uid_driver: '',
  verify: false,
};

/**
 * AGREGAR AL CONDUCTOR EN LA OFERTA
 */

export const INITIAL_OFFER_DRIVER = {
  url_image: '',
  driver_name: '',
  rating: 0,
  trips: 0,
  price: 0,
  uid_driver: '',
  latitude: 0,
  longitude: 0,
};

/**
 * ACTIONS
 */
export const REPLACE_CURRENT_SCREEN = 'ReplaceCurrentScreen';

/**
 * DOCUMENTS
 */
export const SELFIE_URI = 'selfieUri';
export const DOCUMENT_URI = 'documentUri';
export const RECEIPT_URI = 'receiptUri';

/**
 * KEYS PARA IDENTIFICAR LAS IMAGENES EN EL STORAGE DE FIREBASE
 */
export const URL_SELFIE = 'urlSelfie'; // URL SELFIE DEL CONDUCTOR
export const URL_RECEIPT = 'urlReceipt'; // URL RECIBO DE AGUA, LUZ O ARRIENDO
export const URL_DOC = 'urldoc'; // URL DEL PASAPORTE O CARNET

/**
 * STATUS ORDER COLOR
 */
export const STATUS_ORDER = [
  {
    color: palette.status.pending,
    label: I18n.t('order_detail.status.pending'),
  }, // 0 PENDIENTE POR CONFIRMACION DE UN DRIVER
  {
    color: palette.status.in_progress_merchant,
    label: I18n.t('order_detail.status.in_progress_local'),
  }, // 1 EN CAMINO AL LOCAL
  {
    color: palette.status.in_progress_client,
    label: I18n.t('order_detail.status.in_progress_delivery'),
  }, // 2 EN CAMINO A LA ENTREGA DEL PEDIDP
  {
    color: palette.status.finish,
    label: I18n.t('order_detail.status.delivered'),
    icon: 'check',
  }, // 3 PEDIDO FINALIZADO
  {
    color: palette.status.cancel,
    label: I18n.t('order_detail.status.cancel'),
    icon: 'times',
  }, // 4 PEDIDO CANCELADO
];
