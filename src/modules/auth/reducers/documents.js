/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_DOCUMENTS = 'SET_DOCUMENTS';
export const REMOVE_ALL = 'REMOVE_ALL';

const INITIAL_STATE = {
  documents: {
    selfieUrl: '',
    documentUrl: '',
    receiptUrl: '',
  },
};
function Documents(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_DOCUMENTS:
      return {...state, documents: action.payload};
    case REMOVE_ALL:
      return INITIAL_STATE;
    default:
      return state;
  }
}

export default Documents;
