import React, {Component} from 'react';
import {connect} from 'react-redux';
import SignInLayout from './components/sign-in-layout';
import Container from '../../../shared/widgets/container';
import {signIn} from '../../services/user-service';
import {
  DRAWER,
  SIGN_UP,
  RECOVER_PASSWORD,
  PROCESS_VERIFY,
} from '../../../utils/constants/constants-navigate';
import {showMessage} from 'react-native-flash-message';
import I18n from '../../../../translation';
import messaging from '@react-native-firebase/messaging';

class Login extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      isLoading: false,
      formFailed: true,
    };
  }

  Handle = () => {
    const {email, password} = this.state;
    this.setState({isLoading: true});
    const {
      navigation: {navigate},
      dispatch,
      checkInternet,
      fcmToken,
    } = this.props;

    const data = {
      user: email,
      password,
    };
    if (!email || !password) {
      return;
    }
    if (!checkInternet) {
      showMessage({
        message: I18n.t('error'),
        description: I18n.t('internet'),
        type: 'danger',
      });
      return;
    }

    signIn(dispatch, data, fcmToken)
      .then(() => {
        setTimeout(() => {
          const {user} = this.props;
          this.setState({isLoading: false});
          if (
            !user.verify &&
            (!user.documentUrl || !user.selfieUrl || !user.receiptUrl)
          ) {
            navigate(PROCESS_VERIFY, {uploadedDocuments: false});
            return;
          }
          if (
            !user.verify &&
            user.documentUrl &&
            user.selfieUrl &&
            user.receiptUrl
          ) {
            navigate(PROCESS_VERIFY, {uploadedDocuments: true});
            return;
          }
          messaging().subscribeToTopic('drivers');
          navigate(DRAWER);
        }, 250);
      })
      .catch(() => {
        this.setState({isLoading: false});
        showMessage({
          message: I18n.t('error'),
          description: I18n.t('firebase_error.auth/wrong-password'),
          type: 'danger',
        });
      });
  };

  handleSignUp = () => {
    const {
      navigation: {navigate},
    } = this.props;
    navigate(SIGN_UP);
  };

  handleRecovery = () => {
    const {
      navigation: {navigate},
    } = this.props;
    navigate(RECOVER_PASSWORD);
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    return (
      <Container>
        <SignInLayout
          {...this.state}
          handleSignUp={this.handleSignUp}
          handleRecovery={this.handleRecovery}
          handleSignIn={this.Handle}
          setState={this.handleChange}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  checkInternet: state.checkInternet.checkInternet,
  fcmToken: state.fcmToken,
});

export default connect(mapStateToProps)(Login);
