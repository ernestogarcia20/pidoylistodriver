import React, {useState} from 'react';
import {
  View,
  Platform,
  Dimensions,
  StyleSheet,
  Image,
  TouchableOpacity,
  KeyboardAvoidingView,
  ScrollView,
} from 'react-native';
import Logo from '../../../../shared/assets/background/max_delivery_logo.png';
import {FloatingTitleTextInputField} from '../../../../shared/widgets/input-floating';
import Text from '../../../../shared/widgets/text';
import Botton from '../../../../shared/widgets/button';
import {Form, Field} from 'react-native-validate-form';
import {
  inputRequired,
  inputEmail,
} from '../../../../shared/widgets/validations/validations';
import palette from '../../../../shared/assets/palette';
import I18n from '../../../../../translation';
import {APP_VERSION} from '../../../../utils/constants/global-constants';

let myForm = null;
const Layout = ({
  handleSignIn,
  setState,
  email,
  password,
  isLoading,
  handleSignUp,
  handleRecovery,
  formFailed,
}) => {
  const [errors, setErrors] = useState([]);

  const submitForm = (val) => {
    const submitResults = myForm.validate();

    let errorsList = errors;

    const fitler = errorsList.filter((x) => x.field === val);
    submitResults.forEach((item) => {
      if (val !== item.fieldName && val !== 'all') {
        return;
      }

      if (fitler === undefined || fitler.length === 0) {
        if (val === 'all') {
          const t = errorsList.filter((x) => x.field === item.fieldName);

          if (t === undefined || t.length === 0) {
            errorsList.push({field: item.fieldName, error: item.error});
          }

          return;
        }

        errorsList.push({field: item.fieldName, error: item.error});
      } else if (fitler.length > 0) {
        errorsList = errors.map((data) => {
          if (item.fieldName !== data.field) {
            return data;
          }

          if (item.error !== data.error) {
            return {
              ...data,
              error: item.error,
            };
          }

          return data;
        });
      } else if (item.isValid) {
        errorsList = errorsList.filter((x) => x.field !== val);
      }
    });
    setErrors(errorsList);
  };

  const submitSuccess = () => {
    setState('formFailed', false);
    //console.log('Submit Success!');
  };

  const submitFailed = () => {
    setState('formFailed', true);
    //console.log('Submit Faield!');
  };
  return (
    <View style={styles.container} pointerEvents={isLoading ? 'none' : 'auto'}>
      <ScrollView contentContainerStyle={styles.scrollView}>
        <KeyboardAvoidingView
          style={styles.contentForm}
          {...Platform.select({
            ios: {behavior: 'position'},
          })}
          enabled>
          <View style={styles.contentImage}>
            <View style={styles.contentLogo}>
              <Image source={Logo} style={styles.logo} />
            </View>

            <View style={styles.contentTypeApp}>
              <Text
                weight="800"
                fontSize={16}
                align="right"
                color={palette.primaryColor}>
                {I18n.t('buttons.driver')}
              </Text>
            </View>
          </View>
          <View>
            <Form
              ref={(ref) => (myForm = ref)}
              submit={submitSuccess}
              failed={submitFailed}
              validate
              errors={errors}>
              <Field
                required
                component={FloatingTitleTextInputField}
                validations={[inputRequired, inputEmail]}
                attrName="email"
                name="email"
                title={I18n.t('inputs.email')}
                value={email}
                onChangeText={() => submitForm('email')}
                onBlur={() => submitForm('email')}
                updateMasterState={setState}
                textInputStyles={styles.input}
                keyboardType="email-address"
              />
              <Field
                required
                component={FloatingTitleTextInputField}
                validations={[inputRequired]}
                attrName="password"
                name="password"
                title={I18n.t('inputs.password')}
                value={password}
                isPassword
                onChangeText={() => submitForm('password')}
                onBlur={() => submitForm('password')}
                updateMasterState={setState}
                textInputStyles={styles.input}
                otherTextInputProps={{
                  secureTextEntry: true,
                }}
              />
            </Form>
          </View>
          <View style={styles.rowButtons}>
            <TouchableOpacity
              onPress={handleSignUp}
              style={styles.buttonSignUp}>
              <Text fontSize={16} weight="600" color={palette.primaryColor}>
                {I18n.t('buttons.sign_up')}
              </Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={handleRecovery} style={styles.forgot}>
              <Text fontSize={12} weight="600" color={palette.darkColor}>
                {I18n.t('buttons.forgot')}
              </Text>
            </TouchableOpacity>
          </View>
        </KeyboardAvoidingView>

        <View style={styles.contentButtons}>
          <Botton
            enable={!formFailed}
            text={I18n.t('buttons.sign_in')}
            isLoading={isLoading}
            onPress={handleSignIn}
          />
        </View>
      </ScrollView>
      <Text weight="Light">{APP_VERSION}</Text>
    </View>
  );
};
export default Layout;
const {height} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
  },
  contentImage: {height: height * 0.4, justifyContent: 'center'},
  rowButtons: {
    paddingTop: 5,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  scrollView: {},
  contentLogo: {
    alignItems: 'center',
    justifyContent: 'center',
    height: height * 0.3,
    backgroundColor: 'white',
  },
  contentTypeApp: {
    justifyContent: 'flex-end',
    paddingHorizontal: '10%',
    position: 'absolute',
    right: 0,
    bottom: 0,
  },
  buttonColor: {
    height: 45,
    marginVertical: 5,
    backgroundColor: '#B31816',
    alignItems: 'center',
    justifyContent: 'center',
  },
  forgot: {
    paddingTop: 20,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  buttonSignUp: {
    height: 30,
    marginHorizontal: 15,
    marginTop: 15,
    marginBottom: 5,
    justifyContent: 'center',
    alignItems: 'flex-end',
  },
  input: {
    height: 40,
    color: 'black',
  },
  logo: {
    height: 192,
    width: 250,
    alignItems: 'center',
    justifyContent: 'center',
  },
  containerform: {
    justifyContent: 'space-between',
    height: 140,
  },
  whiteText: {
    color: 'white',
    fontWeight: 'bold',
  },
  contentForm: {
    justifyContent: 'center',
    marginHorizontal: 10,
    flex: 0.75,
  },
  contentButtons: {
    marginTop: '15%',
    marginHorizontal: '2%',
    justifyContent: 'center',
  },
  contentInputs: {
    justifyContent: 'space-between',
  },
});
