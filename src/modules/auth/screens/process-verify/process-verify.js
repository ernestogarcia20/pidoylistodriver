import React, {Component} from 'react';
import {connect} from 'react-redux';
import ProcessVerifyLayout from './components/process-verify-layout';
import Container from '../../../shared/widgets/container';
import {
  LOGIN,
  DOCUMENTS,
  DRAWER,
} from '../../../utils/constants/constants-navigate';
import {logOut} from '../../services/user-service';
import messaging from '@react-native-firebase/messaging';

class ProcessVerify extends Component {
  constructor(props) {
    super(props);
    this.state = {
      uploadedDocuments: true,
    };
  }

  componentDidMount() {
    const {
      navigation: {getParam},
    } = this.props;
    messaging().unsubscribeFromTopic('drivers');
    const uploadedDocuments = getParam('uploadedDocuments', true);
    this.setState({uploadedDocuments});
    if (uploadedDocuments) {
      return;
    }
    this.verifyAccount();
  }

  componentDidUpdate(props) {
    const {user} = this.props;
    if (props.user !== user) {
      this.verifyAccount();
    }
  }

  verifyAccount = () => {
    const {
      user,
      navigation: {navigate},
    } = this.props;
    if (!user) {
      return;
    }
    if (!user.verify) {
      return;
    }
    messaging().subscribeToTopic('drivers');
    navigate(DRAWER);
  };

  logOut = () => {
    const {
      navigation: {navigate},
      dispatch,
    } = this.props;
    navigate(LOGIN);
    messaging().subscribeToTopic('drivers');
    logOut(dispatch);
  };

  goToDocument = () => {
    const {
      navigation: {navigate},
    } = this.props;
    navigate(DOCUMENTS, {first_time: false});
  };

  render() {
    return (
      <Container>
        <ProcessVerifyLayout
          goToDocument={this.goToDocument}
          logOut={this.logOut}
          {...this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(ProcessVerify);
