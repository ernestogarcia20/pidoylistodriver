import React, {useState} from 'react';
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
} from 'react-native';
import {FloatingTitleTextInputField} from '../../../../shared/widgets/input-floating';
import Text from '../../../../shared/widgets/text';
import Botton from '../../../../shared/widgets/button';
import {Form, Field} from 'react-native-validate-form';
import {
  inputRequired,
  inputEmail,
} from '../../../../shared/widgets/validations/validations';
import I18n from '../../../../../translation';
import Icon from 'react-native-vector-icons/FontAwesome';
let myForm = null;
const Layout = ({email, setState, sendEmail, formFailed, loading}) => {
  const [errors, setErrors] = useState([]);

  const submitForm = (val) => {
    const submitResults = myForm.validate();

    let errorsList = errors;

    const fitler = errorsList.filter((x) => x.field === val);
    submitResults.forEach((item) => {
      if (val !== item.fieldName && val !== 'all') {
        return;
      }

      if (fitler === undefined || fitler.length === 0) {
        if (val === 'all') {
          const t = errorsList.filter((x) => x.field === item.fieldName);

          if (t === undefined || t.length === 0) {
            errorsList.push({field: item.fieldName, error: item.error});
          }

          return;
        }

        errorsList.push({field: item.fieldName, error: item.error});
      } else if (fitler.length > 0) {
        errorsList = errors.map((data) => {
          if (item.fieldName !== data.field) {
            return data;
          }

          if (item.error !== data.error) {
            return {
              ...data,
              error: item.error,
            };
          }

          return data;
        });
      } else if (item.isValid) {
        errorsList = errorsList.filter((x) => x.field !== val);
      }
    });
    setErrors(errorsList);
  };

  const submitSuccess = () => {
    setState('formFailed', false);
    //console.log('Submit Success!');
  };

  const submitFailed = () => {
    setState('formFailed', true);
    //console.log('Submit Faield!');
  };
  return (
    <ScrollView
      style={styles.container}
      pointerEvents={loading ? 'none' : 'auto'}>
      <KeyboardAvoidingView
        style={styles.KeyboardAvoidingView}
        {...Platform.select({
          android: {},
          ios: {behavior: 'position'},
        })}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}
        enabled>
        <View style={styles.contentForm}>
          <View style={styles.contentText}>
            <View style={styles.icon}>
              <Icon name="lock" size={90} />
            </View>
            <Text align="justify" color="black" weight="Regular">
              {I18n.t('recover_password.description')}
            </Text>
          </View>
          <Form
            ref={(ref) => (myForm = ref)}
            submit={submitSuccess}
            failed={submitFailed}
            style={styles.form}
            validate
            errors={errors}>
            <Field
              required
              component={FloatingTitleTextInputField}
              validations={[inputRequired, inputEmail]}
              attrName="email"
              name="email"
              title={I18n.t('inputs.email')}
              value={email}
              onBlur={() => submitForm('email')}
              onChangeText={() => submitForm('email')}
              updateMasterState={setState}
              keyboardType="email-address"
              textInputStyles={styles.input}
            />
          </Form>
        </View>
      </KeyboardAvoidingView>

      <View style={styles.button}>
        <Botton
          isLoading={loading}
          enable={!formFailed}
          text={I18n.t('buttons.send')}
          onPress={sendEmail}
        />
      </View>
    </ScrollView>
  );
};

export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
    zIndex: -1,
  },
  KeyboardAvoidingView: {paddingBottom: '5%'},
  contentForm: {
    paddingHorizontal: '10%',
    justifyContent: 'center',
    flexDirection: 'column',
  },
  input: {
    height: 40,
    color: 'black',
  },
  form: {marginBottom: '10%'},
  icon: {
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: '20%',
  },
  contentText: {
    marginBottom: '10%',
    marginTop: '20%',
  },
  button: {
    marginBottom: '20%',
    marginHorizontal: '5%',
  },
});
