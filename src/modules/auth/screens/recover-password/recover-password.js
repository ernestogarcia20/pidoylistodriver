import React, {Component} from 'react';
import {connect} from 'react-redux';
import RecoverPasswordLayout from './components/recover-password-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import I18n from '../../../../translation';
import {forgetPassword} from '../../services/user-service';
import {showMessage} from 'react-native-flash-message';

class RecoverPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {formFailed: true, email: '', loading: false};
  }

  sendEmail = () => {
    const {
      checkInternet,
      navigation: {goBack},
    } = this.props;
    const {email} = this.state;
    if (!checkInternet) {
      showMessage({
        message: I18n.t('error'),
        description: I18n.t('internet'),
        type: 'danger',
      });
      return;
    }

    this.setState({loading: true});
    forgetPassword(email)
      .then(() => {
        this.setState({loading: false, formFailed: true, email: ''});
        goBack();
        showMessage({
          message: I18n.t('success'),
          description: I18n.t('send_email'),
          type: 'success',
        });
      })
      .catch(({code}) => {
        this.setState({loading: false});
        showMessage({
          message: I18n.t('error'),
          description: I18n.t(`firebase_error.${code}`),
          type: 'danger',
        });
      });
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    const {
      navigation: {goBack},
    } = this.props;
    return (
      <Container>
        <Header
          name={I18n.t('title_header.recover_password')}
          actionDrawer={() => goBack()}
        />
        <RecoverPasswordLayout
          {...this.state}
          sendEmail={this.sendEmail}
          setState={this.handleChange}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  checkInternet: state.checkInternet.checkInternet,
});

export default connect(mapStateToProps)(RecoverPassword);
