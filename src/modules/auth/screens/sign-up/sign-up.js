import React, {Component} from 'react';
import {connect} from 'react-redux';
import SignUpLayout from './components/sign-up-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import {checkEmail, checkDriver} from '../../services/user-service';
import {PHONE_VERIFY, TERMS_CONDITIONS} from '../../../utils/constants/constants-navigate';
import {showMessage} from 'react-native-flash-message';
import I18n from '../../../../translation';

class SignUp extends Component {
  constructor(props) {
    super(props);
    this.state = {
      email: '',
      password: '',
      confirmPassword: '',
      name: '',
      lastName: '',
      phone: '',
      country: 'Panamá',
      countryId: 1,
      address: '',
      showCamera: false,
      formFailed: true,
      loading: false,
    };
  }

  Handle = () => {
    const {
      email,
      password,
      name,
      lastName,
      phone,
      countryId,
      address,
    } = this.state;
    const {
      navigation: {navigate},
      checkInternet,
    } = this.props;
    const data = {
      email,
      password,
      name,
      lastName,
      phone,
      countryId,
      address,
    };

    if (!checkInternet) {
      showMessage({
        message: I18n.t('error'),
        description: I18n.t('internet'),
        type: 'danger',
      });
      return;
    }

    this.setState({loading: true});
    this.setState({loading: false});
    // navigate(PHONE_VERIFY, {data});
    checkDriver({email, phone})
      .then(() => {
        this.setState({loading: false});
        navigate(PHONE_VERIFY, {data});
      })
      .catch(({Message}) => {
        this.setState({loading: false});
        showMessage({
          message: I18n.t('error'),
          description: I18n.t(`validations.server.${Message}`),
          type: 'danger',
        });
      });
  };

  goToTerms = () => {
    const {
      navigation: {navigate},
    } = this.props;
    navigate(TERMS_CONDITIONS);
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    const {
      navigation: {goBack},
    } = this.props;
    return (
      <Container>
        <Header name={I18n.t('sign_up')} actionDrawer={() => goBack()} />
        <SignUpLayout
          {...this.state}
          handleSignUp={this.Handle}
          setState={this.handleChange}
          openCamera={this.openCamera}
          goToTerms={this.goToTerms}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  checkInternet: state.checkInternet.checkInternet,
});

export default connect(mapStateToProps)(SignUp);
