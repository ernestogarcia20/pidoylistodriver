import React, {useState, useEffect} from 'react';
import {
  View,
  StyleSheet,
  KeyboardAvoidingView,
  Platform,
  ScrollView,
  Dimensions,
} from 'react-native';
import {FloatingTitleTextInputField} from '../../../../shared/widgets/input-floating';
import {Form, Field} from 'react-native-validate-form';
import Botton from '../../../../shared/widgets/button';
import CheckBox from '@react-native-community/checkbox';
import Text from '../../../../shared/widgets/text';
import {
  inputRequired,
  inputEmail,
  inputPassword,
  confirmPasswordValidation,
  inputPhone,
} from '../../../../shared/widgets/validations/validations';
import I18n from '../../../../../translation';
import {TouchableOpacity} from 'react-native-gesture-handler';

let myForm = null;
let refScroll = null;
const Layout = ({
  handleSignUp,
  setState,
  email,
  password,
  confirmPassword,
  name,
  lastName,
  phone,
  country,
  address,
  loading,
  openCamera,
  formFailed,
  goToTerms,
}) => {
  const [errors, setErrors] = useState([]);
  useEffect(() => {
    if (!confirmPassword || !password) {
      return;
    }
    if (password.length < 6) {
      return;
    }
    if (confirmPassword === password) {
      return;
    }
    //alert(confirmPassword +' '+ password)
    setTimeout(() => {
      submitForm('confirmPassword');
    }, 250);
  }, [confirmPassword, password]);

  function submitForm(val) {
    const submitResults = myForm.validate();
    let errorsList = errors;

    const fitler = errorsList.filter((x) => x.field === val);
    submitResults.forEach((item, index) => {
      if (val !== item.fieldName && val !== 'all') {
        return;
      }

      if (fitler === undefined || fitler.length === 0) {
        if (val === 'all') {
          const t = errorsList.filter((x) => x.field === item.fieldName);

          if (t === undefined || t.length === 0) {
            errorsList.push({field: item.fieldName, error: item.error});
          }

          return;
        }

        errorsList.push({field: item.fieldName, error: item.error});
      } else if (fitler.length > 0) {
        errorsList = errors.map((data) => {
          if (item.fieldName !== data.field) {
            return data;
          }

          if (item.error !== data.error) {
            return {
              ...data,
              error: item.error,
            };
          }

          return data;
        });
      } else if (item.isValid) {
        errorsList = errorsList.filter((x) => x.field !== val);
      }
    });

    setErrors(errorsList);
  }

  const [toggleCheckBox, setToggleCheckBox] = useState(false);

  const onChangePassword = (text) => {
    setTimeout(() => {
      submitForm('password');
    }, 50);
  };

  const submitSuccess = () => {
    setState('formFailed', false);
    //console.log('Submit Success!');
  };

  const submitFailed = () => {
    setState('formFailed', true);
    //console.log('Submit Faield!');
  };

  return (
    <View style={styles.container} pointerEvents={loading ? 'none' : 'auto'}>
      <KeyboardAvoidingView
        style={styles.keyboardAvoiding}
        {...Platform.select({
          ios: {behavior: 'padding'},
        })}
        keyboardVerticalOffset={Platform.OS === 'ios' ? 60 : 0}
        enabled>
        <ScrollView
          contentContainerStyle={styles.scrollView}
          ref={(ref) => {
            refScroll = ref;
          }}>
          <Form
            ref={(ref) => (myForm = ref)}
            submit={submitSuccess}
            failed={submitFailed}
            validate
            style={styles.contentForm}
            errors={errors}>
            <Field
              required
              component={FloatingTitleTextInputField}
              validations={[inputRequired, inputEmail]}
              attrName="email"
              name="email"
              title={I18n.t('inputs.email')}
              value={email}
              onBlur={() => submitForm('email')}
              onChangeText={() => submitForm('email')}
              updateMasterState={setState}
              keyboardType="email-address"
              textInputStyles={styles.inputLarge}
            />

            <View style={styles.separator} />
            <Field
              required
              ref={(ref) => (myForm = ref)}
              component={FloatingTitleTextInputField}
              attrName="password"
              name="password"
              isPassword
              validations={[inputRequired, inputPassword]}
              title={I18n.t('inputs.password')}
              onChangeText={onChangePassword}
              value={password}
              updateMasterState={setState}
              otherTextInputProps={{
                secureTextEntry: true,
              }}
              textInputStyles={styles.inputLarge}
            />

            <View style={styles.separator} />
            <Field
              required
              component={FloatingTitleTextInputField}
              attrName="confirmPassword"
              name="confirmPassword"
              isPassword
              validations={[
                inputRequired,
                confirmPasswordValidation(confirmPassword, password),
              ]}
              title={I18n.t('inputs.confirm')}
              onBlur={() => submitForm('confirmPassword')}
              onChangeText={() => {
                setTimeout(() => {
                  submitForm('confirmPassword');
                }, 100);
              }}
              value={confirmPassword}
              updateMasterState={setState}
              otherTextInputProps={{
                secureTextEntry: true,
              }}
              textInputStyles={styles.inputLarge}
            />

            <View style={styles.separator} />
            <View style={styles.row}>
              <Field
                required
                submit={submitSuccess}
                failed={submitFailed}
                validate
                errors={errors}
                ref={(ref) => (myForm = ref)}
                component={FloatingTitleTextInputField}
                attrName="name"
                name="name"
                validations={[inputRequired]}
                title={I18n.t('inputs.first_name')}
                onBlur={() => submitForm('name')}
                onChangeText={() => submitForm('name')}
                value={name}
                updateMasterState={setState}
                textInputStyles={styles.input}
              />
              <Field
                required
                submit={submitSuccess}
                failed={submitFailed}
                validate
                errors={errors}
                ref={(ref) => (myForm = ref)}
                component={FloatingTitleTextInputField}
                attrName="lastName"
                name="lastName"
                validations={[inputRequired]}
                title={I18n.t('inputs.last_name')}
                onBlur={() => submitForm('lastName')}
                onChangeText={() => submitForm('lastName')}
                value={lastName}
                updateMasterState={setState}
                textInputStyles={styles.input}
              />
            </View>
            <View style={styles.separator} />
            <View style={styles.row}>
              <Field
                required
                submit={submitSuccess}
                failed={submitFailed}
                validate
                errors={errors}
                ref={(ref) => (myForm = ref)}
                component={FloatingTitleTextInputField}
                attrName="country"
                name="country"
                editable={false}
                validations={[inputRequired]}
                title={I18n.t('inputs.country')}
                onBlur={() => submitForm('country')}
                onChangeText={() => submitForm('country')}
                value={country}
                updateMasterState={setState}
                textInputStyles={styles.input}
              />
              <Field
                required
                submit={submitSuccess}
                failed={submitFailed}
                validate
                errors={errors}
                ref={(ref) => (myForm = ref)}
                component={FloatingTitleTextInputField}
                attrName="phone"
                name="phone"
                validations={[inputRequired, inputPhone]}
                title={I18n.t('inputs.phone')}
                phone
                onChangeText={() => submitForm('phone')}
                onBlur={() => submitForm('phone')}
                value={phone}
                updateMasterState={setState}
                textInputStyles={styles.input}
                keyboardType="phone-pad"
              />
            </View>
            <View style={styles.separator} />
            <Field
              required
              component={FloatingTitleTextInputField}
              validations={[inputRequired]}
              attrName="address"
              name="address"
              multiline
              title={I18n.t('inputs.address')}
              value={address}
              onFocus={() => {
                if (Platform.OS === 'android') {
                  return;
                }
                refScroll.scrollTo({x: 0, y: 200, animated: true});
              }}
              onBlur={() => {
                submitForm('address');
              }}
              onChangeText={() => submitForm('address')}
              updateMasterState={setState}
              textInputStyles={styles.textArea}
            />

            <View style={styles.contentTems}>
              <CheckBox
                lineWidth={3}
                disabled={false}
                value={toggleCheckBox}
                onValueChange={() =>
                  toggleCheckBox
                    ? setToggleCheckBox(false)
                    : setToggleCheckBox(true)
                }
              />
              <TouchableOpacity style={styles.buttonTerm} onPress={goToTerms}>
                <Text weight="Light">
                  Aceptar los{' '}
                  <Text weight="SemiBold">términos y condiciones</Text>
                </Text>
              </TouchableOpacity>
            </View>
          </Form>
          <View style={styles.contentButtons}>
            <Botton
              enable={!formFailed && String(phone).length > 6 && toggleCheckBox}
              isLoading={loading}
              text={I18n.t('register')}
              onPress={() => {
                if (formFailed) {
                  submitForm('all');
                  return;
                }
                handleSignUp();
              }}
            />
          </View>
        </ScrollView>
      </KeyboardAvoidingView>
    </View>
  );
};
export default Layout;

const {width} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
  },
  scrollView: {paddingTop: '5%', paddingHorizontal: 10},
  contentLogo: {
    flex: 0.3,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonTerm: {borderBottomWidth: 1, marginLeft: 10},
  keyboardAvoiding: {flex: 1},
  separator: {
    paddingVertical: 0,
  },
  buttonColor: {
    height: 45,
    marginVertical: 5,
    backgroundColor: '#B31816',
    alignItems: 'center',
    justifyContent: 'center',
  },
  contentTems: {
    alignItems: 'center',
    flexDirection: 'row',
    justifyContent: 'flex-start',
    marginTop: 15,
  },
  input: {
    width: width / 2.5,
    height: 40,
    color: 'black',
  },
  inputLarge: {
    height: 40,
    color: 'black',
  },
  textArea: {
    height: 90,
    color: 'black',
  },
  logo: {
    height: 192,
    width: 250,
    alignItems: 'center',
    justifyContent: 'center',
  },
  whiteText: {
    color: 'white',
    fontWeight: 'bold',
  },
  row: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'space-between',
    flexWrap: 'wrap',
  },
  contentForm: {paddingHorizontal: 10, marginBottom: '10%'},
  containerForm: {},
  contentButtons: {},
  contentInputs: {
    height: 100,
    justifyContent: 'center',
  },
  contentInputsHorizontal: {
    flexDirection: 'row',
    justifyContent: 'center',
    height: 100,
  },
  contentDocumentHorizontal: {
    paddingTop: 15,
    justifyContent: 'space-around',
    alignItems: 'center',
  },
});
