import {
  Animated,
  Image,
  SafeAreaView,
  View,
  TouchableOpacity,
} from 'react-native';
import React, {useState, useEffect} from 'react';

import {
  CodeField,
  Cursor,
  useBlurOnFulfill,
  useClearByFocusCell,
} from 'react-native-confirmation-code-field';

import I18n from '../../../../../translation';
import styles, {
  ACTIVE_CELL_BG_COLOR,
  CELL_BORDER_RADIUS,
  CELL_SIZE,
  DEFAULT_CELL_BG_COLOR,
  NOT_EMPTY_CELL_BG_COLOR,
} from './style';
import LottieView from 'lottie-react-native';
import FabButton from '../../../../shared/widgets/fab-button';
import Text from '../../../../shared/widgets/text';
import palette from '../../../../shared/assets/palette';
import VerifyPhoneAnimation from '../../../../shared/assets/lottie/verify_phone.json';

const {Value, Text: AnimatedText} = Animated;

const CELL_COUNT = 6;

const animationsColor = [...new Array(CELL_COUNT)].map(() => new Value(0));
const animationsScale = [...new Array(CELL_COUNT)].map(() => new Value(1));
const animateCell = ({hasValue, index, isFocused}) => {
  Animated.parallel([
    Animated.timing(animationsColor[index], {
      useNativeDriver: false,
      toValue: isFocused ? 1 : 0,
      duration: 250,
    }),
    Animated.spring(animationsScale[index], {
      useNativeDriver: false,
      toValue: hasValue ? 0 : 1,
      duration: hasValue ? 300 : 250,
    }),
  ]).start();
};

const Layout = ({
  loading,
  verifyCodeHandle,
  setState,
  verifyError,
  resendCodeHandle,
  resendCode,
  codeVerify,
  phone,
}) => {
  useEffect(() => {
    if (codeVerify) {
      setValue(codeVerify);
    }
    if (resendCode) {
      setValue(null);
      setState('resendCode', false);
    }
  }, [codeVerify, resendCode]);
  const [value, setValue] = useState('');
  const ref = useBlurOnFulfill({value, cellCount: CELL_COUNT});
  const [props, getCellOnLayoutHandler] = useClearByFocusCell({
    value,
    setValue,
  });

  const onChangeText = (text) => {
    setValue(text);
    setState('codeVerify', text);
    setTimeout(() => {
      if (String(text).length > 5) {
        verifyCodeHandle();
      }
    }, 20);
  };

  const renderCell = ({index, symbol, isFocused}) => {
    const hasValue = Boolean(symbol);
    const animatedCellStyle = {
      backgroundColor: hasValue
        ? animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [NOT_EMPTY_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR],
          })
        : animationsColor[index].interpolate({
            inputRange: [0, 1],
            outputRange: [DEFAULT_CELL_BG_COLOR, ACTIVE_CELL_BG_COLOR],
          }),
      borderRadius: animationsScale[index].interpolate({
        inputRange: [0, 1],
        outputRange: [CELL_SIZE, CELL_BORDER_RADIUS],
      }),
      transform: [
        {
          scale: animationsScale[index].interpolate({
            inputRange: [0, 1],
            outputRange: [0.2, 1],
          }),
        },
      ],
    };

    // Run animation on next event loop tik
    // Because we need first return new style prop and then animate this value
    /*setTimeout(() => {
      animateCell({hasValue, index, isFocused});
    }, 0);*/

    return (
      <AnimatedText
        key={index}
        style={[styles.cell, animatedCellStyle]}
        onLayout={getCellOnLayoutHandler(index)}>
        {symbol || (isFocused ? <Cursor /> : null)}
      </AnimatedText>
    );
  };
  return (
    <View style={styles.root}>
      <View style={{height: 200}}>
        <LottieView
          source={VerifyPhoneAnimation}
          autoPlay={false}
          loop={false}
        />
      </View>
      <Text fontSize={16} style={styles.subTitle}>
        {I18n.t('verify_phone.insert_code')}
      </Text>
      <Text fontSize={16} weight="Regular" style={styles.phone}>
        {I18n.t('verify_phone.send_phone')}{' '}
        <Text weight="SemiBold">({phone})</Text>
      </Text>

      <CodeField
        ref={ref}
        {...props}
        value={value}
        onChangeText={onChangeText}
        cellCount={CELL_COUNT}
        rootStyle={styles.codeFiledRoot}
        keyboardType="number-pad"
        renderCell={renderCell}
      />
      {verifyError && (
        <View style={styles.contentResendCode}>
          <TouchableOpacity
            onPress={resendCodeHandle}
            hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}>
            <Text color={palette.primaryColor}>
              {I18n.t('buttons.resend_code')}
            </Text>
          </TouchableOpacity>
        </View>
      )}
      <View style={styles.contentFabAbsolute}>
        <FabButton
          nameIcon="arrow-right"
          loading={loading}
          colorFab={
            String(value).length < 6
              ? palette.primaryColorOpacity
              : palette.primaryColor
          }
          onPress={() => {
            if (!value) {
              return;
            }
            verifyCodeHandle();
          }}
        />
      </View>
    </View>
  );
};

export default Layout;
