import React, {Component} from 'react';
import {connect} from 'react-redux';
import VerifyPhoneLayout from './components/verify-phone-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import I18n from '../../../../translation';
import {authPhone, signUp, verifyPhone} from '../../services/user-service';
import {showMessage} from 'react-native-flash-message';
import {LOGIN, DOCUMENTS} from '../../../utils/constants/constants-navigate';
import {INITIAL_ACCOUNT} from '../../../utils/constants/global-constants';
import {auth} from '../../../shared/services/firebase-service';

import firebaseAuth from '@react-native-firebase/auth';

class VerifyPhone extends Component {
  constructor(props) {
    super(props);
    this.state = {
      codeVerify: '',
      formFailed: false,
      loading: false,
      verifyError: false,
      resendCode: false,
    };
  }
  payload = null;

  componentDidMount() {
    const {
      navigation: {getParam},
    } = this.props;
    const data = getParam('data', {});
    this.setState({...this.state, ...data}, () => this.sendMessageCodeVerify());
  }

  sendMessageCodeVerify = () => {
    const {phone} = this.state;
    const {
      navigation: {goBack},
    } = this.props;
    verifyPhone(phone).on(
      'state_changed',
      (phoneAuthSnapshot) => {
        // How you handle these state events is entirely up to your ui flow and whether
        // you need to support both ios and android. In short: not all of them need to
        // be handled - it's entirely up to you, your ui and supported platforms.

        // E.g you could handle android specific events only here, and let the rest fall back
        // to the optionalErrorCb or optionalCompleteCb functions
        switch (phoneAuthSnapshot.state) {
          // ------------------------
          //  IOS AND ANDROID EVENTS
          // ------------------------
          case firebaseAuth.PhoneAuthState.CODE_SENT:
            this.payload = phoneAuthSnapshot;
            //console.log('code sent');
            // on ios this is the final phone auth state event you'd receive
            // so you'd then ask for user input of the code and build a credential from it
            // as demonstrated in the `signInWithPhoneNumber` example above
            break;
          case firebaseAuth.PhoneAuthState.ERROR: // or 'error'
            //console.log('verification error');
            //console.log(phoneAuthSnapshot.error);
            break;

          // ---------------------
          // ANDROID ONLY EVENTS
          // ---------------------
          case firebaseAuth.PhoneAuthState.AUTO_VERIFY_TIMEOUT: // or 'timeout'
            //console.log('auto verify on android timed out');
            // proceed with your manual code input flow, same as you would do in
            // CODE_SENT if you were on IOS
            break;
          case firebaseAuth.PhoneAuthState.AUTO_VERIFIED: // or 'verified'
            // auto verified means the code has also been automatically confirmed as correct/received
            // phoneAuthSnapshot.code will contain the auto verified sms code - no need to ask the user for input.
            //console.log('auto verified on android');
            //console.log(phoneAuthSnapshot);
            // Example usage if handling here and not in optionalCompleteCb:
            const {verificationId, code} = phoneAuthSnapshot;
            const credential = firebaseAuth.PhoneAuthProvider.credential(
              verificationId,
              code,
            );
            this.setState({loading: true, codeVerify: code});
            auth.signInWithCredential(credential).then(() => {
              this.signUp();
            });
            // Do something with your new credential, e.g.:
            // firebase.auth().signInWithCredential(credential);
            // firebase.auth().currentUser.linkWithCredential(credential);
            // etc ...
            break;
        }
      },
      (error) => {
        // optionalErrorCb would be same logic as the ERROR case above,  if you've already handed
        // the ERROR case in the above observer then there's no need to handle it here
        //console.log(error);
        // verificationId is attached to error if required
        //console.log(error.verificationId);
        if (error.code === 'auth/invalid-phone-number') {
          goBack();
        }
        if (error.code === 'auth/unknown') {
          return;
        }
        showMessage({
          message: I18n.t('error'),
          description: I18n.t(`firebase_error.${error.code}`),
          type: 'danger',
        });
      },
      (phoneAuthSnapshot) => {
        // optionalCompleteCb would be same logic as the AUTO_VERIFIED/CODE_SENT switch cases above
        // depending on the platform. If you've already handled those cases in the observer then
        // there's absolutely no need to handle it here.
        this.payload = phoneAuthSnapshot;
        // Platform specific logic:
        // - if this is on IOS then phoneAuthSnapshot.code will always be null
        // - if ANDROID auto verified the sms code then phoneAuthSnapshot.code will contain the verified sms code
        //   and there'd be no need to ask for user input of the code - proceed to credential creating logic
        // - if ANDROID auto verify timed out then phoneAuthSnapshot.code would be null, just like ios, you'd
        //   continue with user input logic.
        //console.log(phoneAuthSnapshot);
      },
    );
  };

  signUp = () => {
    const {
      navigation: {navigate},
      dispatch,
      fcmToken,
    } = this.props;
    const {
      email,
      password,
      name,
      lastName,
      phone,
      address,
      countryId,
    } = this.state;
    this.setState({loading: true});
    let initalAccount = INITIAL_ACCOUNT;

    initalAccount.email = email;
    initalAccount.first_name = name;
    initalAccount.last_name = lastName;
    initalAccount.phone = phone;
    initalAccount.address = address;
    initalAccount.countryId = countryId;
    signUp(dispatch, initalAccount, password, fcmToken)
      .then(() => {
        this.setState({loading: false});
        navigate(DOCUMENTS, {first_time: true});
      })
      .catch((result) => {
        this.setState({loading: false});
        if (result === 'getUserError') {
          showMessage({
            message: I18n.t('success'),
            description: I18n.t('create_account_success'),
            type: 'success',
          });
          this.setState({loading: false});
          navigate(LOGIN);
          return;
        }
        showMessage({
          message: I18n.t('error'),
          description: I18n.t('firebase_error.failed_create'),
          type: 'danger',
        });
      });
  };

  verifyCodeHandle = () => {
    const {checkInternet} = this.props;
    const {codeVerify} = this.state;
    this.setState({loading: true});

    if (!this.payload) {
      this.setState({loading: false, verifyError: true});
      showMessage({
        message: I18n.t('error'),
        description: I18n.t('firebase_error.auth/code-expire'),
        type: 'danger',
      });
      return;
    }

    if (!checkInternet) {
      return showMessage({
        message: I18n.t('error'),
        description: I18n.t('internet'),
        type: 'danger',
      });
    }

    this.setState({loading: true});

    const {verificationId} = this.payload;
    const credential = firebaseAuth.PhoneAuthProvider.credential(
      verificationId,
      codeVerify,
    );
    auth
      .signInWithCredential(credential)
      .then(() => {
        this.setState({loading: false});
        this.signUp();
      })
      .catch(() => {
        this.setState({loading: false, verifyError: true});
        showMessage({
          message: I18n.t('error'),
          description: I18n.t('firebase_error.auth/code-expire'),
          type: 'danger',
        });
      });
  };

  resendCode = () => {
    this.setState({verifyError: false, resendCode: true});
    this.sendMessageCodeVerify();
  };

  handleChange = (name, value) => this.setState({[name]: value});

  render() {
    const {
      navigation: {goBack},
    } = this.props;
    return (
      <Container>
        <Header
          name={I18n.t('title_header.verify_phone')}
          actionDrawer={() => goBack()}
        />
        <VerifyPhoneLayout
          setState={this.handleChange}
          resendCodeHandle={this.resendCode}
          verifyCodeHandle={this.verifyCodeHandle}
          {...this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  checkInternet: state.checkInternet.checkInternet,
  fcmToken: state.fcmToken,
});

export default connect(mapStateToProps)(VerifyPhone);
