import React, {Component} from 'react';
import {connect} from 'react-redux';
import {PermissionsAndroid, Platform} from 'react-native';
import DocumentsLayout from './components/documents-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import I18n from '../../../../translation';
import Camera from '../../../shared/widgets/camera/camera';
import {
  DOCUMENT_URI,
  SELFIE_URI,
  RECEIPT_URI,
  URL_SELFIE,
  URL_DOC,
  URL_RECEIPT,
} from '../../../utils/constants/global-constants';
import {
  updateDriver,
  updateDocumentsDriver,
} from '../../../dashboard/services/services-dashboard';
import {showMessage} from 'react-native-flash-message';
import {uploadStorageImage} from '../../services/user-service';
import ImagePicker from 'react-native-image-picker';
import {
  LOGIN,
  SUCCESSFULL_VERIFICATION,
  PROCESS_VERIFY,
} from '../../../utils/constants/constants-navigate';

class Documents extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showCamera: false,
      showCameraType: '',
      selfieUri: '',
      documentUri: '',
      receiptUri: '',
      loading: false,
      first_time: false,
    };
  }

  componentDidMount() {
    const {
      user,
      navigation: {getParam},
    } = this.props;
    const first_time = getParam('first_time', false);
    this.setState({
      selfieUri: user.selfieUrl,
      documentUri: user.documentUrl,
      receiptUri: user.receiptUrl,
      first_time,
    });
  }

  saveUri = (data) => {
    const {showCameraType} = this.state;
    if (DOCUMENT_URI === showCameraType) {
      this.handleChange(DOCUMENT_URI, data);
      return;
    }
    if (SELFIE_URI === showCameraType) {
      this.handleChange(SELFIE_URI, data);
      return;
    }
    if (RECEIPT_URI === showCameraType) {
      this.handleChange(RECEIPT_URI, data);
      return;
    }
  };

  sendDocument = async () => {
    const {documentUri, selfieUri, receiptUri, first_time} = this.state;
    const {
      user,
      checkInternet,
      dispatch,
      navigation: {navigate},
    } = this.props;
    this.setState({loading: true});
    if (!checkInternet) {
      return showMessage({
        message: I18n.t('error'),
        description: I18n.t('internet'),
        type: 'danger',
      });
    }

    let newUser = user;

    let selfieUrl = '';
    let documentUrl = '';
    let receiptUrl = '';

    if (!user.selfieUrl) {
      selfieUrl = await this.uploadImage(selfieUri, URL_SELFIE);
      if (!selfieUrl) {
        return showMessage({
          message: I18n.t('error'),
          description: I18n.t('image_loading_error'),
          type: 'danger',
        });
      }
      newUser.selfieUrl = selfieUrl;
    }
    if (!user.documentUrl) {
      documentUrl = await this.uploadImage(documentUri, URL_DOC);
      if (!documentUrl) {
        return showMessage({
          message: I18n.t('error'),
          description: I18n.t('image_loading_error'),
          type: 'danger',
        });
      }
      newUser.documentUrl = documentUrl;
    }
    if (!user.receiptUrl) {
      receiptUrl = await this.uploadImage(receiptUri, URL_RECEIPT);
      if (!receiptUrl) {
        return showMessage({
          message: I18n.t('error'),
          description: I18n.t('image_loading_error'),
          type: 'danger',
        });
      }
      newUser.receiptUrl = receiptUrl;
    }

    await updateDriver(user.uid_driver, newUser)
      .then(() => {
        updateDocumentsDriver(newUser)
          .then(() => {
            if (!first_time) {
              showMessage({
                message: I18n.t('success'),
                description: I18n.t('success_documents'),
                type: 'success',
              });
            }
            navigate(first_time ? SUCCESSFULL_VERIFICATION : PROCESS_VERIFY, {
              uploadedDocuments: true,
            });
            this.setState({loading: false});
          })
          .catch(() => {
            return showMessage({
              message: I18n.t('error'),
              description: I18n.t('image_loading_error'),
              type: 'danger',
            });
          });
      })
      .catch(() => {
        return showMessage({
          message: I18n.t('error'),
          description: I18n.t('image_loading_error'),
          type: 'danger',
        });
      });
  };

  uploadImage = async (uri, key) => {
    const {
      user: {uid_driver},
    } = this.props;
    let result = '';
    await uploadStorageImage(uri, key, uid_driver).then((url) => {
      result = url;
    });
    return result;
  };

  openCamera = async (type) => {
    this.setState({showCameraType: type});
    if (Platform.OS === 'android') {
      await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.WRITE_EXTERNAL_STORAGE,
      );
    }
    const options = {
      title: I18n.t('buttons.select_option'),
      cancelButtonTitle: I18n.t('buttons.cancel'),
      customButtons: [{name: 'camera', title: I18n.t('buttons.camera')}],
      takePhotoButtonTitle: '',
      chooseFromLibraryButtonTitle: I18n.t('buttons.gallery'),
      quality: 1.0,
      mediaType: 'photo',
      maxWidth: 500,
      maxHeight: 500,
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);
      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        this.setState({showCamera: true});
      } else {
        this.saveUri(response.uri);
      }
    });
    // this.setState({showCameraType: type, showCamera: true});
  };

  handleChange = (name, value) => this.setState({[name]: value});
  render() {
    const {showCamera, showCameraType} = this.state;
    return (
      <Container>
        <Header name={I18n.t('title_header.document')} hiddenBack />
        <DocumentsLayout
          {...this.state}
          openCamera={this.openCamera}
          setState={this.handleChange}
          sendDocument={this.sendDocument}
        />
        <Camera
          {...this.state}
          isOpen={showCamera}
          selfie={showCameraType === SELFIE_URI}
          savePhoto={({uri}) => this.saveUri(uri)}
          changeState={(data) => this.handleChange('showCamera', data)}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
  documents: state.documents.documents,
  checkInternet: state.checkInternet.checkInternet,
});

export default connect(mapStateToProps)(Documents);
