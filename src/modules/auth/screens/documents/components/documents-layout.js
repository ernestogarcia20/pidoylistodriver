import React from 'react';
import {View, StyleSheet} from 'react-native';
import FabButton from '../../../../shared/widgets/fab-button';
import Text from '../../../../shared/widgets/text';
import palette from '../../../../shared/assets/palette';
import I18n from '../../../../../translation';
import {
  SELFIE_URI,
  DOCUMENT_URI,
  RECEIPT_URI,
} from '../../../../utils/constants/global-constants';
import Botton from '../../../../shared/widgets/button';

const Layout = ({
  user,
  sendDocument,
  openCamera,
  selfieUri,
  documentUri,
  receiptUri,
  loading,
}) => {
  return (
    <View style={styles.container} pointerEvents={loading ? 'none' : 'auto'}>
      <View style={styles.row}>
        <View style={styles.contentDocumentHorizontal}>
          <View style={styles.contentText}>
            <Text>{I18n.t('documents.add_selfie')}</Text>
          </View>
          <FabButton
            colorFab={selfieUri ? palette.secondaryColor : undefined}
            nameIcon={selfieUri ? 'check' : 'camera'}
            onPress={() => {
              if (selfieUri) {
                return;
              }
              openCamera(SELFIE_URI);
            }}
          />
        </View>
        <View style={styles.contentDocumentHorizontal}>
          <View style={styles.contentText}>
            <Text>{I18n.t('documents.add_document')}</Text>
            <Text fontSize={12} color="rgba(0,0,0,0.7)">
              {I18n.t('documents.type_document')}
            </Text>
          </View>
          <FabButton
            colorFab={documentUri ? palette.secondaryColor : undefined}
            nameIcon={documentUri ? 'check' : 'camera'}
            onPress={() => {
              if (documentUri) {
                return;
              }
              openCamera(DOCUMENT_URI);
            }}
          />
        </View>
        <View style={styles.contentDocumentHorizontal}>
          <View style={styles.contentText}>
            <Text>{I18n.t('documents.license')}</Text>
          </View>
          <FabButton
            colorFab={receiptUri ? palette.secondaryColor : undefined}
            nameIcon={receiptUri ? 'check' : 'camera'}
            onPress={() => {
              if (receiptUri) {
                return;
              }
              openCamera(RECEIPT_URI);
            }}
          />
        </View>
      </View>
      <View style={styles.contentButtons}>
        <Botton
          isLoading={loading}
          enable={selfieUri && documentUri && receiptUri}
          text={I18n.t('send_document')}
          onPress={sendDocument}
        />
      </View>
    </View>
  );
};

export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEE',
    alignContent: 'center',
  },
  contentDocumentHorizontal: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginVertical: 10,
    backgroundColor: 'white',
    paddingVertical: 10,
    paddingHorizontal: 5,
  },
  contentText: {width: '70%'},
  contentButtons: {
    position: 'absolute',
    bottom: 5,
    left: '2%',
    right: '2%',
    zIndex: 1,
  },
});
