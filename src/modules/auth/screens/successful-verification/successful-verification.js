import React, {Component} from 'react';
import {connect} from 'react-redux';
import SuccessfulVerificationLayout from './components/successful-verification-layout';
import Container from '../../../shared/widgets/container';
import {LOGIN} from '../../../utils/constants/constants-navigate';
import {logOut} from '../../services/user-service';

class SuccessfulVerification extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  logOut = () => {
    const {
      navigation: {navigate},
      dispatch,
    } = this.props;
    navigate(LOGIN);
    logOut(dispatch);
  };

  render() {
    return (
      <Container>
        <SuccessfulVerificationLayout
          goToDocument={this.goToDocument}
          logOut={this.logOut}
          {...this.state}
        />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(SuccessfulVerification);
