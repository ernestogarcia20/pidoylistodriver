import React from 'react';
import {View, StyleSheet} from 'react-native';
import Text from '../../../../shared/widgets/text';
import Button from '../../../../shared/widgets/button';
import LottieView from 'lottie-react-native';
import Success from '../../../../shared/assets/lottie/success.json';
import I18n from '../../../../../translation';
import palette from '../../../../shared/assets/palette';
const Layout = ({logOut}) => {
  return (
    <View style={styles.container}>
      <View style={styles.contentIcon}>
        <LottieView source={Success} autoPlay loop={false} />
      </View>
      <View style={styles.contentDesctiption}>
        <View>
          <Text fontSize={24} weight="SemiBold">
            {I18n.t('process_verify.success')}
          </Text>
        </View>
        <View style={styles.contentText}>
          <Text fontSize={14} weight="Normal">
            {I18n.t('process_verify.success_description')}
          </Text>
        </View>
      </View>
      <View style={styles.contentButton}>
        <Button
          text={I18n.t('buttons.ok')}
          colorButton={palette.primaryColor}
          onPress={logOut}
        />
      </View>
    </View>
  );
};

export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: 'white',
    alignContent: 'center',
  },
  contentIcon: {
    marginTop: '10%',
    flex: 0.5,
    justifyContent: 'center',
    alignItems: 'center',
  },
  contentDesctiption: {
    flex: 0.4,
    justifyContent: 'center',
  },
  contentButton: {
    flex: 0.3,
    justifyContent: 'center',
    marginHorizontal: 15,
  },
  contentText: {marginHorizontal: '10%', marginTop: 10},
});
