import React from 'react';
import {connect} from 'react-redux';
import {View} from 'react-native';
import {DRAWER, LOGIN, PROCESS_VERIFY} from '../../utils/constants/constants-navigate';

class Authenticator extends React.Component {
  constructor(props) {
    super(props);
    this.bootstrapValidation();
  }

  bootstrapValidation = () => {
    const {
      navigation: {navigate},
      user,
    } = this.props;
    if (!user) {
      navigate(LOGIN);
      return;
    }
    if (
      !user.verify &&
      (!user.documentUrl || !user.selfieUrl || !user.receiptUrl)
    ) {
      navigate(PROCESS_VERIFY, {uploadedDocuments: false});
      return;
    }
    navigate(user.verify ? DRAWER : PROCESS_VERIFY, {uploadedDocuments: true});
  };

  render = () => <View />;
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(Authenticator);
