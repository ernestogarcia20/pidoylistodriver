import API from '../../shared/services/api-service';
import {SET_USER} from '../reducers/user';
import {URL_DELIVERY, URL_SECURITY} from '../../utils/constants/url-constants';
import {getUser} from '../../dashboard/services/services-dashboard';
import {auth, database, storage} from '../../shared/services/firebase-service';
import {REMOVE_ALL} from '../../dashboard/reducers/order-list';
import * as RNLocalize from 'react-native-localize';
import {SET_PARAMS} from '../../../store/reducers/params';

export const signIn = (dispatch, data, fcmToken) => {
  return new Promise((resolve, reject) => {
    auth
      .signInWithEmailAndPassword(data.user, data.password)
      .then((user) => {
        if (fcmToken) {
          API.post(`${URL_SECURITY}/AuthenticationDriver`, {
            email: data.user,
            tokenNoti: fcmToken,
          });
        }
        getUser(dispatch, user.user.uid)
          .then(() => {
            resolve(user);
          })
          .catch(reject);
      })
      .catch(reject);
  });
};

export const signUp = (dispatch, data, password, fcmToken) => {
  return new Promise((resolve, reject) => {
    auth
      .createUserWithEmailAndPassword(data.email, password)
      .then((payload) => {
        API.post(`${URL_DELIVERY}/AddDriverFire`, {
          ...data,
          uid_driver: payload.user.uid,
          tokenNoti: fcmToken ? fcmToken : '',
        })
          .then((result) => {
            if (!result) {
              reject(result);
              return;
            }
            insertDriverDatabase({
              ...data,
              uid_driver: payload.user.uid,
              id: result.returnid,
            })
              .then(() => {
                getUser(dispatch, payload.user.uid)
                  .then(resolve)
                  .catch(() => {
                    reject('getUserError');
                  });
              })
              .catch(reject);
          })
          .catch(reject);
      })
      .catch(reject);
  });
};

export const forgetPassword = (email) => {
  auth.languageCode = RNLocalize.getLocales()[0].languageCode;
  return auth.sendPasswordResetEmail(email);
};

export const checkDriver = (data) => {
  return API.post(`${URL_DELIVERY}/CheckDriver`, data);
};

export const uploadStorageImage = async (file, key, uid) => {
  return new Promise((resolve, reject) => {
    //const fileName = file.substring(file.lastIndexOf('/') + 1);
    storage
      .ref(`drivers/${key}-${uid}.jpg`)
      .putFile(file)
      .on(
        'state_changed',
        (snapshot) => {
          if (snapshot.state === 'success') {
            snapshot.ref.getDownloadURL().then(resolve).catch(reject);
            return;
          }
        },
        (error) => {
          reject(error);
        },
      );
  });
};

export const checkConectionToFirebase = (dispatch) => {
  return new Promise((resolve, eject) => {
    return auth.onAuthStateChanged((payload) => {
      if (!payload) {
        return eject(null);
      }
      resolve(payload.uid);
      getUser(dispatch, payload.uid);
    });
  });
};

export const insertDriverDatabase = (data) => {
  return new Promise((resolve, reject) => {
    database
      .ref()
      .child(`/drivers/${data.uid_driver}/`)
      .set(data)
      .then(resolve)
      .catch(reject);
  });
};

export const getParams = (dispatch) => {
  database.ref('/params').once('value', (snapshot) => {
    if (!snapshot && !snapshot.exists()) {
      setDispatch(dispatch, false, SET_PARAMS);
    }
    const obj = snapshot.val();
    if (!obj) {
      setDispatch(dispatch, false, SET_PARAMS);
      return;
    }
    setDispatch(dispatch, obj, SET_PARAMS);
  });
};

export const checkEmail = (email, phone) => {
  return new Promise((resolve, reject) => {
    auth.signInWithEmailAndPassword(email, '123').catch(({code}) => {
      if (code === 'auth/wrong-password') {
        return reject(code);
      }
      verifyPhone(phone).then(resolve).catch(reject);
    });
  });
};
export const verifyPhone = (phone) => {
  return auth.verifyPhoneNumber(phone, true, true);
};

export const authPhone = (phone) => {
  return auth.signInWithPhoneNumber(phone);
};

export const logOut = (dispatch) => {
  return new Promise((resolve, reject) => {
    setDispatch(dispatch, '', REMOVE_ALL);
    auth
      .signOut()
      .then(() => {
        setDispatch(dispatch, '', REMOVE_ALL);
        resolve();
      })
      .catch(reject);
  });
};

export const setDispatch = (dispatch, data, type) => {
  dispatch({
    type,
    payload: data,
  });
};
