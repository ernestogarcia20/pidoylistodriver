import I18n from '../../../../translation/index';

export const inputRequired = (value) =>
  value ? '' : I18n.t('validations.required');

export const inputEmail = (value) => {
  if (!value) {
    return '';
  }

  if (/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,5}$/i.test(value)) {
    return '';
  }

  return I18n.t('validations.email.invalid');
};

export const validateForm = (form, setErrors, forceShow = false) =>
  setErrors(
    form
      .validate()
      .map((item) => ({field: item.fieldName, error: item.error, forceShow})),
  );

export const confirmEmailValidate = (value, email) => () => {
  if (!value) {
    return '';
  }

  if (value === email) {
    return '';
  }

  return I18n.t('validations.reEmail.invalid');
};
export const inputPassword = (value) => {
  if (!value) {
    return I18n.t('validations.required');
  }
  if (!value.match(/.{6}/)) {
    return I18n.t('validations.password.hasMiniChars');
  }
  return null;
};
export const confirmPasswordValidation = (value, password) => () => {
  if (!value) {
    return '';
  }

  if (value === password) {
    return null;
  }

  return I18n.t('validations.rePassword.invalid');
};

export const inputPhone = (value) => {
  if (!value) {
    return '';
  }

  if (value.match(/.{11}/)) {
    return '';
  }

  return 'ej: (+507 1234567)';
};
