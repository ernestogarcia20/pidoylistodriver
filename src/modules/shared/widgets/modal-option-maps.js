import React, {Component} from 'react';
import {
  StyleSheet,
  TouchableOpacity,
  View,
  Dimensions,
  Animated,
  Image,
  Alert,
} from 'react-native';
import Modal from 'react-native-modal';
import palette from '../assets/palette';
import Text from './text';
import Google from '../assets/icon/google-maps.png';
import Waze from '../assets/icon/waze.png';
import I18n from '../../../translation';
import FastImage from 'react-native-fast-image';
import LaunchNavigator from 'react-native-launch-navigator';

const {height, width} = Dimensions.get('window');
const INIT_HEIGHT = height * 0.25;
class ModalDetailDriver extends Component {
  state = {
    show: false,
    preSelectedItem: [],
    selectedItem: [],
    data: [],
    keyword: '',
  };

  animatedHeight = new Animated.Value(INIT_HEIGHT);

  closeModal = () => {
    const {closeModal = () => {}} = this.props;
    closeModal();
  };
  handleSelect = (navigate) => {
    const {onSelect = () => {}} = this.props;
    onSelect(navigate);
  };

  showModal = () => this.setState({show: true});

  render() {
    const {show, item, loading, deleteDriver = () => {}} = this.props;
    return (
      <Modal
        onBackdropPress={this.closeModal}
        style={styles.modal}
        useNativeDriver={true}
        animationInTiming={300}
        animationOutTiming={300}
        hideModalContentWhileAnimating
        isVisible={show}>
        <Animated.View
          style={[styles.modalContainer, {height: this.animatedHeight}]}>
          <Text weight="Medium" fontSize={15}>
            Seleccione una opcion para navegar
          </Text>
          <View style={styles.contentOption}>
            <TouchableOpacity
              style={styles.contentMapsOption}
              onPress={() =>
                this.handleSelect(LaunchNavigator.APP.GOOGLE_MAPS)
              }>
              <FastImage style={styles.image} source={Google} />
              <Text fontSize={16}>Google Maps</Text>
            </TouchableOpacity>
            <TouchableOpacity
              style={styles.contentMapsOption}
              onPress={() => this.handleSelect(LaunchNavigator.APP.WAZE)}>
              <FastImage style={styles.image} source={Waze} />
              <Text fontSize={16}>Waze</Text>
            </TouchableOpacity>
          </View>
        </Animated.View>
      </Modal>
    );
  }
}
export default ModalDetailDriver;
// define your styles
const styles = StyleSheet.create({
  container: {
    width: '100%',
    minHeight: 45,
    borderRadius: 2,
    paddingHorizontal: 16,
    flexDirection: 'row',
    alignItems: 'center',
    borderWidth: 1,
    borderColor: '#cacaca',
    paddingVertical: 4,
  },
  image: {width: 40, height: 40, marginBottom: 5},
  contentOption: {
    flexDirection: 'row',
    alignItems: 'flex-end',
    marginTop: '5%',
  },
  textInfo: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 5,
  },
  contentMapsOption: {
    justifyContent: 'center',
    alignItems: 'center',
    paddingHorizontal: '5%',
  },
  contentDetail: {width: width / 2},
  contentDetailRow: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    marginTop: 5,
  },
  contentRating: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: 10,
    marginTop: -35,
  },
  contentTextRating: {
    justifyContent: 'flex-end',
    paddingBottom: 3,
    paddingLeft: 5,
  },
  imagePadding: {
    borderRadius: 100,
    padding: 5,
    borderWidth: 2,
  },
  cancelButton: {
    position: 'absolute',
    width: 40,
    height: 40,
    borderRadius: 100,
    right: '0%',
    top: '-3%',
    backgroundColor: palette.darkColor,
    justifyContent: 'center',
    alignItems: 'center',
  },
  removeDriver: {position: 'absolute', bottom: 0, left: 0, right: 0},
  contentActived: {
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    marginTop: 5,
  },
  contentText: {marginTop: '5%', justifyContent: 'center'},

  contentImage: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  textButton: {
    color: 'rgba(0,0,0,0.6)',
    fontFamily: 'Avenir-Medium',
    fontSize: 15,
    marginTop: 5,
  },
  modal: {
    justifyContent: 'flex-end',
    margin: 0,
  },
  borderButton: {
    color: 'black',
    borderRadius: 3,
    borderStyle: 'solid',
    borderBottomWidth: 0.5,
  },
  textError: {
    marginLeft: 5,
    marginTop: 5,
  },
  modalContainer: {
    paddingTop: 16,
    backgroundColor: '#fff',
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8,
  },
  title: {
    fontSize: 16,
    marginBottom: 16,
    width: '100%',
    textAlign: 'center',
  },
  line: {
    height: 1,
    width: '100%',
    backgroundColor: '#cacaca',
  },
  inputKeyword: {
    height: 40,
    borderRadius: 5,
    borderWidth: 1,
    borderColor: '#cacaca',
    paddingLeft: 8,
    marginHorizontal: 24,
    marginTop: 16,
  },
  buttonWrapper: {
    marginVertical: 16,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
  },
  button: {
    height: 36,
    flex: 1,
  },
  buttonCancel: {
    height: 36,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: palette.darkColor,
    borderRadius: 15,
    borderWidth: 2,
    marginHorizontal: 5,
  },
  buttonSuccess: {
    height: 36,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 5,
    backgroundColor: palette.darkColor,
    borderRadius: 15,
  },
  colorText: {
    color: 'white',
  },
  selectedTitlte: {
    fontSize: 14,
    color: 'gray',
    flex: 1,
  },
  tagWrapper: {
    flexDirection: 'row',
    flexWrap: 'wrap',
  },
  listOption: {
    paddingHorizontal: 24,
    paddingTop: 1,
    marginTop: 16,
  },
  itemWrapper: {
    borderBottomWidth: 1,
    borderBottomColor: '#eaeaea',
    paddingVertical: 12,
    flexDirection: 'row',
    alignItems: 'center',
  },
  itemText: {
    fontSize: 16,
    color: '#333',
    flex: 1,
  },
  itemIcon: {
    width: 30,
    textAlign: 'right',
  },
  empty: {
    fontSize: 16,
    color: 'gray',
    alignSelf: 'center',
    textAlign: 'center',
    paddingTop: 16,
  },
});
