import React from 'react';
import {StyleSheet, Image, View, Dimensions} from 'react-native';
import Text from './text';
import NoWifi from '../assets/icon/wifi.png';
import Fuel from '../assets/icon/empty.png';
import I18n from '../../../translation';

const emptyComponent = ({ordersHistory}) => (
  <View style={styles.content}>
    <Image style={styles.image} source={ordersHistory ? Fuel : NoWifi} />
    <Text fontSize={16} style={styles.marginText}>
      {I18n.t(ordersHistory ? 'empty_orders_history' : 'internet')}
    </Text>
  </View>
);

const {height} = Dimensions.get('window');

const styles = StyleSheet.create({
  content: {
    height: height * 0.8,
    alignItems: 'center',
    justifyContent: 'center',
  },
  marginText: {
    marginTop: 10,
  },
  image: {
    width: 120,
    height: 120,
    marginBottom: 5,
  }
});

export default emptyComponent;
