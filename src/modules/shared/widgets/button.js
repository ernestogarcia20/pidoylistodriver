import React from 'react';

import {TouchableOpacity, ActivityIndicator, StyleSheet} from 'react-native';
import Text from './text';
import palette from '../assets/palette';

const Button = ({
  text,
  isLoading,
  colorIndicator = 'white',
  colorText = 'white',
  onPress = () => {},
  enable = true,
}) => {
  return (
    <TouchableOpacity
      style={[enable ? styles.buttonColor : styles.buttonColorDisable]}
      onPress={() => {
        if (!enable) {
          return;
        }
        onPress();
      }}>
      {!isLoading ? (
        <Text fontSize={14} color={colorText} weight="bold">
          {text}
        </Text>
      ) : (
        <ActivityIndicator color={colorIndicator} size="small" />
      )}
    </TouchableOpacity>
  );
};

export default Button;
const styles = StyleSheet.create({
  buttonColor: {
    height: 45,
    marginVertical: 5,
    backgroundColor: '#B31816',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonColorDisable: {
    height: 45,
    marginVertical: 5,
    backgroundColor: palette.inactiveColor,
    alignItems: 'center',
    justifyContent: 'center',
  },
});
