import React from 'react';
import {StyleSheet, ActivityIndicator, View, Dimensions} from 'react-native';
import Pallete from '../assets/palette';
import Text from './text';
import I18n from '../../../translation';
import palette from '../assets/palette';

const Loader = ({
  findOrder,
  text,
  textColor,
  colorIndicator,
  fontSize = 14,
}) => (
  <View style={styles.loader}>
    <ActivityIndicator
      color={colorIndicator ? colorIndicator : Pallete.primaryColor}
      size="large"
    />
    {findOrder && !text && (
      <Text style={styles.text}>{I18n.t('loader.waiting')}</Text>
    )}
    {!findOrder && (
      <Text
        style={styles.text}
        color={textColor ? textColor : palette.darkColor}
        fontSize={fontSize}>
        {text}
      </Text>
    )}
  </View>
);

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  loader: {
    height: height * 0.9,
    justifyContent: 'center',
    alignItems: 'center',
  },
  text: {marginTop: 10},
});

export default Loader;
