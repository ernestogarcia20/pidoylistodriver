import React from 'react';

import {TouchableOpacity, StyleSheet, ActivityIndicator} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import palette from '../assets/palette';

const FabButton = ({
  nameIcon,
  sizeIcon = 25,
  style = {},
  colorFab = '#1a73e8',
  colorIcon = 'white',
  onPress = () => {},
  styleIcon = {},
  loading,
}) => {
  return (
    <TouchableOpacity
      hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
      style={[styles.buttonColor, {...style, backgroundColor: colorFab}]}
      onPress={onPress}>
      {loading ? (
        <ActivityIndicator color="white" />
      ) : (
        <Icon
          name={nameIcon}
          style={styleIcon}
          size={sizeIcon}
          color={colorIcon}
        />
      )}
    </TouchableOpacity>
  );
};

export default FabButton;
const styles = StyleSheet.create({
  buttonColor: {
    height: 60,
    width: 60,
    borderRadius: 50,
    backgroundColor: '#1a73e8',
    alignItems: 'center',
    justifyContent: 'center',
  },
});
