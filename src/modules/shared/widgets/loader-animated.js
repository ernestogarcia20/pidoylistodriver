import React from 'react';
import {StyleSheet, Platform, View, Dimensions} from 'react-native';
import Pallete from '../assets/palette';
import Text from './text';
import I18n from '../../../translation';
import palette from '../assets/palette';
import WaitingDriver from '../assets/lottie/waiting_driver.json';
import LottieView from 'lottie-react-native';

const Loader = ({text, textColor, fontSize = 14}) => (
  <View style={styles.loader}>
    <View style={styles.contentWaiting}>
      <LottieView
        source={WaitingDriver}
        style={styles.animation}
        autoPlay={false}
        hardwareAccelerationAndroid
        enableMergePathsAndroidForKitKatAndAbove
        loop={false}
        cacheStrategy="strong"
      />
      <Text
        style={styles.text}
        weight="Medium"
        color={textColor ? textColor : palette.darkColor}
        fontSize={fontSize}>
        {text}
      </Text>
    </View>
  </View>
);

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  loader: {
    height: height * 0.7,
    justifyContent: 'center',
    alignItems: 'center',
  },
  animation: {width, marginBottom: '5%'},
  text: {marginTop: 10},
  contentWaiting: {flex: 0.9, justifyContent: 'center', alignItems: 'center'},
});

export default Loader;
