import React from 'react';
import {StyleSheet, Text} from 'react-native';
import {fontMaker} from './typografy';

const TextComponent = ({
  children,
  fontSize,
  color,
  weight = 'Regular',
  align = 'center',
  style,
}) => {
  const styles = StyleSheet.create({
    text: {
      fontSize,
      color,
      textAlign: align,
      ...fontMaker(weight),
    },
  });
  return <Text style={[styles.text, {...style}]}>{children}</Text>;
};

export default TextComponent;
