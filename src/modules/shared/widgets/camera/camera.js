import React, {Component} from 'react';
import {
  StyleSheet,
  View,
  Dimensions,
  TouchableWithoutFeedback,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {RNCamera} from 'react-native-camera';
import ControlsButton from './control-buttons';
import Modal from 'react-native-modal';
import Icon from '../fab-button';

const INITIAL_STATE = {
  isOpen: true,
  isRecording: false,
  time: 0,
  recorded: false,
  videoMode: true,
  recordedData: null,
  dataPhoto: null,
};

class Camera extends Component {
  state = {...INITIAL_STATE};

  /**
   *
   * @type {RNCamera || boolean}
   */
  camera = false;

  callback = () => {};

  takePicture = () => {
    if (this.camera) {
      const options = {
        quality: 0.5,
        // base64: true,
        forceUpOrientation: true,
        pauseAfterCapture: true,
        writeExif: true,
      };
      this.camera
        .takePictureAsync(options)
        .then(result => this.onSave(result))
        .catch(error => this.onSave({error}));
    }
  };

  open = (callback = true) => {
    this.callback = typeof callback === 'function' ? callback : () => {};
    const {changeState} = this.props;
    changeState(true);

    this.setState({...INITIAL_STATE});
  };

  close = () => {
    const {changeState} = this.props;
    changeState(false);
    this.setState({dataPhoto: null});
  };

  onSave = result => {
    this.setState({dataPhoto: result});
    // this.close();
  };

  onRepeat = () => {
    this.setState({dataPhoto: null});
  };

  onSavePress = () => {
    const {dataPhoto} = this.state;
    const {savePhoto} = this.props;
    savePhoto(dataPhoto);
    this.close();
  };

  onCancel = () => {
    this.close();
  };

  renderContent() {
    const {dataPhoto} = this.state;
    return (
      <View style={styles.controlLayer}>
        <View style={[styles.controls]}>
          <ControlsButton
            photo={dataPhoto}
            style={styles.recodingButton}
            onCancelPress={this.onCancel}
            onTakePress={this.takePicture}
            onRepeatPress={this.onRepeat}
            onSavePress={this.onSavePress}
          />
        </View>
      </View>
    );
  }

  renderContentHeader() {
    return (
      <View style={styles.controlLayer}>
        <View style={[styles.controlsHeader]}>
          <TouchableOpacity
            onPress={() => this.close()}
            style={styles.buttonCancel}>
            <Icon nameIcon="cancel" colorIcon="white" colorFab="transparent" />
          </TouchableOpacity>
        </View>
      </View>
    );
  }

  renderCamera = () => {
    const {barcode = false, handleBarcode, selfie} = this.props;
    return (
      <RNCamera
        ref={(ref) => {
          this.camera = ref;
        }}
        style={styles.preview}
        type={
          selfie ? RNCamera.Constants.Type.front : RNCamera.Constants.Type.back
        }
        captureAudio={false}
        flashMode={RNCamera.Constants.FlashMode.auto}
        androidCameraPermissionOptions={{
          title: 'Permission to use camera',
          message: 'We need your permission to use your camera',
          buttonPositive: 'Ok',
          buttonNegative: 'Cancel',
        }}
        onBarCodeRead={(barcodes) => {
          if (barcode) {
            const options = {
              enableVibrateFallback: true,
              ignoreAndroidSystemSettings: false,
            };
            handleBarcode(barcodes.data);
            this.onCancel();
          }
        }}
        onGoogleVisionBarcodesDetected={({barcodes}) => {
          //console.log(barcodes);
        }}>
        {!barcode ? this.renderContent() : this.renderContentHeader()}
      </RNCamera>
    );
  };

  renderPhoto = dataPhoto => (
    <ImageBackground style={{width, height}} source={dataPhoto}>
      {this.renderContent()}
    </ImageBackground>
  );

  render() {
    const {dataPhoto} = this.state;
    const {isOpen, barcode = false} = this.props;
    return (
      <Modal
        visible={isOpen}
        transparent
        animationType="fade"
        onRequestClose={this.close}>
        <View style={styles.modal}>
          <TouchableWithoutFeedback onPress={this.close}>
            <View style={styles.backdrop} />
          </TouchableWithoutFeedback>
          <View style={styles.container}>
            {dataPhoto === null ? (
              <View style={styles.content}>{this.renderCamera(barcode)}</View>
            ) : (
              <View style={styles.content}>{this.renderPhoto(dataPhoto)}</View>
            )}
          </View>
        </View>
      </Modal>
    );
  }
}

const {width, height} = Dimensions.get('window');

const styles = StyleSheet.create({
  modal: {
    alignItems: 'center',
    justifyContent: 'center',
    height,
  },
  backdrop: {
    position: 'absolute',
    top: 0,
    left: 0,
    backgroundColor: 'rgba(0,0,0,0.9)',
    width,
    height,
  },
  container: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  content: {
    alignItems: 'center',
    justifyContent: 'center',
  },
  preview: {
    width,
    height,
  },
  controlLayer: {
    position: 'absolute',
    top: 0,
    left: 0,
    bottom: 0,
    right: 0,
    backgroundColor: 'transparent',
  },
  controls: {
    position: 'absolute',
    bottom: 0,
    alignItems: 'center',
    height: 90,
    justifyContent: 'center',
    width,
  },
  controlsHeader: {
    flexDirection: 'row',
    position: 'absolute',
    top: 0,
    alignItems: 'center',
    height: 90,
    justifyContent: 'flex-start',
    width,
    padding: '5%',
  },
  buttonCancel: {
    width: 42,
    height: 42,
    backgroundColor: 'rgba(0,0,0,0.2)',
    borderRadius: 30,
    alignItems: 'center',
    justifyContent: 'center',
  },
});

export default Camera;
