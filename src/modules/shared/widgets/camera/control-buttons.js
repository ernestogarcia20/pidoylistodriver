import React from 'react';
import {
  TouchableOpacity,
  View,
  StyleSheet,
  SafeAreaView,
  Dimensions,
  Platform,
} from 'react-native';
import Icon from '../fab-button';

const renderWaiting = ({
  onCancelPress,
  style,
  onRepeatPress,
  onTakePress,
  onSavePress,
  photo,
}) => (
  <View style={styles.container}>
    <View
      style={[
        styles.contentButton,
        {
          marginBottom: Platform.OS === 'android' ? 20 : 0,
        },
      ]}>
      {photo === null && (
        <View style={styles.cancelButton}>
          <Icon
            nameIcon="times"
            colorIcon="white"
            sizeIcon={25}
            colorFab="transparent"
            onPress={onCancelPress}
          />
        </View>
      )}
      {photo !== null && (
        <View style={[styles.buttonContainer, style]}>
          <Icon
            nameIcon="redo"
            colorIcon="black"
            onPress={onRepeatPress}
            colorFab="transparent"
          />
        </View>
      )}
      {photo === null && (
        <TouchableOpacity style={[styles.buttonContainer, style]}>
          <Icon
            nameIcon="camera"
            colorIcon="black"
            colorFab="transparent"
            sizeIcon={20}
            onPress={onTakePress}
          />
        </TouchableOpacity>
      )}
      {photo !== null && (
        <TouchableOpacity style={[styles.buttonSave, style]}>
          <Icon
            nameIcon="check"
            colorIcon="black"
            onPress={onSavePress}
            colorFab="transparent"
          />
        </TouchableOpacity>
      )}
      {photo === null && <TouchableOpacity style={[styles.hidden, style]} />}
    </View>
  </View>
);

const RecordButton = props => {
  return <SafeAreaView>{renderWaiting(props)}</SafeAreaView>;
};

const {width} = Dimensions.get('window');
const styles = StyleSheet.create({
  container: {
    flex: 1,
    flexDirection: 'row',
    width,
    alignSelf: 'stretch',
    backgroundColor: '#303C4F',
    justifyContent: 'space-between',
  },
  contentButton: {
    flex: 1,
    flexDirection: 'row',
    width,
    paddingHorizontal: 20,
    alignSelf: 'stretch',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  cancelButton: {
    width: 55,
    height: 55,
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonContainer: {
    width: 55,
    height: 55,
    borderRadius: 40,
    backgroundColor: '#E5E5E5',
    alignItems: 'center',
    justifyContent: 'center',
  },
  buttonSave: {
    width: 55,
    height: 55,
    borderRadius: 40,
    backgroundColor: '#11FFBD',
    alignItems: 'center',
    justifyContent: 'center',
  },
  hidden: {
    width: 55,
    height: 55,
    borderRadius: 40,
    alignItems: 'center',
    justifyContent: 'center',
  },
  circleInsidePhoto: {
    width: 60,
    height: 60,
    borderRadius: 60 / 2,
    backgroundColor: 'white',
  },
});

export default RecordButton;
