import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import Icon from 'react-native-vector-icons/FontAwesome5';
import Text from './text';
import Pallete from '../assets/palette';

const Header = ({
  style,
  name,
  menu = false,
  hiddenBack = false,
  actionDrawer = () => {},
}) => (
  <View style={[styles.container, {...style}]}>
    {!hiddenBack && (
      <TouchableOpacity
        style={styles.menu}
        onPress={actionDrawer}
        hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}>
        <Icon
          name={menu ? 'bars' : 'chevron-left'}
          size={menu ? 24 : 21}
          color="white"
        />
      </TouchableOpacity>
    )}

    <View style={styles.contentText}>
      <Text color="white" fontSize={16} weight="Normal">
        {name}
      </Text>
    </View>
  </View>
);
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: 50,
    backgroundColor: Pallete.primaryColor,
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    paddingHorizontal: 20,
  },
  contentText: {
    justifyContent: 'center',
    alignItems: 'center',
    width: '100%',
    zIndex: -1,
  },
  menu: {
    position: 'absolute',
    marginLeft: '5%',
    zIndex: 1,
  },
});

export default Header;
