import React, {PureComponent} from 'react';
import {
  View,
  Animated,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';
import {string, func, object, number} from 'prop-types';
import PhoneInput from 'react-native-phone-input';
import CustomText from './text';
import palette from '../assets/palette';
import Icon from 'react-native-vector-icons/FontAwesome5';
import {fontMaker} from './typografy';

export class FloatingTitleTextInputField extends PureComponent {
  static propTypes = {
    attrName: string.isRequired,
    title: string.isRequired,
    value: string.isRequired,
    updateMasterState: func.isRequired,
    keyboardType: string,
    titleActiveSize: number, // to control size of title when field is active
    titleInActiveSize: number, // to control size of title when field is inactive
    titleActiveColor: string, // to control color of title when field is active
    titleInactiveColor: string, // to control color of title when field is active
    textInputStyles: object,
    otherTextInputProps: object,
  };

  static defaultProps = {
    keyboardType: 'default',
    titleActiveSize: 11.5,
    titleInActiveSize: 15,
    titleActiveColor: 'black',
    titleInactiveColor: 'dimgrey',
    textInputStyles: {},
    otherTextInputAttributes: {},
  };
  constructor(props) {
    super(props);
    const {value} = this.props;
    this.position = new Animated.Value(value ? 1 : 0);
    this.state = {
      isFieldActive: false,
      isoCode: 'pa',
      hiddenPhone: true,
    };
    this.toggle;
  }

  componentDidMount() {
    const {phone} = this.props;
    this._changeCountry('pa');
    if (!phone) {
      return;
    }
    Animated.timing(this.position, {
      toValue: 1,
      duration: 150,
      useNativeDriver: false,
    }).start();
  }

  _handleFocus = () => {
    const {onFocus = () => {}} = this.props;
    onFocus();
    if (!this.state.isFieldActive) {
      this.setState({isFieldActive: true});
      Animated.timing(this.position, {
        toValue: 1,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  _handleBlur = () => {
    const {onBlur = () => {}} = this.props;
    onBlur();
    if (this.state.isFieldActive && !this.props.value) {
      this.setState({isFieldActive: false});
      Animated.timing(this.position, {
        toValue: 0,
        duration: 150,
        useNativeDriver: false,
      }).start();
    }
  };

  _onChangeText = (updatedValue) => {
    const {onChangeText = () => {}} = this.props;
    const {attrName, updateMasterState, phone} = this.props;
    onChangeText(updatedValue);
    const {phoneCode} = this.state;
    const split = String(updatedValue);
    if (!phone) {
      updateMasterState(attrName, updatedValue);
      return;
    }
    if (!split.includes(`${phoneCode} `)) {
      return;
    }
    updateMasterState(attrName, updatedValue);
  };

  _returnAnimatedTitleStyles = () => {
    const {isFieldActive} = this.state;
    const {
      titleActiveColor,
      titleInactiveColor,
      titleActiveSize,
      titleInActiveSize,
    } = this.props;

    return {
      top: this.position.interpolate({
        inputRange: [0, 1],
        outputRange: [20, 0],
      }),
      fontSize: isFieldActive ? titleActiveSize : titleInActiveSize,
      color: isFieldActive ? titleActiveColor : titleInactiveColor,
    };
  };

  _changeCountry = (country) => {
    const {attrName, updateMasterState} = this.props;
    if (!this.phone) {
      return;
    }
    this.phone.selectCountry(country);
    this.setState({isoCode: country, phoneCode: this.phone.getCountryCode()});
    updateMasterState(attrName, `+${this.phone.getCountryCode()} `);
    this.position = new Animated.Value(this.phone.getCountryCode() ? 1 : 0);
  };

  showPhone = () => {
    this.setState({hiddenPhone: false}, () => {
      this._changeCountry('pa');
    });
  };

  render() {
    const {
      errors,
      name,
      textInputStyles,
      phone,
      title,
      multiline,
      editable = true,
      info = false,
      value,
      textInfo,
      isPassword,
      required,
      onSubmitEditing = () => {},
      onKeyPress = () => {},
    } = this.props;
    const {showEye, hiddenPhone} = this.state;
    return (
      <View style={Styles.container}>
        <Animated.Text
          style={[Styles.titleStyles, this._returnAnimatedTitleStyles()]}>
          {title}
        </Animated.Text>
        {!phone && (
          <View style={Styles.contentInputIcon}>
            <TextInput
              value={this.props.value}
              style={[
                Styles.textInput,
                {
                  ...textInputStyles,
                },
              ]}
              underlineColorAndroid="transparent"
              onFocus={this._handleFocus}
              editable={editable}
              multiline={multiline}
              onKeyPress={onKeyPress}
              onBlur={this._handleBlur}
              onChangeText={this._onChangeText}
              onSubmitEditing={onSubmitEditing}
              keyboardType={this.props.keyboardType}
              {...this.props.otherTextInputProps}
              secureTextEntry={!showEye && isPassword}
            />
            {isPassword && (
              <View style={Styles.contentEye}>
                <TouchableOpacity
                  hitSlop={{top: 20, bottom: 20, left: 50, right: 50}}
                  onPress={() => this.setState({showEye: !showEye})}>
                  <Icon
                    name={showEye ? 'eye' : 'eye-slash'}
                    size={18}
                    color="rgba(0,0,0,0.5)"
                  />
                </TouchableOpacity>
              </View>
            )}
          </View>
        )}
        {phone && !hiddenPhone && (
          <PhoneInput
            ref={(ref) => {
              this.phone = ref;
            }}
            style={[Styles.textInput, this.props.textInputStyles]}
            textStyle={[Styles.textInputPhone]}
            underlineColorAndroid="transparent"
            initialCountry={this.state.isoCode}
            textProps={{
              onFocus: this._handleFocus,
              onBlur: this._handleBlur,
              onChangeText: this._onChangeText,
              autoFocus: true,
            }}
            focus={this._handleFocus}
            blur={this._handleBlur}
            value={this.props.value}
            onSelectCountry={this._changeCountry}
            {...this.props.otherTextInputProps}
          />
        )}
        {phone && hiddenPhone && (
          <TouchableOpacity
            style={[Styles.textInput, this.props.textInputStyles]}
            onPress={this.showPhone}>
            <CustomText
              align="left"
              style={Styles.maskPhone}
              weight="Medium"
              color="rgba(0,0,0,0.5)">
              (+507 12345678)
            </CustomText>
          </TouchableOpacity>
        )}
        {required &&
          errors &&
          errors.length > 0 &&
          errors.map((item, index) =>
            item.field === name && item.error ? (
              <View key={`${index}`} style={{marginBottom: 5,}}>
                <CustomText
                  key={`${index}`}
                  color="#EB5757"
                  align="left"
                  weight="Bold"
                  fontSize={12}
                  style={Styles.textError}>
                  ! &nbsp;{' '}
                  <CustomText weight="Bold" color="#000000">
                    {item.error}
                  </CustomText>
                </CustomText>
              </View>
            ) : null,
          )}
      </View>
    );
  }
}

const Styles = StyleSheet.create({
  container: {
    marginVertical: 5,
    flex: 1,
  },
  textError: {
    marginLeft: 5,
    marginTop: 5,
  },
  contentInputIcon: {
    flexDirection: 'column',
    justifyContent: 'space-between',
    flex: 1,
  },
  textInput: {
    fontSize: 14,
    marginTop: 15,
    color: 'black',
    borderRadius: 3,
    borderStyle: 'solid',
    borderBottomWidth: 0.5,
    ...fontMaker('Medium'),
  },
  textInfo: {
    fontWeight: '400',
    textAlign: 'left',
  },
  contentModalInfo: {
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
    flexWrap: 'wrap',
    height: 'auto',
  },
  contentInfo: {
    position: 'absolute',
    right: '0%',
    width: 50,
    height: 20,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  textInputPhone: {
    color: 'black',
    ...fontMaker('Medium'),
  },
  contentEye: {right: 10, bottom: 20, position: 'absolute'},
  titleStyles: {
    position: 'absolute',
    fontFamily: 'Avenir-Medium',
  },
  maskPhone: {marginTop: 10},
});
