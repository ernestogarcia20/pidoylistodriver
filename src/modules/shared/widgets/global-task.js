import React, {PureComponent} from 'react';
import {PermissionsAndroid, Platform, BackHandler} from 'react-native';
import NetInfo, {NetInfoSubscription} from '@react-native-community/netinfo';
import {connect} from 'react-redux';
import {SET_INTERNET} from '../../../store/reducers/check-internet';
import Geolocation from '@react-native-community/geolocation';
import {
  setGeolocation,
  updateDriver,
} from '../../dashboard/services/services-dashboard';
import SplashScreen from 'react-native-splash-screen';
import FlashMessage from 'react-native-flash-message';
import {NavigationActions} from 'react-navigation';
import messaging from '@react-native-firebase/messaging';
import {
  checkConectionToFirebase,
  getParams,
} from '../../auth/services/user-service';
import {SET_FCM_TOKEN} from '../../../store/reducers/fcm-token';
import {database} from '../services/firebase-service';
class GlobalTask extends PureComponent {
  state = {internet: true};

  subscription: NetInfoSubscription | null = null;

  componentDidMount() {
    const {dispatch, user} = this.props;

    database.setPersistenceEnabled(true);
    getParams(dispatch);

    try {
      setTimeout(() => {
        SplashScreen.hide();
      }, 350);
    } catch (e) {
      // console.log('error', e);
    }

    BackHandler.addEventListener('hardwareBackPress', this.handleBackButton);
    NetInfo.fetch().then((e) => {
      this.setDispatch(dispatch, SET_INTERNET, e.isConnected);
    });
    this.subscription = NetInfo.addEventListener(this.handleConnectionChange);

    checkConectionToFirebase(dispatch).catch(() => {
      if (!user) {
        return;
      }
    });

    /** CallBack notificacion en FOREGROUND */
    messaging().onMessage(this.onMessageReceived);
    messaging().setBackgroundMessageHandler(this.onMessageReceived);
    /** CallBack al abrir la notificacion presionada */
    messaging().onNotificationOpenedApp(() => {});
    this.requestUserPermission();

    if (Platform.OS === 'ios') {
      this.callLocation();
    } else {
      this.requestLocationPermission();
    }
  }

  onMessageReceived = async () => {};

  componentWillUnmount() {
    this.subscription && this.subscription();
    Geolocation.clearWatch(this.watchID);
    BackHandler.removeEventListener('hardwareBackPress', this.handleBackButton);
  }

  componentDidUpdate(prevProps) {
    const {user} = this.props;
    if (!user) {
      return;
    }
    if (
      prevProps.user.reload_geolocation !== user.reload_geolocation &&
      user.reload_geolocation
    ) {
      this.reloadGeolocation();
    }
  }

  /**
   * LISTENER EL GO BACK DE ANDROID
   */
  handleBackButton = () => {
    const {dispatch, backAvailable} = this.props;
    if (!backAvailable) {
      return true;
    }

    dispatch(NavigationActions.back());
    return true;
  };

  /**
   * LISTENER PARA SABER SI EL DISPOSITIVO ESTA CONECTADO A INTERNET
   */
  handleConnectionChange = (state) => {
    const {dispatch} = this.props;
    this.setDispatch(dispatch, SET_INTERNET, state.isConnected);
  };

  /**
   * PERMISOS PARA LA LOCALIZACION
   */
  requestLocationPermission = async () => {
    try {
      const granted = await PermissionsAndroid.request(
        PermissionsAndroid.PERMISSIONS.ACCESS_FINE_LOCATION,
        {
          title: 'Location Access Required',
          message: 'This App needs to Access your location',
        },
      );
      if (granted === PermissionsAndroid.RESULTS.GRANTED) {
        //To Check, If Permission is granted
        this.callLocation();
      } else {
        alert('Permission Denied');
      }
    } catch (err) {
      console.warn(err);
      return err;
    }
  };

  /**
   * PERMISOS PARA LAS NOTIFICACIONES
   */
  requestUserPermission = async () => {
    const authStatus = await messaging().requestPermission();
    const enabled =
      authStatus === messaging.AuthorizationStatus.AUTHORIZED ||
      authStatus === messaging.AuthorizationStatus.PROVISIONAL;

    if (enabled) {
      this.getFcmToken();
      console.log('Authorization status:', authStatus);
    }
  };

  /**
   * OBTENER EL FCM TOKEN PARA RECIBIR NOTIFICACIONES
   */
  getFcmToken = async () => {
    const fcmToken = await messaging().getToken();
    const {dispatch} = this.props;
    if (fcmToken) {
      this.setDispatch(dispatch, SET_FCM_TOKEN, fcmToken);
      console.log('Your Firebase Token is:', fcmToken);
    }
  };

  /**
   * REFRESCAMOS LA UBICACIONES DEL CONDUCTOR
   */
  reloadGeolocation = () => {
    const {user, geolocation} = this.props;
    if (!user) {
      return;
    }

    if (!user.reload_geolocation) {
      return;
    }
    updateDriver(user.uid_driver, {
      latitude: geolocation.lat,
      longitude: geolocation.long,
      reload_geolocation: false,
    });
  };

  /**
   * OBTENEMOS POR PRIMERA VEZ LA UBICACION DEL CONDUCTOR Y EN TIEMPO REAL
   */
  callLocation() {
    //alert("callLocation Called");
    let geolocation = {};
    Geolocation.getCurrentPosition(
      //Will give you the current location
      (position) => {
        const {dispatch, user} = this.props;
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json
        geolocation = {long: currentLongitude, lat: currentLatitude};
        setGeolocation(dispatch, geolocation);

        if (!user) {
          return;
        }
        if (!user.reload_geolocation) {
          return;
        }
        updateDriver(user.uid_driver, {
          latitude: currentLatitude,
          longitude: currentLongitude,
          reload_geolocation: false,
        });
      },
      (error) => error,
    );
    this.watchID = Geolocation.watchPosition(
      (position) => {
        const {dispatch, user} = this.props;
        //Will give you the location on location change
        const currentLongitude = JSON.stringify(position.coords.longitude);
        //getting the Longitude from the location json
        const currentLatitude = JSON.stringify(position.coords.latitude);
        //getting the Latitude from the location json
        geolocation = {long: currentLongitude, lat: currentLatitude};
        setGeolocation(dispatch, geolocation);
        if (!user) {
          return;
        }
        if (!user.verify) {
          return;
        }
        updateDriver(user.uid_driver, {
          latitude: currentLatitude,
          longitude: currentLongitude,
          reload_geolocation: false,
        });
      },
      (error) => error, //console.log(error),
      {
        enableHighAccuracy: true,
        timeout: 20000,
        maximumAge: 1000,
        distanceFilter: 100,
        useSignificantChanges: true,
      },
    );
  }

  setDispatch = (dispatch, type, data) => {
    dispatch({
      type,
      payload: data,
    });
  };

  render() {
    return <FlashMessage duration={3000} position="top" />;
  }
}
const mapStateToProps = ({
  checkInternet: {checkInternet},
  myGeolocation: {geolocation},
  backAvailable,
  user,
}) => ({
  checkInternet,
  user,
  geolocation,
  backAvailable,
});

export default connect(mapStateToProps)(GlobalTask);
