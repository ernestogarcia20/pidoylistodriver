import React, {Fragment} from 'react';
import {
  SafeAreaView,
  StyleSheet,
  Keyboard,
  StatusBar,
  ActivityIndicator,
  View,
} from 'react-native';
import Pallete from '../assets/palette';
import palette from '../assets/palette';

const Container = ({children, style, isLoading}) => (
  <Fragment>
    <SafeAreaView style={{flex: 0, backgroundColor: palette.primaryColor}} />

    <SafeAreaView
      onStartShouldSetResponder={Keyboard.dismiss}
      style={[styles.container, {...style}]}>
      <StatusBar
        animated
        showHideTransition="fade"
        backgroundColor={Pallete.primaryColor}
        barStyle="light-content"
      />
      {isLoading ? loader() : children}
    </SafeAreaView>
  </Fragment>
);

const loader = () => {
  return (
    <View style={styles.loader}>
      <ActivityIndicator color={Pallete.primaryColor} size="large" />
    </View>
  );
};

const styles = StyleSheet.create({
  background: {top: 0, left: 0, right: 0, bottom: 0, position: 'absolute'},
  container: {flex: 1, backgroundColor: 'white'},
  contentKeyboard: {flex: 0.9},
  loader: {
    flex: 1,
    backgroundColor: 'white',
    justifyContent: 'center',
    alignItems: 'center',
  },
});

export default Container;
