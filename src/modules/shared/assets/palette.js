export default {
  primaryColor: '#B31816',
  primaryColorOpacity: 'rgba(0, 0, 0, 0.2)',
  secondaryColor: '#149B72',
  darkColor: '#13151E',
  activeColor: '#AAFD6F',
  inactiveColor: 'rgba(0, 0,0,0.2)',
  alertColor: '#B67349',
  secondaryText: '#747474',
  primaryText: '#FFF',
  active: '#45BBD4',
  inactive: '#EC5353',
  status: {
    in_progress_merchant: '#974CBB',
    in_progress_client: '#668BEA',
    pending: '#E8C751',
    cancel: '#EC5353',
    finish: '#149B72',
  },
  progressStyle: {
    // backgroundColor: '#17FFE3',
    backgroundColor: 'transparent',
    foregroundColor: 'rgba(0,0,0,0.46)',
    borderColor: 'transparent',
    borderRadius: 1,
    height: 4,
    borderWidth: 0,
    marginTop: 9,
  },
  buttonColor: {
    height: 45,
    marginVertical: 10,
    backgroundColor: '#149B72',
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 10,
    padding: 10,
  },
};
