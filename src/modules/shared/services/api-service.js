import {AsyncStorage} from 'react-native';
import validation from './validation-service';
import ENV from '../../../../env';

export const BEARER_TOKEN_NAME = 'bearer_token_api';

export const BASE_API = ENV.path;
export const VERSION = 'v1';
export const BEARER_TOKEN = '';
export const HTTP_ERROR_INTERNET_LOST = 'http/no-internet';
export const HTTP_SERVER_UNAVAILABLE = 'http/server-unavailable';

class Api {
  async get(uri, version = VERSION) {
    const url = `${BASE_API}/${uri}`;
    const headers = await this.getHeaders();
    return fetch(url, {
      method: 'GET',
      headers,
    }).then((response) => response.json());
  }

  async post(uri, body, version = VERSION, returnRequest = false) {
    const headers = await this.getHeaders();
    try {
      console.log(`${BASE_API}/${uri}`, body);
      const request = await fetch(`${BASE_API}/${uri}`, {
        method: 'POST',
        headers,
        body: JSON.stringify(body),
      });
      if (returnRequest) {
        return request;
      }
      try {
        if (request.status === 500) {
          return Promise.reject({code: HTTP_SERVER_UNAVAILABLE});
        }

        const response = await request.json();
        if (request.status !== 200) {
          return Promise.reject(response);
        }

        return response;
      } catch (e) {
        return request;
      }
    } catch (e) {
      return Promise.reject({code: HTTP_ERROR_INTERNET_LOST});
    }
  }

  isJson = (str) => {
    try {
      JSON.parse(str);
    } catch (e) {
      return false;
    }
    return true;
  };

  async upload(uri, body, version = VERSION, returnRequest = false) {
    const headers = await this.getHeadersUpload();
    try {
      const request = await fetch(`${BASE_API}/${uri}`, {
        method: 'POST',
        headers,
        body,
      });
      if (returnRequest) {
        return request;
      }

      try {
        if (request.status === 500) {
          return Promise.reject({code: HTTP_SERVER_UNAVAILABLE});
        }

        const response = await request.json();
        const errorKey = validation(request.status, response.path);
        if (errorKey !== '') {
          if (response.id_token !== undefined || response.id_token !== null) {
            return Promise.reject({code: errorKey, token: response.id_token});
          }
          return Promise.reject({code: errorKey});
        }
        return response;
      } catch (e) {
        return request;
      }
    } catch (e) {
      return Promise.reject({code: HTTP_ERROR_INTERNET_LOST});
    }
  }

  setAuthorization = authorization => {
    try {
      console.log('setting header', authorization);
      AsyncStorage.setItem(BEARER_TOKEN_NAME, authorization);
    } catch (e) {
      console.log('Error saving token', authorization);
    }
  };

  getAuthorization = () => {
    try {
      return AsyncStorage.getItem(BEARER_TOKEN_NAME);
    } catch (e) {
      console.log('Error reading local token:', e);
    }

    return null;
  };

  async getHeaders() {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'application/json',
    };

    const authorization = await this.getAuthorization();
    if (authorization !== null) {
      headers.Authorization = `Bearer ${authorization}`;
    }

    return headers;
  }

  async getHeadersUpload() {
    const headers = {
      Accept: 'application/json',
      'Content-Type': 'multipart/form-data;',
    };

    const authorization = await this.getAuthorization();
    if (authorization !== null) {
      headers.Authorization = `Bearer ${authorization}`;
    }

    return headers;
  }
}

export default new Api();
