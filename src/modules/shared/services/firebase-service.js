import firebaseDatabase from '@react-native-firebase/database';
import firebaseAuth from '@react-native-firebase/auth';
import firebaseStore from '@react-native-firebase/storage';

/**
 * Firebase auth
 */
export const auth = firebaseAuth();

/**
 * Firebase realtime database
 */
export const database = firebaseDatabase();

/**
 * Firebase Cloud Storage
 */
export const storage = firebaseStore();

/**
 * Gets  DataSnapshot for the location at the specified in ref param.
 * @param ref
 * @returns {*}
 */
export const databaseRef = ref => database.ref(ref);
