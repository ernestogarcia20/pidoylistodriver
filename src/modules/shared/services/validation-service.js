export default (code, path) => {
  if (code === 401) {
    if (path === 'auth/finish-registration') {
      return 'register';
    }
    if (String(path).toLowerCase() === 'unauthorized') {
      return 'token';
    }
    if (path === 'auth/wrong-password') {
      return 'userNotFound';
    }
    if (path === '/inovebank/api/client-authenticate') {
      return 'userNotFound';
    }
    if (path === 'auth/waiting') {
      return 'waiting';
    }
    return 'userNotFound';
  }
  if (code === 400) {
    if (path === '/inovebank/api/client-authenticate') {
      return 'userNotFound';
    }
    if (path === 'userexists') {
      return 'userexist';
    }
    if (path === 'phone.already.exist') return 'existPhone';
    return 'userNotFound';
  }
  return '';
};
