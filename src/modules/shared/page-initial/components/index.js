import React from 'react';
import {View, StyleSheet} from 'react-native';

const Layout = () => {
  return (
    <View style={styles.container}>
      <View />
    </View>
  );
};

export default Layout;
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#EEE',
    alignContent: 'center',
  },
});
