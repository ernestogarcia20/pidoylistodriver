import React, {Component} from 'react';
import {connect} from 'react-redux';
import VerifyPhoneLayout from './components/sign-up-layout';
import Container from '../../../shared/widgets/container';
import Header from '../../../shared/widgets/header';
import I18n from '../../../../translation';

class VerifyPhone extends Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    const {
      navigation: {goBack},
    } = this.props;
    return (
      <Container>
        <Header name={I18n.t('sign_up')} actionDrawer={() => goBack()} />
        <VerifyPhoneLayout {...this.state} />
      </Container>
    );
  }
}

const mapStateToProps = (state, store) => ({
  dispatch: store.navigation.dispatch,
  user: state.user,
});

export default connect(mapStateToProps)(VerifyPhone);
