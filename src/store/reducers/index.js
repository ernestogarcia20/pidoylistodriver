import {combineReducers} from 'redux';

import user from '../../modules/auth/reducers/user';
import documents from '../../modules/auth/reducers/documents';
import orderList from '../../modules/dashboard/reducers/order-list';
import myGeolocation from '../../modules/dashboard/reducers/my-geolocation';
import navigation from './navigation';
import app from './app';
import checkInternet from './check-internet';
import ordersHistory from '../../modules/dashboard/reducers/orders-history';
import listIgnore from '../../modules/dashboard/reducers/list-ignore';
import menu from './menu';
import backAvailable from './handle-back';
import params from './params';
import fcmToken from './fcm-token';

export default combineReducers({
  app,
  navigation,
  user,
  checkInternet,
  fcmToken,
  orderList,
  myGeolocation,
  ordersHistory,
  menu,
  listIgnore,
  documents,
  backAvailable,
  params,
});
