/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_PARAMS = 'SET_PARAMS';
export const REMOVE_ALL = 'REMOVE_ALL';

/**
 * @return {boolean | Object}
 */
function params(state = false, action) {
  switch (action.type) {
    case SET_PARAMS:
      return action.payload;
    case REMOVE_ALL:
      return false;
    default:
      return state;
  }
}

export default params;
