/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_MENU = 'SET_MENU';
export const REMOVE_ALL = 'REMOVE_ALL';
import I18n from '../../translation';

const INITIAL_STATE = {
  menu: I18n.t('menu_list.order_list'),
};
function menu(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_MENU:
      return {menu: action.payload};
    case REMOVE_ALL:
      return INITIAL_STATE;
    default:
      return state;
  }
}

export default menu;
