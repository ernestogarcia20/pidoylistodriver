/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_FCM_TOKEN = 'SET_FCM_TOKEN';
export const REMOVE_ALL = 'REMOVE_ALL';

/**
 * @return {boolean | Object}
 */
function fcmToken(state = false, action) {
  switch (action.type) {
    case SET_FCM_TOKEN:
      return action.payload;
    default:
      return state;
  }
}

export default fcmToken;
