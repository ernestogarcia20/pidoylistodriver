export const SET_INTERNET = 'SET_INTERNET';
export const REMOVE_ALL = 'REMOVE_ALL';
const INITIAL_STATE = {
  checkInternet: true,
};

function checkInternet(state = INITIAL_STATE, action) {
  switch (action.type) {
    case SET_INTERNET:
      return { ...state, checkInternet: action.payload };
    case REMOVE_ALL:
      return INITIAL_STATE;
    default:
      return state;
  }
}

export default checkInternet;
