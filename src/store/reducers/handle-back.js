/*
  User Reducer, is used to manage user data and persist them.
 */

export const SET_HANDLE_BACK = 'SET_HANDLE_BACK';
export const REMOVE_ALL = 'REMOVE_ALL';

/**
 * @return {boolean | Object}
 */
function handleBack(state = true, action) {
  switch (action.type) {
    case SET_HANDLE_BACK:
      return action.payload;
    case REMOVE_ALL:
      return true;
    default:
      return state;
  }
}

export default handleBack;
