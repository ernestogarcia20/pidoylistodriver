import createAnimatedSwitchNavigator from 'react-navigation-animated-switch';
import {createSwitchNavigator} from 'react-navigation';
import {createDrawerNavigator} from 'react-navigation-drawer';
import {createStackNavigator} from 'react-navigation-stack';
import Menu from './modules/menu/menu';
import Authenticator from './modules/auth/screens/authenticator';
import Login from './modules/auth/screens/sign-in/sign-in';
import PhoneVerify from './modules/auth/screens/verify-phone/verify-phone';
import Documents from './modules/auth/screens/documents/documents';
import ProcessVerify from './modules/auth/screens/process-verify/process-verify';
import SuccessfullVerification from './modules/auth/screens/successful-verification/successful-verification';
import RecoverPassword from './modules/auth/screens/recover-password/recover-password';
import Dashboard from './modules/dashboard/screens/main-page/dashboard';
import OrderDetail from './modules/dashboard/screens/order-detail/order-detail';
import Settings from './modules/dashboard/screens/settings/screens/settings-page/settings-page';
import SettingProfile from './modules/dashboard/screens/settings/screens/settings-profile/settings-profile';
import TermsConditions from './modules/dashboard/screens/settings/screens/terms-conditions/terms-conditions';
import OrdersHistory from './modules/dashboard/screens/orders-history/orders-history';
import ModalOffer from './modules/dashboard/screens/modal-offer-order/modal-offer-order';
import SignUp from './modules/auth/screens/sign-up/sign-up';
import {
  LOGIN,
  AUTHENTICATOR,
} from './modules/utils/constants/constants-navigate';

const OrderDetailStack = createStackNavigator(
  {
    Dashboard,
    OrderDetail,
  },
  {
    initialRouteName: 'Dashboard',
    headerMode: 'none',
  },
);

const DashboardStack = createStackNavigator(
  {
    // For header options
    Dashboard: OrderDetailStack,
    Modal: ModalOffer,
    OrderDetail: OrderDetailStack,
  },
  {
    headerMode: 'none',
    mode: 'modal',
    defaultNavigationOptions: {
      cardStyle: {backgroundColor: 'transparent'},
    },
  },
);

// ACTIONS FOR REMOVE CURRENT PAGE IN DashboardStack
const prevGetStateForActionHistory = OrderDetailStack.router.getStateForAction;
OrderDetailStack.router = {
  ...OrderDetailStack.router,
  getStateForAction(action, state) {
    if (state && action.type === 'ReplaceCurrentScreen') {
      const routes = state.routes.slice(0, state.routes.length - 1);
      routes.push(action);
      return {
        ...state,
        routes,
        index: routes.length - 1,
      };
    }
    return prevGetStateForActionHistory(action, state);
  },
};

const SettingsStack = createStackNavigator(
  {
    // For header options
    Settings,
    SettingProfile,
    TermsConditions,
  },
  {
    navigationOptions: {
      header: null,
    },
    headerMode: 'none',
  },
);

const Drawer = createDrawerNavigator(
  {
    Dashboard: DashboardStack,
    OrdersHistory,
    Settings: SettingsStack,
  },
  {
    unmountInactiveRoutes: true,
    headerMode: 'none',
    contentComponent: Menu,
    drawerType: 'back',
  },
);

const Auth = createStackNavigator(
  {
    Login,
    SignUp,
    PhoneVerify,
    RecoverPassword,
    TermsConditions,
  },
  {
    initialRouteName: LOGIN,
    headerMode: 'none',
  },
);

export default createSwitchNavigator(
  {
    Authenticator,
    Login: Auth,
    Drawer,
    Documents,
    ProcessVerify,
    SuccessfullVerification,
  },
  {
    initialRouteName: AUTHENTICATOR,
  },
);
